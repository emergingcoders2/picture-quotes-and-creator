package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.RepeatListener;
import com.emergingcoders.imagestatus.databinding.DialogFontAlignmentBinding;
import com.emergingcoders.imagestatus.databinding.DialogFontSizeBinding;
import com.emergingcoders.imagestatus.databinding.FragmentWmFontAdjustTabBinding;

import static com.emergingcoders.imagestatus.Globals.Globals.quoteTextSize;

public class Fragment_WM_Font_Adjustment extends Fragment {
    FragmentWmFontAdjustTabBinding mBinding;

    private Activity context;
    private int watermarkSize = 32, watermarkAlign = Gravity.CENTER;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.fragment_wm_font_adjust_tab,
                vcontainer,
                false);

        //Increase/Decrease WM text size...
        mBinding.btnFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeWMFontDialog();
                adjustFontSize();
            }
        });

        //Align text in the WM section only...
        mBinding.btnTextAlignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeWMFontDialog();
                adjustTextAlignment();
            }
        });

        return mBinding.getRoot();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void adjustFontSize() {
        DialogFontSizeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_font_size,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final int[] textSize = {quoteTextSize};
        watermarkSize = AppPreferences.getWMFontSize(context);
        binding.ivFontAdjustIncrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkSize++;
                ((CreateQuoteActivity) context).tvAddWM.setTextSize(TypedValue.COMPLEX_UNIT_PX, watermarkSize);
            }
        }));

        binding.ivFontAdjustDecrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (watermarkSize > 1) {
                    playSound(R.raw.button_tap);
                    watermarkSize--;
                    ((CreateQuoteActivity) context).tvAddWM.setTextSize(TypedValue.COMPLEX_UNIT_PX, watermarkSize);
                }
            }
        }));

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateQuoteActivity) context).closeWMFontDialog();
                playSound(R.raw.button_tap);
                AppPreferences.setWMFontSize(context, watermarkSize);
                ((CreateQuoteActivity) context).deploayChanges(true);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setWMFontStyles();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvAddWM.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.getWMFontSize(context));
                ((CreateQuoteActivity) context).setWMFontStyles();
                dialog.dismiss();
            }
        });
    }

    private void adjustTextAlignment() {
        final DialogFontAlignmentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_font_alignment,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        if (AppPreferences.getWMAlignment(context) == Gravity.CENTER)
            binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center_black);
        else if (AppPreferences.getWMAlignment(context) == (Gravity.START | Gravity.CENTER_VERTICAL))
            binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left_black);
        else
            binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right_black);

        binding.ivLeftAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvAddWM.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
                watermarkAlign = Gravity.START | Gravity.CENTER_VERTICAL;
                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left_black);
                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center);
                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right);
                ((CreateQuoteActivity) context).deploayChanges(true);
            }
        });

        binding.ivCenterAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvAddWM.setGravity(Gravity.CENTER);
                watermarkAlign = Gravity.CENTER;
                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center_black);
                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left);
                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right);
                ((CreateQuoteActivity) context).deploayChanges(true);
            }
        });

        binding.ivRightAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvAddWM.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
                watermarkAlign = Gravity.END | Gravity.CENTER_VERTICAL;
                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right_black);
                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center);
                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left);
                ((CreateQuoteActivity) context).deploayChanges(true);
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).deploayChanges(true);
                AppPreferences.setWMAlignment(context, watermarkAlign);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setWMFontStyles();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvAddWM.setGravity(AppPreferences.getWMAlignment(context));
                dialog.dismiss();
                ((CreateQuoteActivity) context).setWMFontStyles();
            }
        });
    }
}
