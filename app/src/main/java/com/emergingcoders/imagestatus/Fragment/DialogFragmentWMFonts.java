package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.emergingcoders.imagestatus.Adapter.AdapterWMFontTabs;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.databinding.TabLayoutWmFontStyleBinding;

public class DialogFragmentWMFonts extends DialogFragment {
    TabLayoutWmFontStyleBinding mBinding;

    Activity context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //This style is used for full width and shadowed dialog like as PopupWindow...
        setStyle(DialogFragment.STYLE_NORMAL, R.style.QuoteDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        context = getActivity();

        Window window = getDialog().getWindow();
        window.setGravity(Gravity.BOTTOM);

        getDialog().setCanceledOnTouchOutside(false);

        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.tab_layout_wm_font_style,
                vcontainer,
                false);

        //Add tabs to TabLayout...
        mBinding.tabFontStyle.addTab(mBinding.tabFontStyle.newTab().setText("Style").setIcon(R.drawable.ic_font_size));
        mBinding.tabFontStyle.addTab(mBinding.tabFontStyle.newTab().setText("Family").setIcon(R.drawable.ic_font_type));
        mBinding.tabFontStyle.addTab(mBinding.tabFontStyle.newTab().setText("Shadow").setIcon(R.drawable.ic_shadow));
        mBinding.tabFontStyle.addTab(mBinding.tabFontStyle.newTab().setText("Rotate").setIcon(R.drawable.ic_rotate_tab));
        mBinding.tabFontStyle.setTabGravity(TabLayout.GRAVITY_FILL);

        AdapterWMFontTabs adapterWMFontTabs = new AdapterWMFontTabs(getChildFragmentManager(),
                mBinding.tabFontStyle.getTabCount());

        //Set ViewPager for tabs...
        mBinding.vpFontStyleTabs.setAdapter(adapterWMFontTabs);
        mBinding.tabFontStyle.setupWithViewPager(mBinding.vpFontStyleTabs);
        mBinding.vpFontStyleTabs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                playSound(R.raw.button_tap);
                mBinding.vpFontStyleTabs.setCurrentItem(position, true);
                mBinding.tabFontStyle.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBinding.tabFontStyle.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.vpFontStyleTabs.setCurrentItem(tab.getPosition(), true);
                //Its just a color filter for selected tab...
                tab.getIcon().setColorFilter(context.getResources().getColor(R.color.watermark_color), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#AAAAAA"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //Setting icons for appropriate tab...
        mBinding.tabFontStyle.getTabAt(0).setIcon(R.drawable.ic_font_adjust);
        mBinding.tabFontStyle.getTabAt(0).getIcon().setColorFilter(context.getResources().getColor(R.color.watermark_color),
                PorterDuff.Mode.SRC_IN);
        mBinding.tabFontStyle.getTabAt(1).setIcon(R.drawable.ic_font_type);
        mBinding.tabFontStyle.getTabAt(2).setIcon(R.drawable.ic_shadow);
        mBinding.tabFontStyle.getTabAt(3).setIcon(R.drawable.ic_rotate_tab).setTag("TabColor");

        return mBinding.getRoot();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Convert DP to pixels...
    private float dpToPixel(float pixels) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, displayMetrics);
    }
}
