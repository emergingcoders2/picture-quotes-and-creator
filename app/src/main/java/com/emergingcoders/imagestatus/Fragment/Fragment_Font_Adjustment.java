package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.RepeatListener;
import com.emergingcoders.imagestatus.databinding.DialogFontAlignmentBinding;
import com.emergingcoders.imagestatus.databinding.DialogFontSizeBinding;
import com.emergingcoders.imagestatus.databinding.DialogFrameSizeBinding;
import com.emergingcoders.imagestatus.databinding.DialogLineSpacingBinding;
import com.emergingcoders.imagestatus.databinding.FragmentFontAdjustTabBinding;

import static com.emergingcoders.imagestatus.Globals.Globals.deviceWidth;
import static com.emergingcoders.imagestatus.Globals.Globals.lineSpacing;
import static com.emergingcoders.imagestatus.Globals.Globals.quoteBySize;
import static com.emergingcoders.imagestatus.Globals.Globals.quoteTextSize;
import static com.emergingcoders.imagestatus.Globals.Globals.sectionChildFontGravity;
import static com.emergingcoders.imagestatus.Globals.Globals.sectionChildGravity;
import static com.emergingcoders.imagestatus.Globals.Globals.sectionHeight;
import static com.emergingcoders.imagestatus.Globals.Globals.sectionWidth;

public class Fragment_Font_Adjustment extends Fragment {

    FragmentFontAdjustTabBinding mBinding;

    Activity context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_font_adjust_tab, vcontainer, false);

        context = getActivity();

        //Increase/Decrease quote text size...
        mBinding.btnFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeFontDialog();
                adjustFontSize();
            }
        });

        //Align text in the quote section only...
        mBinding.btnTextAlignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeFontDialog();
                adjustTextAlignment();
            }
        });

        //Adjust line spacing of the text not word spacing!...
        mBinding.btnLineSpacing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeFontDialog();
                adjustLineSpacing();
            }
        });

        //Change quote section/frame size...
        mBinding.btnFrameSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).closeFontDialog();
                setFramSize();
            }
        });

        return mBinding.getRoot();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void adjustFontSize() {
        DialogFontSizeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_font_size,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final int[] textSize = {quoteTextSize};
        final int[] bySize = {quoteBySize};
        binding.ivFontAdjustIncrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                textSize[0]++;
                bySize[0]++;
                ((CreateQuoteActivity) context).tvQuote.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize[0]);
                ((CreateQuoteActivity) context).tvQuoteBy.setTextSize(TypedValue.COMPLEX_UNIT_PX, bySize[0]);
            }
        }));

        binding.ivFontAdjustDecrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quoteBySize > 3) {
                    playSound(R.raw.button_tap);
                    textSize[0]--;
                    bySize[0]--;
                    ((CreateQuoteActivity) context).tvQuote.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize[0]);
                    ((CreateQuoteActivity) context).tvQuoteBy.setTextSize(TypedValue.COMPLEX_UNIT_PX, bySize[0]);
                }
            }
        }));

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateQuoteActivity) context).closeFontDialog();
                playSound(R.raw.button_tap);
                quoteTextSize = textSize[0];
                quoteBySize = bySize[0];
                ((CreateQuoteActivity) context).deploayChanges(true);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvQuote.setTextSize(TypedValue.COMPLEX_UNIT_PX, quoteTextSize);
                ((CreateQuoteActivity) context).tvQuoteBy.setTextSize(TypedValue.COMPLEX_UNIT_PX, quoteBySize);
                ((CreateQuoteActivity) context).setFontStyles();
                dialog.dismiss();
            }
        });
    }

    private void adjustTextAlignment() {
        final DialogFontAlignmentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_font_alignment,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final int[] lastGravity = {sectionChildGravity};
        final int[] lastFontGravity = {sectionChildFontGravity};

        if (sectionChildFontGravity == Gravity.CENTER)
            binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center_black);
        else if (sectionChildFontGravity == Gravity.START)
            binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left_black);
        else
            binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right_black);

        binding.ivLeftAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.START;

                ((CreateQuoteActivity) context).tvQuote.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuoteBy.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuote.setGravity(Gravity.START);
                ((CreateQuoteActivity) context).tvQuoteBy.setGravity(Gravity.START);
                ((CreateQuoteActivity) context).tvQuoteBy.setPadding(0, 30, 0, 0);
                lastGravity[0] = Gravity.START | Gravity.CENTER_VERTICAL;
                lastFontGravity[0] = Gravity.START;

                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left_black);
                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center);
                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right);
            }
        });

        binding.ivCenterAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER;

                ((CreateQuoteActivity) context).tvQuote.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuoteBy.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuote.setGravity(Gravity.CENTER);
                ((CreateQuoteActivity) context).tvQuoteBy.setGravity(Gravity.CENTER);
                lastGravity[0] = Gravity.CENTER;
                lastFontGravity[0] = Gravity.CENTER;
                ((CreateQuoteActivity) context).tvQuoteBy.setPadding(0, 30, 0, 0);

                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center_black);
                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left);
                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right);
            }
        });

        binding.ivRightAlign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.END;

                ((CreateQuoteActivity) context).tvQuote.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuoteBy.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuote.setGravity(Gravity.END);
                ((CreateQuoteActivity) context).tvQuoteBy.setGravity(Gravity.END);
                ((CreateQuoteActivity) context).tvQuoteBy.setPadding(0, 30, 0, 0);
                lastGravity[0] = Gravity.END | Gravity.CENTER_VERTICAL;
                lastFontGravity[0] = Gravity.END;

                binding.ivRightAlign.setImageResource(R.drawable.ic_font_align_right_black);
                binding.ivCenterAlign.setImageResource(R.drawable.ic_font_align_center);
                binding.ivLeftAlign.setImageResource(R.drawable.ic_font_align_left);
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).deploayChanges(true);
                sectionChildGravity = lastGravity[0];
                sectionChildFontGravity = lastFontGravity[0];
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = sectionChildGravity;

                ((CreateQuoteActivity) context).tvQuote.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuoteBy.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).tvQuote.setGravity(sectionChildFontGravity);
                ((CreateQuoteActivity) context).tvQuoteBy.setGravity(sectionChildFontGravity);
                ((CreateQuoteActivity) context).tvQuoteBy.setPadding(0, 30, 0, 0);
                ((CreateQuoteActivity) context).setFontStyles();
                dialog.dismiss();
            }
        });
    }

    private void adjustLineSpacing() {
        DialogLineSpacingBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_line_spacing,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final int[] spacing = {lineSpacing};
        binding.ivIncreaseLineSpacing.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                spacing[0]++;

                ((CreateQuoteActivity) context).tvQuote.setLineSpacing(spacing[0], 1);
            }
        }));

        binding.ivDecreaseLineSpacing.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spacing[0] >= -50) {
                    playSound(R.raw.button_tap);
                    spacing[0]--;

                    ((CreateQuoteActivity) context).tvQuote.setLineSpacing(spacing[0], 1);
                }
            }
        }));

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                lineSpacing = spacing[0];
                ((CreateQuoteActivity) context).deploayChanges(true);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).tvQuote.setLineSpacing(lineSpacing, 1);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });
    }

    private void setFramSize() {
        DialogFrameSizeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_frame_size,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final int[] width = {sectionWidth};
        final int height = sectionHeight;
        binding.ivFrameIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (width[0] < deviceWidth) {
                    playSound(R.raw.button_tap);
                    width[0] += 14;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width[0],
                            height);
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                    ((CreateQuoteActivity) context).frameQuote.setLayoutParams(layoutParams);
                    ((CreateQuoteActivity) context).frameQuote.setGravity(sectionChildGravity);
                    ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(0x33000000);

                    //It is for flashing section when sizing...
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(Color.TRANSPARENT);
                        }
                    }, 100);
                    handler = null;
                }
            }
        });

        binding.ivFrameDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (width[0] > 29) {
                    playSound(R.raw.button_tap);
                    width[0] -= 14;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width[0],
                            height);
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                    ((CreateQuoteActivity) context).frameQuote.setLayoutParams(layoutParams);
                    ((CreateQuoteActivity) context).frameQuote.setGravity(sectionChildGravity);
                    ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(0x33000000);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(Color.TRANSPARENT);
                        }
                    }, 100);
                    handler = null;
                }
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(sectionWidth,
                        sectionHeight);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                ((CreateQuoteActivity) context).frameQuote.setLayoutParams(layoutParams);
                ((CreateQuoteActivity) context).frameQuote.setGravity(sectionChildGravity);
                ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(0x33000000);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((CreateQuoteActivity) context).frameQuote.setBackgroundColor(Color.TRANSPARENT);
                    }
                }, 100);
                handler = null;
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                sectionWidth = width[0];
                sectionHeight = height;
                ((CreateQuoteActivity) context).deploayChanges(true);
                dialog.dismiss();
                ((CreateQuoteActivity) context).setFontStyles();
            }
        });
    }
}
