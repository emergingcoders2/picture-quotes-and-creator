package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emergingcoders.imagestatus.Adapter.Adapter_WM_Font_Family;
import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.RecyclerItemClickListener;
import com.emergingcoders.imagestatus.databinding.FragmentFontFamilyTabBinding;

import static android.content.Context.MODE_PRIVATE;

public class Fragment_WM_Font_Family extends Fragment {
    FragmentFontFamilyTabBinding mBinding;

    Activity context;
    Typeface typeface;
    SharedPreferences preferences;
    int positionApplied = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_font_family_tab,
                vcontainer,
                false);

        context = getActivity();
        preferences = context.getSharedPreferences("MP", MODE_PRIVATE);
        setRetainInstance(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mBinding.rvFontType.setLayoutManager(layoutManager);
        mBinding.rvFontType.setAdapter(((CreateQuoteActivity) getActivity()).adapterWmFontFamily);

        mBinding.rvFontType.addOnItemTouchListener(new RecyclerItemClickListener(context,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        playSound(R.raw.button_tap);
                        mBinding.llConfirmFontFamily.setVisibility(View.VISIBLE);
                        applyFonts(position);
                    }
                }));

        mBinding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).deploayChanges(true);

                String fontPath = ((CreateQuoteActivity) context).fontStyleArrayList.get(positionApplied).getFilePath();
                AppPreferences.setWMFontID(context, positionApplied);
                AppPreferences.setWMFont(context, "fonts/" + fontPath);
                mBinding.llConfirmFontFamily.setVisibility(View.GONE);
            }
        });

        mBinding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                applyFonts(AppPreferences.getWMFontID(context));
                mBinding.llConfirmFontFamily.setVisibility(View.GONE);
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && mBinding != null) {
            applyFonts(AppPreferences.getWMFontID(context));
            mBinding.llConfirmFontFamily.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        applyFonts(AppPreferences.getWMFontID(context));
        mBinding.llConfirmFontFamily.setVisibility(View.GONE);
        super.onDestroyView();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void applyFonts(int position) {
        String fontPath = ((CreateQuoteActivity) context).fontStyleArrayList.get(position).getFilePath();

        AssetManager am = getActivity().getApplicationContext().getAssets();
        typeface = Typeface.createFromAsset(am, "fonts/" + fontPath);

        ((CreateQuoteActivity) context).tvAddWM.setTypeface(typeface);

        Adapter_WM_Font_Family.selectedID = position;
        positionApplied = position;

        ((CreateQuoteActivity) context).adapterWmFontFamily.notifyDataSetChanged();
        mBinding.llConfirmFontFamily.setVisibility(View.VISIBLE);
    }
}
