package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.databinding.FragmentWmRotateTabBinding;

import static com.emergingcoders.imagestatus.Globals.Globals.rotateMultiplier;

public class Fragment_WM_Rotation extends Fragment {
    FragmentWmRotateTabBinding mBinding;

    Activity context;
    int lastRotateProgress = 0, lastRotate = 24;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_wm_rotate_tab,
                vcontainer,
                false);

        context = getActivity();

        mBinding.sbFrameRotation.setProgress(AppPreferences.getWMLastRotateProgress(context));
        lastRotate = AppPreferences.getWMLastRotate(context);
        lastRotateProgress = AppPreferences.getWMLastRotateProgress(context);
        mBinding.sbFrameRotation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rotateQuote(progress);
                mBinding.llConfirmRotateQuote.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                AppPreferences.setWMLastRotateProgress(context, lastRotateProgress);
                AppPreferences.setWMLastRotate(context, lastRotate);
                ((CreateQuoteActivity) context).deploayChanges(true);
                mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            }
        });

        mBinding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.sbFrameRotation.setProgress(AppPreferences.getWMLastRotateProgress(context));
                rotateQuote(AppPreferences.getWMLastRotateProgress(context));
                mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        rotateQuote(AppPreferences.getWMLastRotateProgress(context));
        mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && mBinding != null) {
            rotateQuote(AppPreferences.getWMLastRotateProgress(context));
            mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            mBinding.sbFrameRotation.setProgress(AppPreferences.getWMLastRotateProgress(context));
        }
    }

    private void rotateQuote(int progress) {
        lastRotate = progress - 24;
        lastRotateProgress = progress;
        ((CreateQuoteActivity) getActivity()).tvAddWM.setRotation(lastRotate * rotateMultiplier);
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
