package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.databinding.FragmentWmFontShadowTabBinding;

public class Fragment_WM_Font_Shadows extends Fragment {
    FragmentWmFontShadowTabBinding mBinding;

    Activity context;

    private int lastProgressX = 11, lastProgressY = 11, lastProgressRadius = 2, lastX = 0, lastY = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_wm_font_shadow_tab,
                vcontainer,
                false);

        context = getActivity();

        lastProgressX = AppPreferences.getWMLastProgressX(context);
        lastProgressY = AppPreferences.getWMLastProgressY(context);
        lastProgressRadius = AppPreferences.getWMLastProgressRadius(context);
        lastX = AppPreferences.getWMLastX(context);
        lastY = AppPreferences.getWMLastY(context);

        mBinding.sbUpDownShadow.setProgress(lastProgressY);
        mBinding.sbLeftRightShadow.setProgress(lastProgressX);
        mBinding.sbBlurShadow.setProgress(lastProgressRadius);

        //This seekbar for left-right shadow...
        mBinding.sbLeftRightShadow.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.llConfirmFontShadow.setVisibility(View.VISIBLE);
                applyLeftRightShadow(progress, lastY, lastProgressRadius);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //This seekbar for up-down shadow...
        mBinding.sbUpDownShadow.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.llConfirmFontShadow.setVisibility(View.VISIBLE);
                applyUpDownShadow(progress, lastX, lastProgressRadius);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //This seekbar for set bluritiy of shadow...
        mBinding.sbBlurShadow.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.llConfirmFontShadow.setVisibility(View.VISIBLE);
                applyBlurShadow(progress, lastX, lastY);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                AppPreferences.setWMLastProgressX(context, lastProgressX);
                AppPreferences.setWMLastProgressY(context, lastProgressY);
                AppPreferences.setWMLastProgressRadius(context, lastProgressRadius);
                AppPreferences.setWMLastX(context, lastX);
                AppPreferences.setWMLastY(context, lastY);
                ((CreateQuoteActivity) context).deploayChanges(true);
                mBinding.llConfirmFontShadow.setVisibility(View.GONE);
            }
        });

        mBinding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.sbUpDownShadow.setProgress(AppPreferences.getWMLastProgressY(context));
                mBinding.sbLeftRightShadow.setProgress(AppPreferences.getWMLastProgressX(context));
                mBinding.sbBlurShadow.setProgress(AppPreferences.getWMLastProgressRadius(context));
                mBinding.llConfirmFontShadow.setVisibility(View.GONE);
                applyBlurShadow(AppPreferences.getWMLastProgressRadius(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastY(context));
                applyLeftRightShadow(AppPreferences.getWMLastProgressX(context), AppPreferences.getWMLastY(context), AppPreferences.getWMLastProgressRadius(context));
                applyUpDownShadow(AppPreferences.getWMLastProgressY(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastProgressRadius(context));
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mBinding.sbUpDownShadow.setProgress(AppPreferences.getWMLastProgressY(context));
        mBinding.sbLeftRightShadow.setProgress(AppPreferences.getWMLastProgressX(context));
        mBinding.sbBlurShadow.setProgress(AppPreferences.getWMLastProgressRadius(context));
        applyBlurShadow(AppPreferences.getWMLastProgressRadius(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastY(context));
        applyLeftRightShadow(AppPreferences.getWMLastProgressX(context), AppPreferences.getWMLastY(context), AppPreferences.getWMLastProgressRadius(context));
        applyUpDownShadow(AppPreferences.getWMLastProgressY(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastProgressRadius(context));
        mBinding.llConfirmFontShadow.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && mBinding != null) {
            mBinding.sbUpDownShadow.setProgress(AppPreferences.getWMLastProgressY(context));
            mBinding.sbLeftRightShadow.setProgress(AppPreferences.getWMLastProgressX(context));
            mBinding.sbBlurShadow.setProgress(AppPreferences.getWMLastProgressRadius(context));
            applyBlurShadow(AppPreferences.getWMLastProgressRadius(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastY(context));
            applyLeftRightShadow(AppPreferences.getWMLastProgressX(context), AppPreferences.getWMLastY(context), AppPreferences.getWMLastProgressRadius(context));
            applyUpDownShadow(AppPreferences.getWMLastProgressY(context), AppPreferences.getWMLastX(context), AppPreferences.getWMLastProgressRadius(context));
            mBinding.llConfirmFontShadow.setVisibility(View.GONE);
        }
    }

    private void applyUpDownShadow(int lastProgressY, int lastX, int lastProgressRadius) {
        if (lastProgressY > 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastX, lastProgressY, AppPreferences.getWMShadowColor(context));
            this.lastProgressY = lastProgressY;
            lastY = lastProgressY;
        } else if (lastProgressY == 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastX, 0, AppPreferences.getWMShadowColor(context));
            this.lastProgressY = 11;
            lastY = 0;
        } else if (lastProgressY < 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastX, lastProgressY - 11, AppPreferences.getWMShadowColor(context));
            this.lastProgressY = lastProgressY;
            lastY = lastProgressY - 11;
        }
    }

    private void applyLeftRightShadow(int lastProgressX, int lastY, int lastProgressRadius) {
        if (lastProgressX > 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastProgressX, lastY, AppPreferences.getWMShadowColor(context));
            this.lastProgressX = lastProgressX;
            lastX = lastProgressX;
        } else if (lastProgressX == 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, 0, lastY, AppPreferences.getWMShadowColor(context));
            this.lastProgressX = 11;
            lastX = 0;
        } else if (lastProgressX < 11) {
            ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastProgressX - 11, lastY, AppPreferences.getWMShadowColor(context));
            this.lastProgressX = lastProgressX;
            lastX = lastProgressX - 11;
        }
    }

    private void applyBlurShadow(int lastProgressRadius, int lastX, int lastY) {
        ((CreateQuoteActivity) context).tvAddWM.setShadowLayer(lastProgressRadius, lastX, lastY, AppPreferences.getWMShadowColor(context));
        this.lastProgressRadius = lastProgressRadius;
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
