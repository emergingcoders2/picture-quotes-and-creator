package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.databinding.FragmentQuoteRotateTabBinding;

import static com.emergingcoders.imagestatus.Globals.Globals.lastRotateProgress;
import static com.emergingcoders.imagestatus.Globals.Globals.rotateMultiplier;

public class Fragment_Quote_Rotate extends Fragment {

    FragmentQuoteRotateTabBinding mBinding;

    Activity context;
    int progressLast = 0, lastRotate = 24;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_quote_rotate_tab,
                vcontainer,
                false);

        context = getActivity();

        mBinding.sbFrameRotation.setProgress(lastRotateProgress);
        lastRotate = Globals.lastRotate;
        progressLast = lastRotateProgress;
        mBinding.sbFrameRotation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rotateQuote(progress);
                mBinding.llConfirmRotateQuote.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                lastRotateProgress = progressLast;
                Globals.lastRotate = lastRotate;
                ((CreateQuoteActivity) context).deploayChanges(true);
                mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            }
        });

        mBinding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.sbFrameRotation.setProgress(lastRotateProgress);
                rotateQuote(lastRotateProgress);
                mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        rotateQuote(lastRotateProgress);
        mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && mBinding != null) {
            rotateQuote(lastRotateProgress);
            mBinding.llConfirmRotateQuote.setVisibility(View.GONE);
            mBinding.sbFrameRotation.setProgress(lastRotateProgress);
        }
    }

    private void rotateQuote(int progress) {
        lastRotate = progress - 24;
        progressLast = progress;
        ((CreateQuoteActivity) getActivity()).frameQuote.setRotation(lastRotate * rotateMultiplier);
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
