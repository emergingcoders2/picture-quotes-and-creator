package com.emergingcoders.imagestatus.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
<<<<<<< HEAD
=======
import android.support.v7.widget.Toolbar;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emergingcoders.imagestatus.Adapter.Adapter_Font_Family;
import com.emergingcoders.imagestatus.Adapter.Adapter_WM_Font_Family;
import com.emergingcoders.imagestatus.Design.CreateQuoteActivity;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.RecyclerItemClickListener;
import com.emergingcoders.imagestatus.databinding.FragmentFontFamilyTabBinding;

import static android.content.Context.MODE_PRIVATE;
import static com.emergingcoders.imagestatus.Globals.Globals.fontFamilyPosition;

/**
 * Created by mr on 15/12/17.
 * This tab is for all font types...
 */

public class Fragment_Font_Family extends Fragment {

    FragmentFontFamilyTabBinding mBinding;
    TextView tvQuote, tvQuoteBy;

    Activity context;
    Typeface typeface;
    SharedPreferences preferences;
    int positionApplied = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup vcontainer, @Nullable Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_font_family_tab,
                vcontainer,
                false);

        context = getActivity();
        preferences = context.getSharedPreferences("MP", MODE_PRIVATE);
        setRetainInstance(true);

        tvQuote = getActivity().findViewById(R.id.tv_create_quote_text);
        tvQuoteBy = getActivity().findViewById(R.id.tv_create_quote_by);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mBinding.rvFontType.setLayoutManager(layoutManager);
        mBinding.rvFontType.setAdapter(((CreateQuoteActivity) getActivity()).adapterFontType2);

        mBinding.rvFontType.addOnItemTouchListener(new RecyclerItemClickListener(context,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        playSound(R.raw.button_tap);
                        mBinding.llConfirmFontFamily.setVisibility(View.VISIBLE);
                        applyFonts(position);
                    }
                }));

        mBinding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                ((CreateQuoteActivity) context).deploayChanges(true);
<<<<<<< HEAD
                Adapter_WM_Font_Family.selectedID = AppPreferences.getWMFontID(context);
                fontFamilyPosition = positionApplied;

                String fontPath = ((CreateQuoteActivity) context).fontStyleArrayList.get(positionApplied).getFilePath();
=======
                fontFamilyPosition = positionApplied;

                String fontPath = ((CreateQuoteActivity) context).fontStyleArrayList.get(fontFamilyPosition).getFilePath();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("Fonts", "fonts/" + fontPath);
                editor.putInt("FontID", positionApplied);
                editor.apply();
                mBinding.llConfirmFontFamily.setVisibility(View.GONE);
            }
        });

        mBinding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                applyFonts(fontFamilyPosition);
                mBinding.llConfirmFontFamily.setVisibility(View.GONE);
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && mBinding != null) {
            applyFonts(fontFamilyPosition);
            mBinding.llConfirmFontFamily.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        applyFonts(fontFamilyPosition);
        mBinding.llConfirmFontFamily.setVisibility(View.GONE);
        super.onDestroyView();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void applyFonts(int position) {
        String fontPath = ((CreateQuoteActivity) context).fontStyleArrayList.get(position).getFilePath();

        AssetManager am = getActivity().getApplicationContext().getAssets();
        typeface = Typeface.createFromAsset(am, "fonts/" + fontPath);

        tvQuote.setTypeface(typeface);
        tvQuoteBy.setTypeface(typeface);

<<<<<<< HEAD
        Adapter_Font_Family.selectedID = position;
=======
        ((CreateQuoteActivity) context).adapterFontType2.selectedID = position;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        positionApplied = position;

        ((CreateQuoteActivity) context).adapterFontType2.notifyDataSetChanged();
        mBinding.llConfirmFontFamily.setVisibility(View.VISIBLE);
    }
}
