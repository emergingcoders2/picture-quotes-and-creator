package com.emergingcoders.imagestatus.Retrofit;

import com.emergingcoders.imagestatus.Model.ModelSingleLike;
import com.emergingcoders.imagestatus.Model.Model_App_Feedback;
import com.emergingcoders.imagestatus.Model.Model_Liked_Quotes;
import com.emergingcoders.imagestatus.Model.Model_Quotes_List;
import com.emergingcoders.imagestatus.Model.Model_Status_Category;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitInterfaces {

    @POST("categories")
    Call<Model_Status_Category> getCategory();

    @FormUrlEncoded
    @POST("quotes")
    Call<Model_Quotes_List> getQuotesList(@Field("cat_id") String cat_id,
                                          @Field("quote_ids") String stored_id,
                                          @Field("device_id") String deviceID);

    @FormUrlEncoded
    @POST("quotes")
    Call<Model_Quotes_List> getRelatedQuotesList(@Field("cat_id") int cat_id,
<<<<<<< HEAD
                                                    @Field("quote_ids") String stored_id,
                                                    @Field("device_id") String deviceID);
=======
                                               @Field("quote_ids") String stored_id,
                                               @Field("device_id") String deviceID);
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

    @FormUrlEncoded
    @POST("liked-quotes")
    Call<Model_Liked_Quotes> getLikedQuotes(@Field("page") int page,
                                            @Field("device_id") String deviceID);

    @FormUrlEncoded
    @POST("like-unlike-quote")
    Call<Model_Liked_Quotes> setLikedQuotes(@Field("quote_id") int liked_ids,
                                            @Field("flag") int flag,
                                            @Field("device_id") String deviceID);

    @FormUrlEncoded
    @POST("feedback")
    Call<Model_App_Feedback> sendFeedback(@Field("name") String name,
                                          @Field("email") String email,
                                          @Field("message") String message);

    @FormUrlEncoded
    @POST("is-quote-liked")
    Call<ModelSingleLike> getSingleLike(@Field("quote_id") int id,
                                        @Field("device_id") String deviceID);
}