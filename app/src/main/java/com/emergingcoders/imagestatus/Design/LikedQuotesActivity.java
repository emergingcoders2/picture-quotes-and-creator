package com.emergingcoders.imagestatus.Design;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Adapter.Adapter_Liked_Quotes;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.ModelLikedQuotes;
import com.emergingcoders.imagestatus.Model.Model_Liked_Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Retrofit.RetrofitClient;
import com.emergingcoders.imagestatus.Retrofit.RetrofitInterfaces;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.EndlessRecyclerViewScrollListener;
import com.emergingcoders.imagestatus.Utils.InternetConnection;
<<<<<<< HEAD
=======
import com.emergingcoders.imagestatus.Utils.QuotesUtils;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.databinding.ActivityLikedQuotesBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutNoInternetBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LikedQuotesActivity extends AppCompatActivity {

    ActivityLikedQuotesBinding mBinding;

    Activity mContext;
    public String strLoadedID = "";
    int previousPosition = 0;
    public int pageID = 1;
    boolean flagLoadMore = false;
    private int SDK;
    private String DEVICE_ID;

    Adapter_Liked_Quotes adapterLikedQuotes;
    ArrayList<ModelLikedQuotes> likedQuotesArrayList = new ArrayList<>();

    EndlessRecyclerViewScrollListener scrollListener;

    InterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd interstitialAd;
    AdView adViewFB;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    boolean isAdInitialized = false;

    final String BROADCAST_LIKE_QUOTE = "BROADCAST_LIKE_QUOTE";
    BroadcastReceiver brLikeQuote;
    boolean flagBRLike = false, isShowDialog = false;

    @SuppressLint({"SetTextI18n", "HardwareIds"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_liked_quotes);

        SDK = Build.VERSION.SDK_INT;
        mContext = LikedQuotesActivity.this;

        //Set Title...
        mBinding.toolbarParentLayout.tvAppTitle.setText("Liked Quotes");

        DEVICE_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //Set Loders...
        mBinding.pbLoading.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        mBinding.pbLoadMore.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);
        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Set RecyclerView...
        final StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        mBinding.rvLikedQuotes.setLayoutManager(staggeredGridLayoutManager);

        adapterLikedQuotes = new Adapter_Liked_Quotes(likedQuotesArrayList,
                R.layout.row_layout_liked_quotes,
                mContext);
        mBinding.rvLikedQuotes.setAdapter(adapterLikedQuotes);

        //Set Endless RecyclerView...
        scrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (likedQuotesArrayList.size() > 19) {
                    pageID++;

                    if (SDK > 19)
                        mBinding.cvLoadMore.setVisibility(View.VISIBLE);
                    else
                        mBinding.pbLoadMore.setVisibility(View.VISIBLE);

                    flagLoadMore = true;
                    getLikedQuotes(pageID, DEVICE_ID);
                }
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);

                int[] firstVisiblePosition = staggeredGridLayoutManager.findFirstVisibleItemPositions(null);

                if (firstVisiblePosition[0] > 10)
                    mBinding.fab.show();
                else
                    mBinding.fab.hide();
            }
        };

        brLikeQuote = new BroadcastReceiver() {
            @SuppressLint("HardwareIds")
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getBooleanExtra("isUnLiked", false)) {
                    if (SDK < 20)
                        mBinding.pbLoading.setVisibility(View.VISIBLE);
                    else
                        mBinding.cvLoading.setVisibility(View.VISIBLE);

                    flagBRLike = true;

                    likedQuotesArrayList.clear();
                    adapterLikedQuotes.notifyDataSetChanged();

                    scrollListener.resetState();

                    previousPosition = 0;
                    pageID = 1;

                    getLikedQuotes(pageID, DEVICE_ID);
                }
            }
        };
        registerReceiver(brLikeQuote, new IntentFilter(BROADCAST_LIKE_QUOTE));

        //Back button top...
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        //Floating button for go to top of the list...
        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.rvLikedQuotes.smoothScrollToPosition(0);
            }
        });

        getLikedQuotes(pageID, DEVICE_ID);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (flagBRLike)
            unregisterReceiver(brLikeQuote);
        if (mBinding.adView != null)
            mBinding.adView.destroy();
        if (adViewFB != null)
            adViewFB.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.adView != null)
            mBinding.adView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Globals.flagRemoveAds)
            if (mBinding.adView != null)
                mBinding.adView.resume();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Ad settings...
    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                    requestNewInterstitial();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mBinding.adView.loadAd(adRequest);
            mBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                mBinding.adView.loadAd(adRequest);
                mBinding.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private void requestNewInterstitial() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    String isShow = mFirebaseRemoteConfig.getString("status");
                    if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                        if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                        if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    }
                }
            });
            mInterstitialAd.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd = new com.facebook.ads.InterstitialAd(mContext,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd.loadAd();
                    interstitialAd.setAdListener(new AbstractAdListener() {
                        @Override
                        public void onAdLoaded(Ad ad) {
                            super.onAdLoaded(ad);
                            String isShow = mFirebaseRemoteConfig.getString("status");
                            if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                                if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                                if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                        Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                    if (interstitialAd.isAdLoaded())
                                        interstitialAd.show();

<<<<<<< HEAD
                                    AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                    QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        String isShow = mFirebaseRemoteConfig.getString("status");
                        if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                            if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                            if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                    Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                if (mInterstitialAd.isLoaded())
                                    mInterstitialAd.show();

<<<<<<< HEAD
                                AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                            }
                        }
                    }
                });
                mInterstitialAd.loadAd(adRequest2);
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    //API Call Region...
    public void getLikedQuotes(final int pageID, String deviceID) {
        if (InternetConnection.checkConnection(mContext)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getClient().create(RetrofitInterfaces.class);

            Call<Model_Liked_Quotes> call = retrofitInterfaces.getLikedQuotes(pageID, deviceID);
            call.enqueue(new Callback<Model_Liked_Quotes>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<Model_Liked_Quotes> call, Response<Model_Liked_Quotes> response) {
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("success")) {
                        if (!flagLoadMore)
                            likedQuotesArrayList.clear();

                        likedQuotesArrayList.addAll(response.body().getQoutesList());
                        adapterLikedQuotes.notifyItemRangeChanged(previousPosition, likedQuotesArrayList.size());
                        mBinding.tvNoQuotes.setVisibility(View.GONE);

                        for (int i = 0; i < likedQuotesArrayList.size(); i++)
                            strLoadedID = strLoadedID + "," + likedQuotesArrayList.get(i).getId();

                        Log.e("LoadedID>>>", strLoadedID);

                        previousPosition = likedQuotesArrayList.size();

                        if (flagLoadMore) {
                            flagLoadMore = false;
                            if (SDK > 19)
                                mBinding.cvLoadMore.setVisibility(View.GONE);
                            else
                                mBinding.pbLoadMore.setVisibility(View.GONE);
                        } else {
                            if (SDK > 19)
                                mBinding.cvLoading.setVisibility(View.GONE);
                            else
                                mBinding.pbLoading.setVisibility(View.GONE);
                            mBinding.rvLikedQuotes.addOnScrollListener(scrollListener);
                        }
                    } else {
                        //Log.e("LikedQuotes>>>", response.toString());
                        if (!flagLoadMore) {
                            if (SDK > 19)
                                mBinding.cvLoading.setVisibility(View.GONE);
                            else mBinding.pbLoading.setVisibility(View.GONE);

                            mBinding.tvNoQuotes.setText("There are no liked quotes to show!");
                            mBinding.tvNoQuotes.setVisibility(View.VISIBLE);

                            likedQuotesArrayList.clear();
                            adapterLikedQuotes.notifyDataSetChanged();
                        } else {
                            flagLoadMore = false;
                            if (SDK > 19)
                                mBinding.cvLoadMore.setVisibility(View.GONE);
                            else
                                mBinding.pbLoadMore.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Model_Liked_Quotes> call, Throwable t) {
                    Crashlytics.log("LikedQuotes>>>" + t.getMessage());
                    if (flagLoadMore) {
                        flagLoadMore = false;
                        if (SDK > 19)
                            mBinding.cvLoadMore.setVisibility(View.GONE);
                        else
                            mBinding.pbLoadMore.setVisibility(View.GONE);
                    } else {
                        if (SDK > 19)
                            mBinding.cvLoading.setVisibility(View.GONE);
                        else
                            mBinding.pbLoading.setVisibility(View.GONE);
                    }
                    if (!isShowDialog)
                        showPopup(true);
                }
            });
        } else {
            if (flagLoadMore) {
                flagLoadMore = false;
                if (SDK > 19)
                    mBinding.cvLoadMore.setVisibility(View.GONE);
                else
                    mBinding.pbLoadMore.setVisibility(View.GONE);
            } else {
                if (SDK > 19)
                    mBinding.cvLoading.setVisibility(View.GONE);
                else
                    mBinding.pbLoading.setVisibility(View.GONE);
            }
            if (!isShowDialog)
                showPopup(false);
        }
    }

    //Try Again Popup...
    @SuppressLint("SetTextI18n")
    public void showPopup(boolean failed) {
        isShowDialog = true;
        //Setting loading layouts...
        final DialogLayoutNoInternetBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_layout_no_internet,
                null,
                false);
        final Dialog dialog;
        dialog = new Dialog(mContext, R.style.MY_DIALOG_Feedback);
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
<<<<<<< HEAD
        if (!isFinishing())
            dialog.show();
=======
        dialog.show();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        binding.cvCreateQuotes.setAnimation(zoomin);
        binding.cvCreateQuotes.setAnimation(zoomout);

        binding.cvCreateQuotes.startAnimation(zoomin);

        zoomin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomout);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomin);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        if (failed)
<<<<<<< HEAD
            binding.tvNoInternetDialog.setText("Connection problem!");
=======
            binding.tvNoInternetDialog.setText("Server Connection problem!");
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        else
            binding.tvNoInternetDialog.setText("No internet connection!");

        binding.cvTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isShowDialog = false;
                if (!flagLoadMore) {
                    if (SDK > 19)
                        mBinding.cvLoading.setVisibility(View.VISIBLE);
                    else
                        mBinding.pbLoading.setVisibility(View.VISIBLE);

                    dialog.dismiss();
                    getLikedQuotes(pageID, DEVICE_ID);
                } else {
                    if (SDK > 19)
                        mBinding.cvLoadMore.setVisibility(View.VISIBLE);
                    else
                        mBinding.pbLoadMore.setVisibility(View.VISIBLE);

                    dialog.dismiss();
                    getLikedQuotes(pageID, DEVICE_ID);
                }
            }
        });

        binding.cvCreateQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isShowDialog = false;
                Intent intent = new Intent(mContext, CreateQuoteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        binding.cvQuitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isShowDialog = false;
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });
    }
}
