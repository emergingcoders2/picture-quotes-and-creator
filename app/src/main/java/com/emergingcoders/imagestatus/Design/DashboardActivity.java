package com.emergingcoders.imagestatus.Design;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Adapter.Adapter_Quotes_List;
import com.emergingcoders.imagestatus.Adapter.Adapter_Status_Category;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.ModelQuotesList;
import com.emergingcoders.imagestatus.Model.Model_Quotes_List;
import com.emergingcoders.imagestatus.Model.Model_Status_Category;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Retrofit.RetrofitClient;
import com.emergingcoders.imagestatus.Retrofit.RetrofitInterfaces;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.EndlessRecyclerViewScrollListener;
import com.emergingcoders.imagestatus.Utils.InternetConnection;
<<<<<<< HEAD
=======
import com.emergingcoders.imagestatus.Utils.QuotesUtils;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.databinding.ActivityDashboardBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutNoInternetBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DashboardActivity extends AppCompatActivity {

    ActivityDashboardBinding mBinding;
    ActionBarDrawerToggle mDrawerToggle;
    private int SDK;

    Activity mContext;
    Adapter_Quotes_List adapterQuotesList;
    ArrayList<ModelQuotesList> quotesListArrayList = new ArrayList<>();

    EndlessRecyclerViewScrollListener scrollListener;
    Typeface typeface;

    InterstitialAd mInterstitialAd, mInterstitialAd2;
    private com.facebook.ads.InterstitialAd interstitialAd, interstitialAd2;
    AdView adViewFB;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    boolean isAdInitialized = false;
    final static int REQUEST_AD = 100;

    SharedPreferences preferences;
    private String strLoadedID = "";
    public String strCatID = "0", strCatName = "";
    Dialog popupWindow;
    String flagDialogNo = "", flagDialogYes = "";
    boolean doubleBackToExitPressedOnce = false, flagLoadMore = false, flagCategory = false, flagSwipe = false;
    Handler handler;
    RecyclerView.LayoutManager layoutManager2;

    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    int previousPosition = 0, BILLING_RESPONSE_RESULT_OK = 0;
    ArrayList<String> skuList = new ArrayList<>();

    String BROADCAST_REMOVE_ADS = "com.emergingcoders.imagestatus.Design.PremiumFeaturesActivity2";

    public static int countFailed = 0;
    String DEVICE_ID,
            tokenRWM = "",
            tokenRAds = "";

    SharedPreferences.Editor editor;
    IInAppBillingService mService;
    BroadcastReceiver brInApp;
    boolean flagBRInApp = false, isShowDialog = false;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            /*try {
                mService.consumePurchase(3, getPackageName(), tokenRWM);
                mService.consumePurchase(3, getPackageName(), tokenRAds);
            } catch (RemoteException e) {
                e.printStackTrace();
            }*/
            getInAppProducts();
        }
    };

    @SuppressLint({"SetTextI18n", "HardwareIds"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        SDK = Build.VERSION.SDK_INT;
        mContext = DashboardActivity.this;
        preferences = getSharedPreferences("MP", MODE_PRIVATE);

        mBinding.appBarLayout.tvAppTitle.setText(getResources().getString(R.string.picture_quotes_creator));

        //Set Device id...
        DEVICE_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //Set Progress bar....
        mBinding.appBarLayout.contentLayout.pbLoading.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        mBinding.appBarLayout.contentLayout.pbLoadMore.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        //Configure Firebase config for Interstitial ads...
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        final FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        mBinding.rvCategory.setLayoutManager(layoutManager);
        mBinding.drawerLayout.closeDrawers();

        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        layoutManager2 = staggeredGridLayoutManager;
        mBinding.appBarLayout.contentLayout.rvQuoteList.setLayoutManager(layoutManager2);

        adapterQuotesList = new Adapter_Quotes_List(quotesListArrayList,
                R.layout.row_layout_quotes_list,
                mContext);
        mBinding.appBarLayout.contentLayout.rvQuoteList.setAdapter(adapterQuotesList);

        mDrawerToggle = new ActionBarDrawerToggle(mContext, mBinding.drawerLayout, R.string.drawer_open, R.string.drawer_close);
        mBinding.drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mBinding.appBarLayout.ivNavButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        scrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (!flagSwipe) {
                    setLoadMore(true);
                    flagLoadMore = true;
                    getQuotesList(strCatID, strLoadedID, DEVICE_ID);
                }
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);
                if (!flagSwipe) {

                    int[] firstVisiblePosition = staggeredGridLayoutManager.findFirstVisibleItemPositions(null);

                    if (firstVisiblePosition[0] > 10)
                        mBinding.appBarLayout.contentLayout.fab.show();
                    else
                        mBinding.appBarLayout.contentLayout.fab.hide();
                }
            }
        };

        mBinding.appBarLayout.contentLayout.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.appBarLayout.contentLayout.rvQuoteList.smoothScrollToPosition(0);
            }
        });

        mBinding.appBarLayout.contentLayout.swipeQuotesList.setColorSchemeColors(getResources().getIntArray(R.array.androidcolors));
        mBinding.appBarLayout.contentLayout.swipeQuotesList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mBinding.appBarLayout.contentLayout.swipeQuotesList.setRefreshing(false);
                if (!flagLoadMore) {
                    flagSwipe = true;
                    strLoadedID = "";
                    previousPosition = 0;
                    setLoading(true);

                    mBinding.appBarLayout.contentLayout.rvQuoteList.removeOnScrollListener(scrollListener);

                    quotesListArrayList.clear();
                    adapterQuotesList.notifyDataSetChanged();

                    getQuotesList(strCatID, "", DEVICE_ID);
                }
            }
        });

        mBinding.btnLikedQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.drawerLayout.closeDrawers();
                Intent intent = new Intent(mContext, LikedQuotesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mBinding.btnRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(goToMarket);
                } else {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(mContext.getResources().getString(R.string.app_playstore_path))));
                }
            }
        });

        mBinding.btnShareApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                if (!Globals.flagRemoveAds)
                    requestNewInterstitial2();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + mContext.getResources().getString(R.string.app_name));
                intent.putExtra(Intent.EXTRA_TEXT, mContext.getResources().getString(R.string.share_text) + "\n" +
                        "\nClick here to install:\n" +
                        mContext.getResources().getString(R.string.app_playstore_path));
                intent.setType("text/plain");
                startActivityForResult(Intent.createChooser(intent, "Share link!"), REQUEST_AD);
            }
        });

        mBinding.appBarLayout.ivCreateQuoteDash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(mContext, CreateQuoteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mBinding.cvCreateQuotesNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.drawerLayout.closeDrawers();
                Intent intent = new Intent(mContext, CreateQuoteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mBinding.cbSounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreferences.setSoundOn(mContext, mBinding.cbSounds.isChecked());
                playSound(R.raw.button_tap);
            }
        });

        mBinding.cvInAppNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.drawerLayout.closeDrawers();
                Intent intent = new Intent(mContext, PremiumFeaturesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        brInApp = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                flagBRInApp = true;
<<<<<<< HEAD
                if (mBinding.appBarLayout.contentLayout.adView != null) {
                    mBinding.appBarLayout.contentLayout.adView.destroy();
                    mBinding.appBarLayout.contentLayout.adView.setVisibility(View.GONE);
                }
                if (adViewFB != null)
                    adViewFB.destroy();
=======
                if (Globals.flagRemoveAds) {
                    if (mBinding.appBarLayout.contentLayout.adView != null)
                        mBinding.appBarLayout.contentLayout.adView.destroy();
                    else if (adViewFB != null)
                        adViewFB.destroy();
                }
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
            }
        };
        registerReceiver(brInApp, new IntentFilter(BROADCAST_REMOVE_ADS));

        //Don't remove these click events it causes the back image click issue!...
        mBinding.ivDashLogo.setOnClickListener(null);
        mBinding.llNavHeaderSection.setOnClickListener(null);

        if (!InternetConnection.checkConnection(mContext)) {
<<<<<<< HEAD
            if (!preferences.getBoolean("isPolicyAccepted", false)) {
                if (!hasPermissions(mContext, PERMISSIONS))
                    showPolicyDialog();
            } else
=======
            if (!preferences.getBoolean("isPolicyAccepted", false))
                showPolicyDialog();
            else
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                showPopup(false);
        } else {
            getCategory();
            getQuotesList(strCatID, "", DEVICE_ID);
            if (!preferences.getBoolean("isPolicyAccepted", false))
                if (!hasPermissions(mContext, PERMISSIONS))
                    showPolicyDialog();
        }

        if (AppPreferences.isSoundOn(mContext))
            mBinding.cbSounds.setChecked(true);
        else
            mBinding.cbSounds.setChecked(false);

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        //Setup for Rat us dialog. Calculating date for showing dialog after 3 days...
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String strDate = df.format(calendar.getTime());
        Log.e("Today'sDate>>>", strDate);
        if (preferences.getString("Date", null) != null) {
            if (!preferences.getString("Date", "").equalsIgnoreCase(strDate)) {
                editor = preferences.edit();
                editor.putString("Date", strDate);
                editor.putString("CatName", "Picture Status");
                int count = preferences.getInt("Count", 0);
                editor.putInt("Count", count + 1);
                editor.apply();
            }
        } else {
            editor = preferences.edit();
            editor.putString("Date", strDate);
            editor.putString("CatName", "Picture Status");
            editor.putInt("Count", 1);
            editor.putString("Rate", "false");
            editor.apply();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
            mBinding.drawerLayout.closeDrawers();
        else {
            if (preferences.getString("Rate", "").equalsIgnoreCase("false")) {
                if (preferences.getInt("Count", 0) > 2) {
                    popupWindow = new Dialog(mContext, R.style.MY_DIALOG_Feedback);
                    popupWindow.setCancelable(false);
                    popupWindow.setContentView(R.layout.dialog_layout_rate_app);
                    popupWindow.show();

                    final Button btnNo = popupWindow.findViewById(R.id.btn_dialog_no);
                    final Button btnYes = popupWindow.findViewById(R.id.btn_dialog_yes);
                    final TextView tvTitle = popupWindow.findViewById(R.id.tv_dialog_title);

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onClick(View view) {
                            playSound(R.raw.button_tap);
                            if (flagDialogNo.equalsIgnoreCase("")) {
                                btnNo.setText("No, thanks");
                                btnYes.setText("Ok, sure");
                                tvTitle.setText("Would you mind giving us some feedback?");
                                flagDialogNo = "No";
                                flagDialogYes = "Feedback";
                            } else {
                                popupWindow.dismiss();
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("Rate", "true");
                                editor.apply();
                            }
                        }
                    });
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onClick(View view) {
                            playSound(R.raw.button_tap);
                            if (flagDialogYes.equalsIgnoreCase("")) {
                                btnNo.setText("No, thanks");
                                btnYes.setText("Ok, sure");
                                tvTitle.setText("How about a rating on the Play Store, then?");
                                flagDialogYes = "Yes";
                                flagDialogNo = "Yes";
                            } else if (flagDialogYes.equalsIgnoreCase("Yes")) {
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("Rate", "true");
                                editor.apply();
                                popupWindow.dismiss();
                                rateMyApp();
                            } else if (flagDialogYes.equalsIgnoreCase("Feedback")) {
                                popupWindow.dismiss();
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("Rate", "true");
                                editor.apply();
                                Intent intent = new Intent(mContext, AppFeedbackActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }
                    });
                } else {
                    if (doubleBackToExitPressedOnce) {
                        handler = null;
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        System.exit(0);
                        return;
                    }

                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                }
            } else {
                if (doubleBackToExitPressedOnce) {
                    handler = null;
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();

                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (flagBRInApp) {
            unregisterReceiver(brInApp);
            flagBRInApp = false;
        }
        if (mBinding.appBarLayout.contentLayout.adView != null)
            mBinding.appBarLayout.contentLayout.adView.destroy();
        if (adViewFB != null)
            adViewFB.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding.appBarLayout.contentLayout.adView != null)
            mBinding.appBarLayout.contentLayout.adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.appBarLayout.contentLayout.adView != null)
            mBinding.appBarLayout.contentLayout.adView.pause();
    }

    private void showPolicyDialog() {
        final Dialog dialogPolicy = new Dialog(mContext, R.style.MyAlertDialogPrivacy);
        dialogPolicy.setContentView(R.layout.dialog_privacy_pliocy);
        dialogPolicy.setCancelable(false);
        dialogPolicy.show();

        TextView tvDecline = dialogPolicy.findViewById(R.id.tv_policy_decline);
        TextView tvAccept = dialogPolicy.findViewById(R.id.tv_give_per_dialog);
        TextView tvPermissionRequired = dialogPolicy.findViewById(R.id.tv_permission_required);

        tvPermissionRequired.setTypeface(typeface);

        tvDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });

        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPolicy.dismiss();
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("isPolicyAccepted", true);
                editor.apply();
                if (!hasPermissions(mContext, PERMISSIONS))
                    ActivityCompat.requestPermissions(mContext, PERMISSIONS, PERMISSION_ALL);
            }
        });
    }

    //Check available products and check whether already purchased or not...
    private void getInAppProducts() {
        skuList.add("removewatermark");
        skuList.add("removeads");
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
        try {
            Bundle skuDetails = mService.getSkuDetails(3,
                    getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == BILLING_RESPONSE_RESULT_OK) {
                ArrayList<String> responseList
                        = skuDetails.getStringArrayList("DETAILS_LIST");
                assert responseList != null;
                Log.e("InAppResponse>>>", Arrays.toString(responseList.toArray()));

                Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
                ArrayList<String> purchaseDataList =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                if (purchaseDataList != null) {
                    Log.e("PurchasedItems>>>", Arrays.toString(purchaseDataList.toArray()));
                    if (!Arrays.toString(purchaseDataList.toArray()).equalsIgnoreCase("[]")) {
                        for (String thisResponse : purchaseDataList) {
                            JSONObject object = new JSONObject(thisResponse);
                            String sku = object.getString("productId");

                            if (sku.equals("removeads")) {
                                editor = preferences.edit();
                                editor.putBoolean("Remove_Ads", true);
                                editor.apply();
                                Globals.flagRemoveAds = true;
                            } else if (sku.equals("removewatermark")) {
                                editor = preferences.edit();
                                editor.putBoolean("Remove_Water_Mark", true);
                                editor.apply();
                                Globals.flagRemoveWm = true;
                            }
                        }
                        if (!Globals.flagRemoveAds) {
                            MobileAds.initialize(mContext, getString(R.string.ad_app_id));
                            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
                            final FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
                            mFirebaseRemoteConfig.setConfigSettings(configSettings);
                            mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);
                            showAdsSettings();
                        }
                    } else {
                        Log.e("PurchasedItems>>>", "0");
                        if (!Globals.flagRemoveAds) {
                            MobileAds.initialize(mContext, getString(R.string.ad_app_id));
                            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
                            final FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
                            mFirebaseRemoteConfig.setConfigSettings(configSettings);
                            mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);
                            showAdsSettings();
                        }
                    }
                }
            }

        } catch (RemoteException | JSONException e) {
            e.printStackTrace();
            Crashlytics.log("InAppPurchase>>>" + e.getMessage());
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL) {
            getCategory();
            getQuotesList(strCatID, "", DEVICE_ID);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AD) {
            if (mInterstitialAd2 != null && mInterstitialAd2.isLoaded())
                mInterstitialAd2.show();
            else if (interstitialAd2 != null && interstitialAd2.isAdLoaded())
                interstitialAd2.show();
        }
    }

    //Check Multiple Permissions...
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    //API call region...
    private void rateMyApp() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.manchesterapps.picturequotesandstatus")));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mContext.getResources().getString(R.string.app_playstore_path))));
        }
    }

    //InterstitialAd settings...
    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
<<<<<<< HEAD
                    .addTestDevice("2845F988145081EEA41D805467142B95")
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                    .build();
            mBinding.appBarLayout.contentLayout.adView.loadAd(adRequest);
            mBinding.appBarLayout.contentLayout.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.appBarLayout.contentLayout.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.appBarLayout.contentLayout.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
<<<<<<< HEAD
                        .addTestDevice("2845F988145081EEA41D805467142B95")
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        .build();
                mBinding.appBarLayout.contentLayout.adView.loadAd(adRequest);
                mBinding.appBarLayout.contentLayout.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.appBarLayout.contentLayout.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.appBarLayout.contentLayout.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    public void requestNewInterstitial() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    String isShow = mFirebaseRemoteConfig.getString("status");
                    if (isShow.equals("show")) {
<<<<<<< HEAD
                        if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                        if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    }
                }
            });
            mInterstitialAd.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd = new com.facebook.ads.InterstitialAd(mContext,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd.loadAd();
                    interstitialAd.setAdListener(new AbstractAdListener() {
                        @Override
                        public void onAdLoaded(Ad ad) {
                            super.onAdLoaded(ad);
                            String isShow = mFirebaseRemoteConfig.getString("status");
                            if (isShow.equals("show")) {
<<<<<<< HEAD
                                if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                                if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                        Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                    if (interstitialAd.isAdLoaded())
                                        interstitialAd.show();

<<<<<<< HEAD
                                    AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                    QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        String isShow = mFirebaseRemoteConfig.getString("status");
                        if (isShow.equals("show")) {
<<<<<<< HEAD
                            if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                            if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                    Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                if (mInterstitialAd.isLoaded())
                                    mInterstitialAd.show();

<<<<<<< HEAD
                                AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                            }
                        }
                    }
                });
                mInterstitialAd.loadAd(adRequest2);
            }
        }
    }

    private void requestNewInterstitial2() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd2.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd2 = new com.facebook.ads.InterstitialAd(mContext,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd2.loadAd();
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                Log.e("AdsType>>>", "GoogleAds");

                mInterstitialAd2 = new InterstitialAd(this);
                mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd2.loadAd(adRequest2);
            }
        }
<<<<<<< HEAD
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void setLoading(boolean isShow) {
        if (isShow) {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoading.setVisibility(View.VISIBLE);
            else
                mBinding.appBarLayout.contentLayout.pbLoading.setVisibility(View.VISIBLE);
        } else {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoading.setVisibility(View.GONE);
            else
                mBinding.appBarLayout.contentLayout.pbLoading.setVisibility(View.GONE);
        }
    }

=======
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void setLoading(boolean isShow) {
        if (isShow) {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoading.setVisibility(View.VISIBLE);
            else
                mBinding.appBarLayout.contentLayout.pbLoading.setVisibility(View.VISIBLE);
        } else {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoading.setVisibility(View.GONE);
            else
                mBinding.appBarLayout.contentLayout.pbLoading.setVisibility(View.GONE);
        }
    }

>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    private void setLoadMore(boolean isShow) {
        if (isShow) {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoadMore.setVisibility(View.VISIBLE);
            else
                mBinding.appBarLayout.contentLayout.pbLoadMore.setVisibility(View.VISIBLE);
        } else {
            if (SDK > 19)
                mBinding.appBarLayout.contentLayout.cvLoadMore.setVisibility(View.GONE);
            else
                mBinding.appBarLayout.contentLayout.pbLoadMore.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    public void getQuotesByCategory(final String id, final String name) {
        mBinding.drawerLayout.closeDrawers();
        quotesListArrayList.clear();
        adapterQuotesList.notifyDataSetChanged();

        scrollListener.resetState();

        previousPosition = 0;

        strCatID = id;
        strLoadedID = "";

        setLoading(true);

        flagCategory = true;

        if (!name.equalsIgnoreCase("all"))
            mBinding.appBarLayout.tvAppTitle.setText(name);
        else
            mBinding.appBarLayout.tvAppTitle.setText("Picture Quotes & Creator");

        getQuotesList(strCatID, strLoadedID, DEVICE_ID);
    }

    public void getQuotesList(String catID, String loadedID, String deviceID) {
        if (InternetConnection.checkConnection(mContext)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getRetrofit().create(RetrofitInterfaces.class);

            Call<Model_Quotes_List> call = retrofitInterfaces.getQuotesList(catID, loadedID, deviceID);
            call.enqueue(new Callback<Model_Quotes_List>() {
                @Override
                public void onResponse(Call<Model_Quotes_List> call, Response<Model_Quotes_List> response) {
                    if (mBinding.appBarLayout.contentLayout.swipeQuotesList.isRefreshing())
                        mBinding.appBarLayout.contentLayout.swipeQuotesList.setRefreshing(false);

                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("success")) {
                        quotesListArrayList.addAll(response.body().getQoutesList());
                        adapterQuotesList.notifyItemRangeChanged(previousPosition, quotesListArrayList.size());

                        for (int i = 0; i < quotesListArrayList.size(); i++)
                            strLoadedID = strLoadedID + "," + quotesListArrayList.get(i).getId();

                        previousPosition = quotesListArrayList.size();

                        if (flagCategory) {
                            mBinding.appBarLayout.contentLayout.rvQuoteList.smoothScrollToPosition(0);
                            flagCategory = false;
                        }

                        if (flagLoadMore) {
                            flagLoadMore = false;
                            setLoadMore(false);
                        } else
                            setLoading(false);
                        flagSwipe = false;
                        mBinding.appBarLayout.contentLayout.rvQuoteList.addOnScrollListener(scrollListener);
                    } else {
                        if (!flagLoadMore)
                            if (!isShowDialog)
                                showPopup(true);
                        setLoading(false);
                    }
                }

                @Override
                public void onFailure(Call<Model_Quotes_List> call, Throwable t) {
                    if (mBinding.appBarLayout.contentLayout.swipeQuotesList.isRefreshing())
                        mBinding.appBarLayout.contentLayout.swipeQuotesList.setRefreshing(false);

                    flagSwipe = false;

                    if (flagLoadMore) {
                        flagLoadMore = false;
                        setLoadMore(false);
                    } else
                        setLoading(false);
                    if (!isShowDialog)
                        showPopup(true);
                }
            });
        } else {
            if (flagLoadMore) {
                flagLoadMore = false;
                setLoadMore(false);
            } else
                setLoading(false);
            if (!isShowDialog)
                showPopup(false);
            flagSwipe = false;
        }
    }

    public void getCategory() {
        if (InternetConnection.checkConnection(mContext)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getRetrofit().create(RetrofitInterfaces.class);

            Call<Model_Status_Category> call = retrofitInterfaces.getCategory();

            call.enqueue(new Callback<Model_Status_Category>() {
                @Override
                public void onResponse(Call<Model_Status_Category> call, Response<Model_Status_Category> response) {
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("success")) {
                        mBinding.rvCategory.setAdapter(new Adapter_Status_Category(response.body().getCategoryList(),
                                R.layout.layout_status_category,
                                mContext));

                        countFailed = 0;
                    }
                }

                @Override
                public void onFailure(Call<Model_Status_Category> call, Throwable t) {
                    countFailed++;
                    if (countFailed < 3)
                        getCategory();
                }
            });
        }
    }

    //Try Again Popup...
    @SuppressLint("SetTextI18n")
    public void showPopup(boolean failed) {
        isShowDialog = true;
        //Setting loading layouts...

        final DialogLayoutNoInternetBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_layout_no_internet,
                null,
                false);
        final Dialog dialog;
        dialog = new Dialog(mContext, R.style.MY_DIALOG_Feedback);
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
<<<<<<< HEAD
        if (!isFinishing())
            dialog.show();
=======
        dialog.show();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        binding.cvCreateQuotes.setAnimation(zoomin);
        binding.cvCreateQuotes.setAnimation(zoomout);

        binding.cvCreateQuotes.startAnimation(zoomin);

        zoomin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomout);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomin);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        if (failed)
<<<<<<< HEAD
            binding.tvNoInternetDialog.setText("Connection problem!");
=======
            binding.tvNoInternetDialog.setText("Server Connection problem!");
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        else
            binding.tvNoInternetDialog.setText("No internet connection!");

        binding.cvTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialog.dismiss();
                isShowDialog = false;
                if (!flagLoadMore) {
                    setLoading(true);
                    getCategory();
                    getQuotesList(strCatID, "", DEVICE_ID);
                } else {
                    setLoadMore(true);
                    getQuotesList(strCatID, strLoadedID, DEVICE_ID);
                }
            }
        });

        binding.cvCreateQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDialog = false;
                playSound(R.raw.button_tap);
                Intent intent = new Intent(mContext, CreateQuoteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        binding.cvQuitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDialog = false;
                playSound(R.raw.button_tap);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });
    }
}
