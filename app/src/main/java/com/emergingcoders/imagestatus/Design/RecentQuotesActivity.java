package com.emergingcoders.imagestatus.Design;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.emergingcoders.imagestatus.Adapter.Adapter_Recent_Quotes;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.Model_Recent_Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.DbHelper;
import com.emergingcoders.imagestatus.Utils.EndlessRecyclerViewScrollListener;
import com.emergingcoders.imagestatus.databinding.ActivityRecentQuotesBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RecentQuotesActivity extends AppCompatActivity {

    ActivityRecentQuotesBinding mBinding;
    Activity mContext;

    Adapter_Recent_Quotes adapterRecentQuotes;
    ArrayList<Model_Recent_Quotes> recentQuotesArrayList = new ArrayList<>();

    EndlessRecyclerViewScrollListener scrollListener;
    DbHelper dbHelper;
    int pageID = 1;

    com.facebook.ads.AdView adViewFB;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_recent_quotes);

        mContext = RecentQuotesActivity.this;

        mBinding.toolbarParentLayout.tvAppTitle.setText("Recent Quotes");
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Setup RecyclerView of recent quotes...
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mBinding.rvRecentQuotes.setLayoutManager(linearLayoutManager);

        dbHelper = new DbHelper(mContext);
        recentQuotesArrayList.addAll(dbHelper.getRecentQuotes(pageID));

        adapterRecentQuotes = new Adapter_Recent_Quotes(recentQuotesArrayList,
                R.layout.layout_recent_quotes,
                mContext);
        mBinding.rvRecentQuotes.setAdapter(adapterRecentQuotes);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (recentQuotesArrayList.size() > 29)
                    getMoreQuotes();
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);

                int firstPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (firstPosition > 10)
                    mBinding.fab.setVisibility(View.VISIBLE);
                else
                    mBinding.fab.setVisibility(View.GONE);
            }
        };
        mBinding.rvRecentQuotes.addOnScrollListener(scrollListener);

        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.rvRecentQuotes.smoothScrollToPosition(0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding.adView != null)
            mBinding.adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.adView != null)
            mBinding.adView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBinding.adView != null)
            mBinding.adView.destroy();
        if (adViewFB != null)
            adViewFB.destroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void getMoreQuotes() {
        recentQuotesArrayList.addAll(dbHelper.getRecentQuotes(pageID));
        adapterRecentQuotes.notifyDataSetChanged();
    }

    public void refreshRecentQuotes(int position) {
        recentQuotesArrayList.remove(position);
        adapterRecentQuotes.notifyDataSetChanged();
    }

    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mBinding.adView.loadAd(adRequest);
            mBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                mBinding.adView.loadAd(adRequest);
                mBinding.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
