package com.emergingcoders.imagestatus.Design;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Adapter.Adapter_Related_Quotes;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.ModelQuotesList;
import com.emergingcoders.imagestatus.Model.Model_Quotes_List;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Retrofit.RetrofitClient;
import com.emergingcoders.imagestatus.Retrofit.RetrofitInterfaces;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.EndlessRecyclerViewScrollListener;
import com.emergingcoders.imagestatus.Utils.InternetConnection;
<<<<<<< HEAD
=======
import com.emergingcoders.imagestatus.Utils.QuotesUtils;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.databinding.ActivityImageShowBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutNoInternetBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ImageShowActivity extends AppCompatActivity {

    ActivityImageShowBinding mBinding;

    Activity mContext;
    private Adapter_Related_Quotes adapterRelatedQuotes;
    private ArrayList<ModelQuotesList> relatedQuotesArrayList = new ArrayList<>();
    private ModelQuotesList modelQuotesList;

    private String strLoadedID = "";
    int strCatID = 0;
    EndlessRecyclerViewScrollListener scrollListener;
    boolean flagLoadMore = false, isShowDialog = false;
    private int previousPosition = 0;
    private int SDK;
    String DEVICE_ID;

    InterstitialAd mInterstitialAd, mInterstitialAd2;
    private com.facebook.ads.InterstitialAd interstitialAd, interstitialAd2;
    AdView adViewFB;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    boolean isAdInitialized = false;

    int PERMISSION_ALL = 1;
    final private String BROADCAST_LIKE_QUOTE = "BROADCAST_LIKE_QUOTE";
    final static int REQUEST_AD = 100;

    @SuppressLint({"SetTextI18n", "HardwareIds"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_image_show);

        SDK = Build.VERSION.SDK_INT;
        mContext = ImageShowActivity.this;

        //Set Device id...
        DEVICE_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //Set screen title...
        modelQuotesList = new Gson().fromJson(getIntent().getStringExtra("QuotesList"), ModelQuotesList.class);
        mBinding.toolbarParentLayout.tvAppTitle.setText(modelQuotesList.getCatName());

        //Loading Layout for Related Quotes...
        mBinding.pbLoading.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        //Loading for Endless Recycler...
        mBinding.pbLoadMore.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable
                .Builder(mContext)
                .colors(getResources().getIntArray(R.array.pb_loading))
                .build());

        //Setting related quotes recyclerview...
        final StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        mBinding.rvRelatedQuotes.setLayoutManager(staggeredGridLayoutManager);
        mBinding.rvRelatedQuotes.setNestedScrollingEnabled(false);
        mBinding.rvRelatedQuotes.setHasFixedSize(false);

        adapterRelatedQuotes = new Adapter_Related_Quotes(relatedQuotesArrayList,
                R.layout.row_layout_related_quotes,
                mContext,
                modelQuotesList);
        mBinding.rvRelatedQuotes.setAdapter(adapterRelatedQuotes);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Setting Endless Recycleriew...
        scrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                setLoadMore(true);

                flagLoadMore = true;
                getQuotesList(strCatID, strLoadedID, DEVICE_ID);
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);

                int[] firstVisiblePosition = staggeredGridLayoutManager.findFirstVisibleItemPositions(null);

                if (firstVisiblePosition[0] > 5)
                    mBinding.fab.show();
                else
                    mBinding.fab.hide();
            }
        };

        //Floating button for go to top of the list...
        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.rvRelatedQuotes.smoothScrollToPosition(0);
            }
        });

        //Back button top...
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        //Call API first time...
        strCatID = modelQuotesList.getCatID();
        getQuotesList(strCatID, strLoadedID, DEVICE_ID);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        flagLoadMore = false;
        if (mBinding.adView != null)
            mBinding.adView.destroy();
        if (adViewFB != null)
            adViewFB.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.adView != null)
            mBinding.adView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding.adView != null)
            mBinding.adView.resume();
    }

    @Override
    public void onBackPressed() {
        if (adapterRelatedQuotes.flagUnLike) {
            Intent intent = new Intent(BROADCAST_LIKE_QUOTE);
            intent.putExtra("isUnLiked", true);
            sendBroadcast(intent);
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AD) {
            if (mInterstitialAd2 != null && mInterstitialAd2.isLoaded())
                mInterstitialAd2.show();
            else if (interstitialAd2 != null && interstitialAd2.isAdLoaded())
                interstitialAd2.show();
<<<<<<< HEAD
            adapterRelatedQuotes.btnClick = "";
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL)
            if (grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED)
                adapterRelatedQuotes.saveImageAfterPermission();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setCVLoading(boolean isShow) {
        if (isShow) {
            if (SDK > 19)
                mBinding.cvLoading.setVisibility(View.VISIBLE);
            else
                mBinding.pbLoading.setVisibility(View.VISIBLE);
        } else {
            if (SDK > 19)
                mBinding.cvLoading.setVisibility(View.GONE);
            else
                mBinding.pbLoading.setVisibility(View.GONE);
        }
    }

    private void setLoadMore(boolean isShow) {
        if (isShow) {
            if (SDK > 19)
                mBinding.cvLoadMore.setVisibility(View.VISIBLE);
            else
                mBinding.pbLoadMore.setVisibility(View.VISIBLE);
        } else {
            if (SDK > 19)
                mBinding.cvLoadMore.setVisibility(View.GONE);
            else
                mBinding.pbLoadMore.setVisibility(View.GONE);
        }
    }

    //InterstitialAd settings...
    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                    requestNewInterstitial();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mBinding.adView.loadAd(adRequest);
            mBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                mBinding.adView.loadAd(adRequest);
                mBinding.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private void requestNewInterstitial() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    String isShow = mFirebaseRemoteConfig.getString("status");
                    if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                        if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                        if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    }
                }
            });
            mInterstitialAd.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd = new com.facebook.ads.InterstitialAd(mContext,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd.loadAd();
                    interstitialAd.setAdListener(new AbstractAdListener() {
                        @Override
                        public void onAdLoaded(Ad ad) {
                            super.onAdLoaded(ad);
                            String isShow = mFirebaseRemoteConfig.getString("status");
                            if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                                if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                                if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                        Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                    if (interstitialAd.isAdLoaded())
                                        interstitialAd.show();

<<<<<<< HEAD
                                    AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                    QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        String isShow = mFirebaseRemoteConfig.getString("status");
                        if (isShow.equalsIgnoreCase("show")) {
<<<<<<< HEAD
                            if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(mContext) >
=======
                            if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(mContext) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                    Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                if (mInterstitialAd.isLoaded())
                                    mInterstitialAd.show();

<<<<<<< HEAD
                                AppPreferences.setLastSavedDateForAds(mContext, System.currentTimeMillis());
=======
                                QuotesUtils.setLastSavedDateForAds(mContext, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                            }
                        }
                    }
                });
                mInterstitialAd.loadAd(adRequest2);
            }
        }
    }

    public void requestNewInterstitial2() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd2.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                Log.e("AdsType>>>", "FBAds");
                try {
                    interstitialAd2 = new com.facebook.ads.InterstitialAd(mContext,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd2.loadAd();
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                Log.e("AdsType>>>", "GoogleAds");

                mInterstitialAd2 = new InterstitialAd(this);
                mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd2.loadAd(adRequest2);
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    //API call region...
    public void getQuotesList(int catID, String loadedID, String deviceID) {
        if (InternetConnection.checkConnection(mContext)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getRetrofit().create(RetrofitInterfaces.class);

            Call<Model_Quotes_List> call = retrofitInterfaces.getRelatedQuotesList(catID, loadedID, deviceID);

            call.enqueue(new Callback<Model_Quotes_List>() {
                @Override
                public void onResponse(Call<Model_Quotes_List> call, Response<Model_Quotes_List> response) {
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("success")) {
                        relatedQuotesArrayList.addAll(response.body().getQoutesList());
                        if (flagLoadMore)
                            adapterRelatedQuotes.notifyItemRangeChanged(previousPosition, relatedQuotesArrayList.size());
                        else
                            adapterRelatedQuotes.notifyItemRangeChanged(previousPosition, relatedQuotesArrayList.size() + 1);

                        for (int i = 0; i < relatedQuotesArrayList.size(); i++)
                            strLoadedID = strLoadedID + "," + relatedQuotesArrayList.get(i).getId();
                        previousPosition = relatedQuotesArrayList.size();

                        mBinding.rvRelatedQuotes.addOnScrollListener(scrollListener);

                        if (flagLoadMore) {
                            flagLoadMore = false;
                            setLoadMore(false);
                        } else {
                            setCVLoading(false);
<<<<<<< HEAD
                        }
                    } else {
                        if (flagLoadMore) {
                            flagLoadMore = false;
                            setLoadMore(false);
                        } else {
                            setCVLoading(false);
                            if (!isShowDialog)
                                showPopup(true);
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    }
                }

                @Override
                public void onFailure(Call<Model_Quotes_List> call, Throwable t) {
                    if (flagLoadMore) {
                        flagLoadMore = false;
                        setLoadMore(false);
                    } else
                        setCVLoading(false);

                    if (!isShowDialog)
                        showPopup(true);
                    Crashlytics.log("ShowQuotes>>>" + t.getMessage());
                }
            });
        } else {
            if (flagLoadMore) {
                flagLoadMore = false;
                setLoadMore(false);
            } else
                setCVLoading(false);

            if (!isShowDialog)
                showPopup(false);
        }
    }

    //Try Again Popup...
    @SuppressLint("SetTextI18n")
    public void showPopup(boolean failed) {
        isShowDialog = true;
        //Setting loading layouts...

        final DialogLayoutNoInternetBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_layout_no_internet,
                null,
                false);
        final Dialog pwTryAgain;
        pwTryAgain = new Dialog(mContext, R.style.MY_DIALOG_Feedback);
        pwTryAgain.setContentView(binding.getRoot());
        pwTryAgain.setCancelable(false);
<<<<<<< HEAD
        if (!isFinishing())
            pwTryAgain.show();
=======
        pwTryAgain.show();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        binding.cvCreateQuotes.setAnimation(zoomin);
        binding.cvCreateQuotes.setAnimation(zoomout);

        binding.cvCreateQuotes.startAnimation(zoomin);

        zoomin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomout);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.cvCreateQuotes.startAnimation(zoomin);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        if (failed)
<<<<<<< HEAD
            binding.tvNoInternetDialog.setText("Connection problem");
=======
            binding.tvNoInternetDialog.setText("Server Connection problem");
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        else
            binding.tvNoInternetDialog.setText("No internet connection!");

        binding.cvTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isShowDialog = false;
                if (!flagLoadMore) {
                    setCVLoading(true);

                    pwTryAgain.dismiss();
                    getQuotesList(strCatID, "", DEVICE_ID);
                } else {
                    setLoadMore(true);

                    pwTryAgain.dismiss();
                    getQuotesList(strCatID, strLoadedID, DEVICE_ID);
                }
            }
        });

        binding.cvCreateQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDialog = false;
                playSound(R.raw.button_tap);
                Intent intent = new Intent(mContext, CreateQuoteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        binding.cvQuitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDialog = false;
                playSound(R.raw.button_tap);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });
    }
}
