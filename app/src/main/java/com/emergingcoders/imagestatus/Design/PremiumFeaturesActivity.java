package com.emergingcoders.imagestatus.Design;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PremiumFeaturesActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView ibBack, ivRemoveWM, ivRemoveAds;
    TextView tvTitle;
    CardView cvRemoveAd, cvRemoveWM;

    Dialog dialogRemoveAds, dialogRemoveWM, dialogSuccess;

    Activity context;

    IInAppBillingService mService;
    ArrayList<String> skuList = new ArrayList<>();

    static final int REQUEST_EMOVE_ADS = 1001,
            BILLING_RESPONSE_RESULT_OK = 0,
            REQUEST_REMOVE_WM = 111;
    String priceRemoveAds = "",
            priceRemoveWM = "";
    String BROADCAST_FILTER_IN_APP = "com.emergingcoders.imagestatus.Design.PremiumFeaturesActivity";
    String BROADCAST_REMOVE_ADS = "com.emergingcoders.imagestatus.Design.PremiumFeaturesActivity2";
    SharedPreferences preferences;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            /*try {
                mService.consumePurchase(3, getPackageName(), tokenRWM);
            } catch (RemoteException e) {
                e.printStackTrace();
            }*/
            getInAppProducts();
        }
    };

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = PremiumFeaturesActivity.this;
        preferences = getSharedPreferences("MP", MODE_PRIVATE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_features);

        toolbar = findViewById(R.id.toolbar_main);
        ibBack = findViewById(R.id.ib_back);
        tvTitle = findViewById(R.id.tv_app_title);
        cvRemoveAd = findViewById(R.id.cv_remove_ads);
        cvRemoveWM = findViewById(R.id.cv_remove_wm);
        ivRemoveWM = findViewById(R.id.iv_in_app_remove_wm);
        ivRemoveAds = findViewById(R.id.iv_in_app_remove_ads);

        tvTitle.setText("Premium Features");

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        cvRemoveAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                if (!Globals.flagRemoveAds)
                    openRemoveAdsDialog();
                else
                    Toast.makeText(context, getResources().getString(R.string.in_app_already_purchased), Toast.LENGTH_SHORT).show();
            }
        });

        cvRemoveWM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                if (!Globals.flagRemoveWm)
                    openRemoveWMDialog();
                else
                    Toast.makeText(context, getResources().getString(R.string.in_app_already_purchased), Toast.LENGTH_SHORT).show();
            }
        });

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        if (mService != null) {
            unbindService(mServiceConn);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (dialogSuccess != null && dialogSuccess.isShowing())
            dialogSuccess.dismiss();
        else if (dialogRemoveAds != null && dialogRemoveAds.isShowing())
            dialogRemoveAds.dismiss();
        else if (dialogRemoveWM != null && dialogRemoveWM.isShowing())
            dialogRemoveWM.dismiss();
        else
            super.onBackPressed();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //Check available products and check whether already purchased or not...
    private void getInAppProducts() {
        skuList.add("removewatermark");
        skuList.add("removeads");
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
        try {
            Bundle skuDetails = mService.getSkuDetails(3,
                    getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == BILLING_RESPONSE_RESULT_OK) {
                ArrayList<String> responseList
                        = skuDetails.getStringArrayList("DETAILS_LIST");
                assert responseList != null;
                Log.e("InAppResponse>>>", Arrays.toString(responseList.toArray()));
                for (String priceList : responseList) {
                    JSONObject object = new JSONObject(priceList);
                    String pID = object.getString("productId");

                    if (pID.equalsIgnoreCase("removeads"))
                        priceRemoveAds = object.getString("price");
                    else if (pID.equalsIgnoreCase("removewatermark"))
                        priceRemoveWM = object.getString("price");
                }

                Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
                ArrayList<String> purchaseDataList =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                if (purchaseDataList != null) {
                    Log.e("PurchasedItems>>>", Arrays.toString(purchaseDataList.toArray()));
                    if (!Arrays.toString(purchaseDataList.toArray()).equalsIgnoreCase("[]")) {
                        for (String thisResponse : purchaseDataList) {
                            JSONObject object = new JSONObject(thisResponse);
                            String sku = object.getString("productId");

                            if (sku.equals("removeads")) {
                                cvRemoveAd.setCardBackgroundColor(Color.parseColor("#47C9A2"));
                                ivRemoveAds.setImageResource(R.drawable.ic_in_app_done);
                            } else if (sku.equals("removewatermark")) {
                                cvRemoveWM.setCardBackgroundColor(Color.parseColor("#47C9A2"));
                                ivRemoveWM.setImageResource(R.drawable.ic_in_app_done);
                            }
                        }
                    } else
                        Log.e("PurchasedItems>>>", "0");
                }
            }

        } catch (RemoteException | JSONException e) {
            e.printStackTrace();
            Crashlytics.log("InAppPurchases>>>" + e.getMessage());
        }
    }

    //Open Remove Watermark dialog...
    @SuppressLint("SetTextI18n")
    private void openRemoveWMDialog() {
        dialogRemoveWM = new Dialog(context, R.style.MY_DIALOG_Feedback);
        dialogRemoveWM.setContentView(R.layout.dialog_in_app_remove_watermark);
        dialogRemoveWM.setCancelable(true);
        dialogRemoveWM.show();

        Button btnPremiumUpgrade = (Button) dialogRemoveWM.findViewById(R.id.btn_in_app_premium_upgrade);
        Button btnSkip = (Button) dialogRemoveWM.findViewById(R.id.btn_skip_in_app);
        ImageView ivClose = (ImageView) dialogRemoveWM.findViewById(R.id.iv_close_dialog_in_app);

        btnPremiumUpgrade.setText("Premium Upgrade " + priceRemoveWM);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveWM.dismiss();
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveWM.dismiss();
            }
        });

        btnPremiumUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveWM.dismiss();
                purchaseRemoveWatermark();
            }
        });
    }

    //Open Remove Ads dialog...
    @SuppressLint("SetTextI18n")
    private void openRemoveAdsDialog() {
        dialogRemoveAds = new Dialog(context, R.style.MY_DIALOG_Feedback);
        dialogRemoveAds.setContentView(R.layout.dialog_in_app_remove_ads);
        dialogRemoveAds.setCancelable(true);
        dialogRemoveAds.show();

        Button btnPremiumUpgrade = (Button) dialogRemoveAds.findViewById(R.id.btn_in_app_premium_upgrade);
        Button btnSkip = (Button) dialogRemoveAds.findViewById(R.id.btn_skip_in_app);
        ImageView ivClose = (ImageView) dialogRemoveAds.findViewById(R.id.iv_close_dialog_in_app);

        btnPremiumUpgrade.setText("Premium Upgrade " + priceRemoveAds);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveAds.dismiss();
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveAds.dismiss();
            }
        });

        btnPremiumUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogRemoveAds.dismiss();
                purchaseRemoveAds();
            }
        });
    }

    //Purchase the add watermark functionality...
    private void purchaseRemoveAds() {
        try {
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
                    "removeads", "inapp", getResources().getString(R.string.in_app_key));
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            if (pendingIntent != null) {
                startIntentSenderForResult(pendingIntent.getIntentSender(),
                        REQUEST_EMOVE_ADS, new Intent(), 0, 0,
                        0);
            }
        } catch (RemoteException | IntentSender.SendIntentException e) {
            e.printStackTrace();
            Crashlytics.log("PurchaseInApp>>>" + e.getMessage());
        }
    }

    //Purchase the remove watermark functionality...
    private void purchaseRemoveWatermark() {
        Bundle ownedItems;
        try {
            /*ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            ArrayList<String> purchaseDataList =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");*/
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
                    "removewatermark", "inapp", getResources().getString(R.string.in_app_key));
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            if (pendingIntent != null) {
                startIntentSenderForResult(pendingIntent.getIntentSender(),
                        REQUEST_REMOVE_WM, new Intent(), 0, 0,
                        0);
            }

        } catch (RemoteException | IntentSender.SendIntentException e) {
            e.printStackTrace();
            Crashlytics.log("PurchaseInApp>>>" + e.getMessage());
        }
    }

    //Show success dialog on purchase complete...
    private void showSuccessDialog(String message) {
        dialogSuccess = new Dialog(context, R.style.MY_DIALOG_Feedback);
        dialogSuccess.setContentView(R.layout.dialog_success_in_app);
        dialogSuccess.setCancelable(true);
        dialogSuccess.show();

        TextView tvSuccessMessage = dialogSuccess.findViewById(R.id.tv_dialog_success_in_app);
        ImageView ivCancel = dialogSuccess.findViewById(R.id.iv_close_success_dialog);

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSuccess.dismiss();
            }
        });

        tvSuccessMessage.setText(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EMOVE_ADS) {
            /*int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
            String dataToken = data.getStringExtra("INAPP_CONTINUATION_TOKEN ");*/

            if (resultCode == RESULT_OK) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("Remove_Ads", true);
                editor.apply();
                Globals.flagRemoveAds = true;
                cvRemoveAd.setCardBackgroundColor(getResources().getColor(R.color.suceess_dialog));
                ivRemoveAds.setImageResource(R.drawable.ic_in_app_done);
                showSuccessDialog(getResources().getString(R.string.dialog_no_ads));
                Intent intent = new Intent(BROADCAST_REMOVE_ADS);
                sendBroadcast(intent);
            }
        } else if (requestCode == REQUEST_REMOVE_WM) {
            /*int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
            String dataToken = data.getStringExtra("INAPP_CONTINUATION_TOKEN ");*/

            if (resultCode == RESULT_OK) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("Remove_Water_Mark", true);
                editor.apply();
                Globals.flagRemoveWm = true;
                cvRemoveWM.setCardBackgroundColor(getResources().getColor(R.color.suceess_dialog));
                ivRemoveWM.setImageResource(R.drawable.ic_in_app_done);
                showSuccessDialog(getResources().getString(R.string.dialog_no_wm));
                Intent intent = new Intent(BROADCAST_FILTER_IN_APP);
                sendBroadcast(intent);
            }
        }
    }
}
