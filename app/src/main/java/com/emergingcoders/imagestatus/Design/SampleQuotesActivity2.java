package com.emergingcoders.imagestatus.Design;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emergingcoders.imagestatus.Adapter.Adapter_Sample_Quotes;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.Categories;
import com.emergingcoders.imagestatus.Model.Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.DbHelper;
import com.emergingcoders.imagestatus.Utils.EndlessRecyclerViewScrollListener;
import com.emergingcoders.imagestatus.databinding.ActivitySampleQuotes2Binding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SampleQuotesActivity2 extends AppCompatActivity {

    ActivitySampleQuotes2Binding mBinding;
    Activity mContext;

    Adapter_Sample_Quotes adapterSampleQuotes;
    Adapter_Sample_Quotes_Category adapterSampleQuotesCategory;

    ArrayList<Categories> categoriesArrayList = new ArrayList<>();
    ArrayList<Quotes> quotesArrayList = new ArrayList<>();

    EndlessRecyclerViewScrollListener scrollListener;
    public String loadedIds = "";
    DbHelper dbHelper;
    int lastPostition = 0;

    AdView adViewFB;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sample_quotes2);

        mContext = this;

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Toolbar back press button...
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        //Setup RecyclerView...
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mBinding.rvSampleQuotes.setLayoutManager(linearLayoutManager);

        dbHelper = new DbHelper(mContext);

        quotesArrayList.addAll(dbHelper.getQuotes());

        adapterSampleQuotes = new Adapter_Sample_Quotes(quotesArrayList,
                R.layout.layout_sample_quotes,
                mContext);
        mBinding.rvSampleQuotes.setAdapter(adapterSampleQuotes);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (mBinding.toolbarParentLayout.spSampleQuotesCategory.getSelectedItemPosition() == 0) {
                    quotesArrayList.addAll(dbHelper.getQuotes(loadedIds));
                    Handler handler = new Handler();
                    final Runnable r = new Runnable() {
                        public void run() {
                            adapterSampleQuotes.notifyItemInserted(quotesArrayList.size());
                        }
                    };
                    handler.post(r);
                } else {
                    quotesArrayList.addAll(dbHelper.getMoreQuotesByCategory(loadedIds,
                            categoriesArrayList.get(mBinding.toolbarParentLayout.spSampleQuotesCategory.getSelectedItemPosition()).getId()));
                    Handler handler = new Handler();
                    final Runnable r = new Runnable() {
                        public void run() {
                            adapterSampleQuotes.notifyItemInserted(quotesArrayList.size());
                        }
                    };
                    handler.post(r);
                }
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);

                int firstPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (firstPosition > 10)
                    mBinding.fab.setVisibility(View.VISIBLE);
                else
                    mBinding.fab.setVisibility(View.GONE);
            }
        };
        mBinding.rvSampleQuotes.addOnScrollListener(scrollListener);

        //Set Category Spinner...
        Categories categories = new Categories();
        categories.setCategoryName("All");
        categories.setId("0");
        categoriesArrayList.add(categories);
        categoriesArrayList.addAll(dbHelper.getCategories());

        adapterSampleQuotesCategory = new Adapter_Sample_Quotes_Category(categoriesArrayList,
                R.layout.layout_sample_quotes_category,
                mContext);

        mBinding.toolbarParentLayout.spSampleQuotesCategory.setAdapter(adapterSampleQuotesCategory);
        mBinding.toolbarParentLayout.spSampleQuotesCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lastPostition = 0;
                loadedIds = "";
                quotesArrayList.clear();
                adapterSampleQuotes.notifyDataSetChanged();
                scrollListener.resetState();
                if (position > 0) {
                    String catID = adapterSampleQuotesCategory.getItem(position).toString();

                    quotesArrayList.addAll(dbHelper.getQuotesByCategory(catID));
                    adapterSampleQuotes.notifyDataSetChanged();
                } else {
                    quotesArrayList.addAll(dbHelper.getQuotes(loadedIds));
                    adapterSampleQuotes.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.rvSampleQuotes.smoothScrollToPosition(0);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBinding.adView != null)
            mBinding.adView.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding.adView != null)
            mBinding.adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.adView != null)
            mBinding.adView.pause();
    }

    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mBinding.adView.loadAd(adRequest);
            mBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                mBinding.adView.loadAd(adRequest);
                mBinding.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class Adapter_Sample_Quotes_Category extends BaseAdapter {
        ArrayList<Categories> arrayList;
        int rowLayout;
        Activity context;

        Adapter_Sample_Quotes_Category(ArrayList<Categories> arrayList, int rowLayout, Activity context) {
            this.arrayList = arrayList;
            this.rowLayout = rowLayout;
            this.context = context;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayList.get(position).getId();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CategoryViewHolder holder;
            if (convertView == null) {
                holder = new CategoryViewHolder();
                convertView = LayoutInflater.from(context).inflate(rowLayout, parent, false);

                holder.tvCategory = convertView.findViewById(R.id.tv_sample_quotes_category);
                convertView.setTag(holder);
            } else holder = (CategoryViewHolder) convertView.getTag();

            holder.tvCategory.setText(arrayList.get(position).getCategoryName());

            return convertView;
        }

        class CategoryViewHolder {
            TextView tvCategory;
        }
    }
}
