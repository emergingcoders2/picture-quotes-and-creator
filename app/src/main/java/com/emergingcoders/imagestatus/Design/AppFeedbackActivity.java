package com.emergingcoders.imagestatus.Design;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.Model_App_Feedback;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Retrofit.RetrofitClient;
import com.emergingcoders.imagestatus.Retrofit.RetrofitInterfaces;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.InternetConnection;
import com.emergingcoders.imagestatus.databinding.ActivityAppFeedbackBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AppFeedbackActivity extends AppCompatActivity {

    ActivityAppFeedbackBinding mBinding;

    Activity mContext;
    SharedPreferences preferences;

    com.facebook.ads.AdView adViewFB;
    FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_app_feedback);

        mContext = AppFeedbackActivity.this;
        preferences = getSharedPreferences("MP", MODE_PRIVATE);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Back button top...
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        //Set Rate=ture so it will never show again rate dialog!
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Rate", "true");
        editor.apply();

        mBinding.btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSound(R.raw.button_tap);
                if (mBinding.etFeedbackName.getText().toString().equalsIgnoreCase(""))
                    Toast.makeText(mContext, "Please enter your name!", Toast.LENGTH_SHORT).show();
                else if (mBinding.etFeedbackEmail.getText().toString().equalsIgnoreCase(""))
                    Toast.makeText(mContext, "Please enter your email!", Toast.LENGTH_SHORT).show();
                else if (mBinding.etFeedbackMessage.getText().toString().equalsIgnoreCase(""))
                    Toast.makeText(mContext, "Please type some feedback message!", Toast.LENGTH_LONG).show();
                else if (!isValidEmail(mBinding.etFeedbackEmail.getText().toString()))
                    Toast.makeText(mContext, "Please enter valid email!", Toast.LENGTH_SHORT).show();
                else {
                    mBinding.btnSubmitFeedback.setEnabled(false);
                    sendFeedback(mBinding.etFeedbackEmail.getText().toString(),
                            mBinding.etFeedbackEmail.getText().toString(),
                            mBinding.etFeedbackMessage.getText().toString());
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBinding.adView != null)
            mBinding.adView.destroy();
        if (adViewFB != null)
            adViewFB.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding.adView != null)
            mBinding.adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.adView != null)
            mBinding.adView.pause();
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(mContext)) {
            MediaPlayer mp = MediaPlayer.create(mContext, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(mContext, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //Ad settings...
    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                    requestNewBannerAd();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewBannerAd() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mBinding.adView.loadAd(adRequest);
            mBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mBinding.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mBinding.adView.setVisibility(View.GONE);
                }
            });
        } else {
            if (getInstalled("com.facebook.katana")) {
                adViewFB = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner_id), AdSize.BANNER_HEIGHT_50);
                final LinearLayout adContainer = findViewById(R.id.banner_container);
                adContainer.addView(adViewFB);
                adViewFB.loadAd();
                adViewFB.setAdListener(new AbstractAdListener() {
                    @Override
                    public void onAdLoaded(Ad ad) {
                        super.onAdLoaded(ad);
                        adContainer.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                mBinding.adView.loadAd(adRequest);
                mBinding.adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mBinding.adView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        mBinding.adView.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void sendFeedback(String name, String email, String message) {
        if (InternetConnection.checkConnection(mContext)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getClient().create(RetrofitInterfaces.class);

            Call<Model_App_Feedback> call = retrofitInterfaces.sendFeedback(name, email, message);
            call.enqueue(new Callback<Model_App_Feedback>() {
                @Override
                public void onResponse(Call<Model_App_Feedback> call, Response<Model_App_Feedback> response) {
                    if (response.body().getResult().equalsIgnoreCase("success")) {
                        Toast.makeText(mContext, "Your feedback sent to our team", Toast.LENGTH_LONG).show();

                        mBinding.btnSubmitFeedback.setEnabled(true);
                        mBinding.etFeedbackEmail.setText("");
                        mBinding.etFeedbackName.setText("");
                        mBinding.etFeedbackMessage.setText("");
                        Intent intent = new Intent(mContext, DashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<Model_App_Feedback> call, Throwable t) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_LONG).show();
                    mBinding.btnSubmitFeedback.setEnabled(true);
                    mBinding.etFeedbackEmail.setText("");
                    mBinding.etFeedbackName.setText("");
                    mBinding.etFeedbackMessage.setText("");
                }
            });
        } else {
            Toast.makeText(mContext, "Please check your internet!", Toast.LENGTH_LONG).show();
            mBinding.btnSubmitFeedback.setEnabled(true);
            mBinding.etFeedbackEmail.setText("");
            mBinding.etFeedbackName.setText("");
            mBinding.etFeedbackMessage.setText("");
        }
    }
}
