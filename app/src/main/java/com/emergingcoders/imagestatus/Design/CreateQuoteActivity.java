package com.emergingcoders.imagestatus.Design;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
<<<<<<< HEAD
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
=======
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Adapter.Adapter_Font_Family;
import com.emergingcoders.imagestatus.Adapter.Adapter_WM_Font_Family;
<<<<<<< HEAD
import com.emergingcoders.imagestatus.Fragment.DialogFragmentWMFonts;
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.Fragment.DialogFragmentWindow;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.Model_Font_Style;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.DbHelper;
<<<<<<< HEAD
import com.emergingcoders.imagestatus.databinding.ActivityCreateQuoteBinding;
import com.emergingcoders.imagestatus.databinding.DialogEditWatermarkBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutBackWatermarkBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutQuoteTintBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutTextColorPickerBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutWmColorBinding;
=======
import com.emergingcoders.imagestatus.Utils.QuotesUtils;
import com.emergingcoders.imagestatus.Utils.RecyclerItemClickListener;
import com.emergingcoders.imagestatus.Utils.RepeatListener;
import com.emergingcoders.imagestatus.databinding.ActivityCreateQuoteBinding;
import com.emergingcoders.imagestatus.databinding.DialogEditWatermarkBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutWmColorBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutWmFontFamilyBinding;
import com.emergingcoders.imagestatus.databinding.DialogLayoutWmFontSizeBinding;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.databinding.DialogLayoutWmOpacityBinding;
import com.emergingcoders.imagestatus.databinding.DialogSaveQuoteBinding;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.emergingcoders.imagestatus.Globals.Globals.lastProgressRadius;
import static com.emergingcoders.imagestatus.Globals.Globals.lastX;
import static com.emergingcoders.imagestatus.Globals.Globals.lastY;
import static com.emergingcoders.imagestatus.Globals.Globals.rotateMultiplier;

public class CreateQuoteActivity extends AppCompatActivity implements View.OnTouchListener, ColorPickerDialogListener {

    /*
    * Please pay attention all public objects are declared because
    * it is also used by some other fragments or classes!
    * Normal objects are just for local and none sharable data.
    */

    ActivityCreateQuoteBinding mBinding;
    private ImageView ivSave;
<<<<<<< HEAD
    public TextView tvQuote, tvQuoteBy, tvAddWM;
    public RelativeLayout framePicture, frameWM;
    public LinearLayout frameQuote;
    SeekBar sbTint;
    Button btnTextColor, btnWMTextColor, btnTextShadowColor, btnWMTextShadowColor;
=======
    public TextView tvQuote, tvQuoteBy;
    public RelativeLayout framePicture;
    public LinearLayout frameQuote;
    SeekBar sbTint;
    Button btnTextColor, btnTextShadowColor;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

    Activity context;

    Adapter_Background_Quotes adapterBackgroundQuotes;
    public Adapter_Font_Family adapterFontType2;
<<<<<<< HEAD
    public Adapter_WM_Font_Family adapterWmFontFamily;
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

    ArrayList<Integer> sampleImagesList = new ArrayList<>();
    public ArrayList<Model_Font_Style> fontStyleArrayList = new ArrayList<>();

<<<<<<< HEAD
    private Dialog dialogChangeQuote,
            dialogTextColors,
            dialogQuoteTint,
            dialogWatermark,
            dialogWMColor,
            dialogWMOpacity;
    private DialogFragmentWindow fragmentWindow;
    private DialogFragmentWMFonts dialogFragmentWMFonts;
=======
    private Dialog dialogChangeQuote, dialogTextColors, dialogQuoteTint, dialogInApp, dialogWatermark, dialogWMOpacity;
    private DialogFragmentWindow fragmentWindow;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    int selectedSampleImage = R.drawable.abc1;
    String strQuoteText = "", strQuoteBy = "", lastSavedImage = "";
    Typeface typeface2, typeface3, typeface4, typeface5;
    boolean flagNoText = true,
            flagNoWMText = true,
            doubleTap = false,
            flagBroadcast = false,
            flagBRInApp = false,
            isFontColor = false,
<<<<<<< HEAD
            isFontShadowColor = false,
            isWMColor = false,
            isWMSahdowColor = false,
=======
            isWMColor = false,
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
            isBackColor = false;
    Handler handler;
    float dX, dY, dX1, dY1;
    int lastWMAction;
    int SDK, lastColor = Color.WHITE,
            lastShadowColor = Color.BLACK,
            lastWMColor = Color.WHITE,
<<<<<<< HEAD
            lastWMSahdowColor = Color.BLACK,
            wmOpacity = 0,
            watermarkSize = 32,
            tintLast = 0;
=======
            wmOpacity = 0,
            watermarkSize = 32,
            positionWMFonts = 13;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

    private final int REQUEST_SELECT_PICTURE = 0x01,
            REQUEST_AD = 100;
    String BROADCAST_FILTER = "com.emergingcoders.imagestatus.Design.SampleQuotesActivity";
    String BROADCAST_FILTER_IN_APP = "com.emergingcoders.imagestatus.Design.PremiumFeaturesActivity";
    SharedPreferences preferences;

    ScaleGestureDetector scaleGestureDetector, scaleGestureDetector2;
    BroadcastReceiver broadcastReceiver, brInApp;

    InterstitialAd mInterstitialAd2, mInterstitialAd;
    private com.facebook.ads.InterstitialAd interstitialAd2, interstitialAd;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    boolean isAdInitialized = false;

    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    private String strClick = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SDK = Build.VERSION.SDK_INT;
        context = CreateQuoteActivity.this;

        super.onCreate(savedInstanceState);

        //Progressbar design not work properly in Pre-Lollipop versions so seprate design for it...
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_quote);

        preferences = getSharedPreferences("MP", MODE_PRIVATE);

        ivSave = mBinding.toolbarParentLayout.ivSaveCreatedQuote;
        tvQuote = findViewById(R.id.tv_create_quote_text);
        tvQuoteBy = findViewById(R.id.tv_create_quote_by);
        framePicture = findViewById(R.id.frame_create_quote);
        frameQuote = findViewById(R.id.ll_quote_frame);
<<<<<<< HEAD
        tvAddWM = mBinding.tvAddWaterMark;
        frameWM = mBinding.llAddWaterMark;
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        //Set first sample image into ImageView...
        setFullSizeSection(R.drawable.abc1);

        //Check for Water Mark...
        if (Globals.flagRemoveWm)
            mBinding.tvWaterMark.setVisibility(View.GONE);

        //Set font type for quotes which was last selected by user...
        AssetManager am = context.getApplicationContext().getAssets();
        typeface2 = Typeface.createFromAsset(am, preferences.getString("Fonts", "fonts/Bebas Neue Bold.ttf"));
        typeface3 = Typeface.createFromAsset(am, "fonts/American Typewriter Condensed Bold.ttf");
        typeface4 = Typeface.createFromAsset(am, "fonts/Roboto Regular.ttf");
        typeface5 = Typeface.createFromAsset(am, AppPreferences.getWMFont(context));
        tvQuote.setTypeface(typeface2);
        tvQuoteBy.setTypeface(typeface2);

        //Set layout of sample images...
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvSampleImages.setLayoutManager(layoutManager);

        //Set Adapter of sample images...
        adapterBackgroundQuotes = new Adapter_Background_Quotes(getSampleImages(),
                R.layout.layout_sample_backgound,
                context);
        mBinding.rvSampleImages.setAdapter(adapterBackgroundQuotes);
        framePicture.setOnTouchListener(this);

        //OnBackPressed in actionbar...
        mBinding.toolbarParentLayout.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                onBackPressed();
            }
        });

        //Save quote with image to app folder...
        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);

                saveQuoteDialog();
            }
        });

        mBinding.toolbarParentLayout.ivInAppOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(context, PremiumFeaturesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        //Open quote edit dialog...
        mBinding.llEditQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                changeQuoteText();
            }
        });

        //Open Font Style tab...
        mBinding.llFontAdjustQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                setFontStyles();
            }
        });

        //Pick image from Gallery...
        mBinding.llGalleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);

                strClick = "llGallery";

                if (!hasPermissions(context, PERMISSIONS))
                    ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                else
                    pickFromGallery();
            }
        });

        //Tint option for providing tint over image...
        mBinding.llTextColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                setFontColors();
            }
        });

        //Rotate Quote frame...
        mBinding.llQuoteTint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                setQuoteTint();
            }
        });

        mBinding.toolbarParentLayout.ivWatermark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                addWatermark();
            }
        });

        mBinding.toolbarParentLayout.rlWmToolbar.setOnClickListener(null);

        mBinding.rlAddWmOptions.setOnClickListener(null);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings mFirebaseconfigSettings = new FirebaseRemoteConfigSettings.Builder().build();
        mFirebaseRemoteConfig.setConfigSettings(mFirebaseconfigSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);

        if (!Globals.flagRemoveAds)
            showAdsSettings();

        //Initialize scale gesture for scaling font sizes...
        scaleGestureDetector = new ScaleGestureDetector(context,
                new MySimpleOnScaleGestureListener());

        //Get text sizes as in xml...
        Globals.quoteTextSize = (int) tvQuote.getTextSize();
        Globals.quoteBySize = (int) tvQuoteBy.getTextSize();

        //Just initialize if app fresh installed...
        new DbHelper(context);

        brInApp = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mBinding.tvWaterMark.setVisibility(View.GONE);
                flagBRInApp = true;
            }
        };
        registerReceiver(brInApp, new IntentFilter(BROADCAST_FILTER_IN_APP));

        //Adapter for font types...
        adapterFontType2 = new Adapter_Font_Family(getFontStyles(),
                R.layout.row_layout_font_types,
                context);

        //Its select the last selected font type from style tab...
<<<<<<< HEAD
        if (Adapter_Font_Family.selectedID != preferences.getInt("FontID", 13))
            Adapter_Font_Family.selectedID = preferences.getInt("FontID", 13);
=======
        if (adapterFontType2.selectedID != preferences.getInt("FontID", 13))
            adapterFontType2.selectedID = preferences.getInt("FontID", 13);
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        Globals.fontFamilyPosition = preferences.getInt("FontID", 13);

        //Its select the last selected font type from watermark...
<<<<<<< HEAD
        adapterWmFontFamily = new Adapter_WM_Font_Family(fontStyleArrayList,
                R.layout.row_layout_wm_font_types,
                context);
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        if (Adapter_WM_Font_Family.selectedID != AppPreferences.getWMFontID(context))
            Adapter_WM_Font_Family.selectedID = AppPreferences.getWMFontID(context);

        mBinding.tvAddWaterMark.setTypeface(typeface5);
        mBinding.tvAddWaterMark.setText(AppPreferences.getWMText(context));
        mBinding.tvAddWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.getWMFontSize(context));
        mBinding.tvAddWaterMark.setGravity(AppPreferences.getWMAlignment(context));
        mBinding.tvAddWaterMark.setTextColor(AppPreferences.getWMColor(context));
        mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
<<<<<<< HEAD
        mBinding.tvAddWaterMark.setRotation(AppPreferences.getWMLastRotate(context) * rotateMultiplier);
        mBinding.tvAddWaterMark.setShadowLayer(AppPreferences.getWMLastProgressRadius(context),
                AppPreferences.getWMLastX(context),
                AppPreferences.getWMLastY(context),
                AppPreferences.getWMShadowColor(context));
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.dialog_layout_quote_tint, null);
        sbTint = view.findViewById(R.id.sb_quote_tint);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (dialogChangeQuote != null && dialogChangeQuote.isShowing())
            dialogChangeQuote.dismiss();
        else if (dialogTextColors != null && dialogTextColors.isShowing())
            dialogTextColors.dismiss();
        else if (fragmentWindow != null && fragmentWindow.getDialog() != null)
            fragmentWindow.dismiss();
        else if (dialogFragmentWMFonts != null && dialogFragmentWMFonts.getDialog() != null)
            dialogFragmentWMFonts.dismiss();
        else if (mBinding.toolbarParentLayout.rlWmToolbar.getVisibility() == View.VISIBLE) {
            enableQuteFrame(true);
            enableWatermark(false);
        } else {
            if (Globals.isChangesOccurs) {
                final Dialog dialog = new Dialog(context, R.style.MyAlertDialog);
                dialog.setContentView(R.layout.layout_back_press_create_quote);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                Button btnNo = dialog.findViewById(R.id.btn_back_no);
                Button btnYes = dialog.findViewById(R.id.btn_back_yes);
                TextView tvText = dialog.findViewById(R.id.tv_create_back_text);

                tvText.setTypeface(typeface3);
                btnNo.setTypeface(typeface4);
                btnYes.setTypeface(typeface4);

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playSound(R.raw.button_tap);
                        dialog.dismiss();
                    }
                });

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fontStyleArrayList.clear();
                        Globals.tintLast = 0;
                        Globals.lastRotateProgress = 24;
                        Globals.lastTextColor = Color.WHITE;
                        Globals.lastTextShadowColor = Color.BLACK;
                        Globals.lastBackColor = Color.BLACK;
                        Globals.lastProgressX = 11;
                        Globals.lastProgressY = 11;
                        Globals.lastProgressRadius = 2;
                        Globals.lastX = 0;
                        Globals.lastY = 0;
                        Globals.isFontColor = false;
                        isBackColor = false;
                        deploayChanges(false);
                        playSound(R.raw.button_tap);
                        dialog.cancel();
                        dialog.dismiss();
                        context.finish();
                        if (flagBroadcast)
                            unregisterReceiver(broadcastReceiver);
                        if (flagBRInApp)
                            unregisterReceiver(brInApp);
                    }
                });
            } else {
                fontStyleArrayList.clear();
                Globals.tintLast = 0;
                Globals.lastRotateProgress = 24;
                Globals.lastTextColor = Color.WHITE;
                Globals.lastTextShadowColor = Color.BLACK;
                Globals.lastBackColor = Color.BLACK;
                Globals.lastProgressX = 11;
                Globals.lastProgressY = 11;
                Globals.lastProgressRadius = 2;
                Globals.lastX = 0;
                Globals.lastY = 0;
                Globals.isFontColor = false;
                isBackColor = false;
                deploayChanges(false);
                context.finish();
                if (flagBroadcast)
                    unregisterReceiver(broadcastReceiver);
                if (flagBRInApp)
                    unregisterReceiver(brInApp);
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialogChangeQuote != null && dialogChangeQuote.isShowing())
            dialogChangeQuote.dismiss();
        else if (dialogTextColors != null && dialogTextColors.isShowing())
            dialogTextColors.dismiss();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL) {
            if (grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                if (strClick.equalsIgnoreCase("ivSave"))
                    ivSave.performClick();
                else if (strClick.equalsIgnoreCase("llGallery"))
                    mBinding.llGalleryImage.performClick();
                else if (strClick.equalsIgnoreCase("ivShare"))
                    shareQuote(framePicture);
            }
        }
    }

    //OnTouch for picture section provides text zooming, moving and double tap etc...
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (doubleTap)
                    changeQuoteText();

                doubleTap = true;
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleTap = false;
                    }
                }, 250);

                dX = frameQuote.getX() - event.getRawX();
                dY = frameQuote.getY() - event.getRawY();
                Globals.lastAction = MotionEvent.ACTION_DOWN;
                deploayChanges(true);
                break;

            case MotionEvent.ACTION_MOVE:
                if (Globals.lastAction != MotionEvent.ACTION_POINTER_DOWN) {
                    frameQuote.setY(event.getRawY() + dY);
                    frameQuote.setX(event.getRawX() + dX);
                    Globals.lastAction = MotionEvent.ACTION_MOVE;
                    deploayChanges(true);
                }
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                Globals.lastAction = MotionEvent.ACTION_POINTER_DOWN;
                deploayChanges(true);
                break;

            case MotionEvent.ACTION_UP:
                Globals.lastAction = MotionEvent.ACTION_UP;
                deploayChanges(true);
                break;
            default:
                return false;
        }
        return true;
    }

    //Color library method after color selected...
    @Override
    public void onColorSelected(int dialogId, @ColorInt int color) {
<<<<<<< HEAD
        if (isFontColor) {
            tvQuote.setTextColor(color);
            tvQuoteBy.setTextColor(color);
            lastColor = color;
            btnTextColor.setBackgroundColor(color);
            dialogTextColors.setCancelable(false);
            isFontColor = false;
        } else if (isFontShadowColor) {
            tvQuote.setShadowLayer(lastProgressRadius, lastX, lastY, color);
            tvQuoteBy.setShadowLayer(lastProgressRadius, lastX, lastY, color);
            lastShadowColor = color;
            btnTextShadowColor.setBackgroundColor(color);
            dialogTextColors.setCancelable(false);
            isFontShadowColor = false;
        } else if (isBackColor) {
            final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

            Globals.lastBackColor = color;

            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
            mBinding.quoteTintView.setLayoutParams(layoutParams);
            mBinding.ivCreateQuote.setColorFilter(color);

            final RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

            layoutParams2.setMargins(60, 0, 60, 0);
            frameQuote.setLayoutParams(layoutParams2);

            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams1.gravity = Gravity.CENTER;
            layoutParams1.setMargins(0, 0, 0, 30);
            framePicture.setLayoutParams(layoutParams1);

            mBinding.quoteTintView.setBackgroundColor(Color.argb(0, 0, 0, 0));
            Globals.tintLast = 0;
            sbTint.setProgress(0);

            setSectionMax();

            deploayChanges(true);
        } else if (isWMSahdowColor) {
            lastWMSahdowColor = color;
            mBinding.tvAddWaterMark.setShadowLayer(AppPreferences.getWMLastProgressRadius(context),
                    AppPreferences.getWMLastX(context),
                    AppPreferences.getWMLastY(context),
                    color);
            mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
            btnWMTextShadowColor.setBackgroundColor(color);
            isWMSahdowColor = false;
            dialogWMColor.setCancelable(false);
        } else if (isWMColor) {
            lastWMColor = color;
            mBinding.tvAddWaterMark.setTextColor(lastWMColor);
            mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
            btnWMTextColor.setBackgroundColor(color);
            isWMColor = false;
            dialogWMColor.setCancelable(false);
=======
        deploayChanges(true);
        if (!isBackColor && !isWMColor) {
            if (isFontColor) {
                tvQuote.setTextColor(color);
                tvQuoteBy.setTextColor(color);
                lastColor = color;
                btnTextColor.setBackgroundColor(color);
                dialogTextColors.setCancelable(false);
            } else {
                tvQuote.setShadowLayer(lastProgressRadius, lastX, lastY, color);
                tvQuoteBy.setShadowLayer(lastProgressRadius, lastX, lastY, color);
                lastShadowColor = color;
                btnTextShadowColor.setBackgroundColor(color);
                dialogTextColors.setCancelable(false);
            }
        } else {
            if (!isWMColor) {
                final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                Globals.lastBackColor = color;

                mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                mBinding.quoteTintView.setLayoutParams(layoutParams);
                mBinding.ivCreateQuote.setColorFilter(color);

                final RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                layoutParams2.setMargins(60, 0, 60, 0);
                frameQuote.setLayoutParams(layoutParams2);

                FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams1.gravity = Gravity.CENTER;
                layoutParams1.setMargins(0, 0, 0, 30);
                framePicture.setLayoutParams(layoutParams1);

                mBinding.quoteTintView.setBackgroundColor(Color.argb(0, 0, 0, 0));
                sbTint.setProgress(0);

                setSectionMax();

                isBackColor = true;
                deploayChanges(true);
            } else {
                lastWMColor = color;
                mBinding.tvAddWaterMark.setTextColor(lastWMColor);
                mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
                mBinding.tvAddWaterMark.post(new Runnable() {
                    @Override
                    public void run() {
                        watermarkColor();
                    }
                });
            }
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        }
    }

    @Override
    public void onDialogDismissed(int dialogId) {
<<<<<<< HEAD
        isFontColor = false;
        isFontShadowColor = false;
        isBackColor = false;
        isWMColor = false;
        isWMSahdowColor = false;
    }


=======
        Globals.isFontColor = false;
    }

>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    //Check Multiple Permissions...
    private boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    //Its a Gesture detector for two finger zooming text...
    private class MySimpleOnScaleGestureListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        float factor;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            factor = 1.0f;
            return true;
            //return super.onScaleBegin(detector);
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = tvQuote.getTextSize();
            factor = detector.getScaleFactor();

            tvQuote.setTextSize(TypedValue.COMPLEX_UNIT_PX, (factor * scaleFactor));
            tvQuoteBy.setTextSize(TypedValue.COMPLEX_UNIT_PX, (factor * scaleFactor) / 2);

            Globals.quoteTextSize = (int) (tvQuote.getTextSize());
            Globals.quoteBySize = (int) (tvQuoteBy.getTextSize());
            deploayChanges(true);
            return true;
        }
    }

    private class MyWatermarkOnScaleGestureListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        float factor;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            factor = 1.0f;
            return true;
            //return super.onScaleBegin(detector);
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = mBinding.tvAddWaterMark.getTextSize();
            factor = detector.getScaleFactor();

            mBinding.tvAddWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_PX, (factor * scaleFactor));

            AppPreferences.setWMFontSize(context, (int) (mBinding.tvAddWaterMark.getTextSize()));
            watermarkSize = (int) (mBinding.tvAddWaterMark.getTextSize());
            deploayChanges(true);
            return true;
        }
    }

    //Take screenshot of Image...
    private void saveQuote(View view) {
        if (Globals.isChangesOccurs) {
            if (!Globals.flagRemoveAds)
                requestNewInterstitial();
            if (AppPreferences.getSaveType(context) == R.id.btn_save_default) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                view.draw(canvas);

                FileOutputStream fOut = null;
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;

                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    Toast.makeText(context, "Image Saved In Gallery", Toast.LENGTH_LONG).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                        }
                    }, 500);

                    deploayChanges(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
                        if (fOut != null) {
                            fOut.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (AppPreferences.getSaveType(context) == R.id.btn_save_1o5_x) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

<<<<<<< HEAD
                view.draw(canvas);

                Bitmap bitmap1o5X = getResizedBitmap(bitmap, view.getWidth() * 1.5f, view.getHeight() * 1.5f);

                FileOutputStream fOut = null;
=======
                deploayChanges(false);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;

                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap1o5X.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    Toast.makeText(context, "Image Saved In Gallery", Toast.LENGTH_LONG).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                        }
                    }, 500);

                    deploayChanges(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
                        if (fOut != null) {
                            fOut.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (AppPreferences.getSaveType(context) == R.id.btn_save_2_x) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                view.draw(canvas);

                Bitmap bitmap2X = getResizedBitmap(bitmap, view.getWidth() * 2f, view.getHeight() * 2f);

                FileOutputStream fOut = null;
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;

                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap2X.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    Toast.makeText(context, "Image Saved In Gallery", Toast.LENGTH_LONG).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                        }
                    }, 500);

                    deploayChanges(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
                        if (fOut != null) {
                            fOut.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else
            Toast.makeText(context, "File already downloaded!", Toast.LENGTH_SHORT).show();
    }

    //Save and share image to other apps...
    private void shareQuote(final View view) {
<<<<<<< HEAD
        if (!Globals.flagRemoveAds)
            requestNewInterstitial2();

        final String filePath = getAppPath(context) + lastSavedImage;

        File myFile = new File(filePath);

        if (!Globals.isChangesOccurs && myFile.exists()) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text) + "\n" +
                    "\nClick here to install:\n" +
                    getResources().getString(R.string.app_playstore_path));
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
            startActivityForResult(Intent.createChooser(intent, "Share"), REQUEST_AD);
            mBinding.toolbarParentLayout.pbShare.setVisibility(View.GONE);
            mBinding.toolbarParentLayout.ivSaveCreatedQuote.setVisibility(View.VISIBLE);
        } else {
            if (AppPreferences.getSaveType(context) == R.id.btn_save_default) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
=======
        mBinding.toolbarParentLayout.pbShare.setVisibility(View.VISIBLE);
        mBinding.toolbarParentLayout.ivSaveCreatedQuote.setVisibility(View.GONE);
        mBinding.toolbarParentLayout.pbShare.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Globals.isChangesOccurs) {
                    Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);

                    view.draw(canvas);
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

                view.draw(canvas);

                FileOutputStream fOut = null;
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;

                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                            deploayChanges(false);
                            shareQuote(view);
                        }
                    }, 500);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
<<<<<<< HEAD
                        if (fOut != null) {
                            fOut.close();
=======
                        final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                        fOut = new FileOutputStream(sdImageMainDirectory);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                        fOut.flush();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                refreshGallery(context, sdImageMainDirectory);

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text) + "\n" +
                                        "\nClick here to install:\n" +
                                        getResources().getString(R.string.app_playstore_path));
                                intent.setType("image/*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
                                startActivityForResult(Intent.createChooser(intent, "Share"), REQUEST_AD);
                                mBinding.toolbarParentLayout.pbShare.setVisibility(View.GONE);
                                mBinding.toolbarParentLayout.ivSaveCreatedQuote.setVisibility(View.VISIBLE);
                                if (!Globals.flagRemoveAds)
                                    requestNewInterstitial2();
                            }
                        }, 500);
                        deploayChanges(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (fOut != null) {
                                fOut.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
<<<<<<< HEAD
                }
            } else if (AppPreferences.getSaveType(context) == R.id.btn_save_1o5_x) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                view.draw(canvas);

                Bitmap bitmap1o5X = getResizedBitmap(bitmap, view.getWidth() * 1.5f, view.getHeight() * 1.5f);

                FileOutputStream fOut = null;
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;
=======
                } else {
                    final String filePath = getAppPath(context) + lastSavedImage;

                    File myFile = new File(filePath);

                    if (myFile.exists()) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                        intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text) + "\n" +
                                "\nClick here to install:\n" +
                                getResources().getString(R.string.app_playstore_path));
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
                        startActivityForResult(Intent.createChooser(intent, "Share"), REQUEST_AD);
                        mBinding.toolbarParentLayout.pbShare.setVisibility(View.GONE);
                        mBinding.toolbarParentLayout.ivSaveCreatedQuote.setVisibility(View.VISIBLE);
                        if (!Globals.flagRemoveAds)
                            requestNewInterstitial2();
                    } else {
                        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(bitmap);
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap1o5X.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                            deploayChanges(false);
                            shareQuote(view);
                        }
                    }, 500);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
                        if (fOut != null) {
                            fOut.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (AppPreferences.getSaveType(context) == R.id.btn_save_2_x) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                view.draw(canvas);

                Bitmap bitmap2X = getResizedBitmap(bitmap, view.getWidth() * 2f, view.getHeight() * 2f);

                FileOutputStream fOut = null;
                try {
                    String fileName = "PictureQuotes" + System.currentTimeMillis() / 1000 + ".jpg";
                    lastSavedImage = fileName;

<<<<<<< HEAD
                    final File sdImageMainDirectory = new File(getAppPath(context), fileName);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                    bitmap2X.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshGallery(context, sdImageMainDirectory);
                            deploayChanges(false);
                            shareQuote(view);
                        }
                    }, 500);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                } finally {
                    try {
                        if (fOut != null) {
                            fOut.close();
=======
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    refreshGallery(context, sdImageMainDirectory);

                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                                    intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text) + "\n" +
                                            "\nClick here to install:\n" +
                                            getResources().getString(R.string.app_playstore_path));
                                    intent.setType("image/*");
                                    intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
                                    startActivityForResult(Intent.createChooser(intent, "Share"), REQUEST_AD);
                                    mBinding.toolbarParentLayout.pbShare.setVisibility(View.GONE);
                                    mBinding.toolbarParentLayout.ivSaveCreatedQuote.setVisibility(View.VISIBLE);

                                    if (!Globals.flagRemoveAds)
                                        requestNewInterstitial2();
                                }
                            }, 500);
                            deploayChanges(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (fOut != null) {
                                    fOut.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private Bitmap getResizedBitmap(Bitmap bm, float newWidth, float newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = (newWidth) / width;
        float scaleHeight = (newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Get directory...
    private String getAppPath(Context context) {
        File dir = new File(android.os.Environment.getExternalStorageDirectory(),
                context.getResources().getString(R.string.app_name) +
                        File.separator + "Created - " +
                        context.getResources().getString(R.string.app_name) +
                        File.separator);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getPath() + File.separator;
    }

    //Set no. of sample images...
    private ArrayList<Integer> getSampleImages() {
        sampleImagesList.add(R.drawable.ic_sample_color_picker);
        sampleImagesList.add(R.drawable.abc1);
        sampleImagesList.add(R.drawable.abc2);
        sampleImagesList.add(R.drawable.abc3);
        sampleImagesList.add(R.drawable.abc4);
        sampleImagesList.add(R.drawable.abc5);
        sampleImagesList.add(R.drawable.abc6);
        sampleImagesList.add(R.drawable.abc7);
        sampleImagesList.add(R.drawable.abc8);
        sampleImagesList.add(R.drawable.abc9);
        sampleImagesList.add(R.drawable.abc10);
        sampleImagesList.add(R.drawable.abc11);
        sampleImagesList.add(R.drawable.abc12);
        sampleImagesList.add(R.drawable.abc13);
        sampleImagesList.add(R.drawable.abc14);
        sampleImagesList.add(R.drawable.abc15);
        sampleImagesList.add(R.drawable.abc16);
        sampleImagesList.add(R.drawable.abc17);
        sampleImagesList.add(R.drawable.abc18);
        sampleImagesList.add(R.drawable.abc19);
        sampleImagesList.add(R.drawable.abc20);
        sampleImagesList.add(R.drawable.abc21);
        sampleImagesList.add(R.drawable.abc22);
        sampleImagesList.add(R.drawable.abc23);
        sampleImagesList.add(R.drawable.abc24);
        sampleImagesList.add(R.drawable.abc25);
        sampleImagesList.add(R.drawable.abc27);
        sampleImagesList.add(R.drawable.abc28);
        sampleImagesList.add(R.drawable.abc29);
        sampleImagesList.add(R.drawable.abc30);
        sampleImagesList.add(R.drawable.abc31);
        sampleImagesList.add(R.drawable.abc32);
        sampleImagesList.add(R.drawable.abc33);
        sampleImagesList.add(R.drawable.abc34);
        sampleImagesList.add(R.drawable.abc35);
        sampleImagesList.add(R.drawable.abc36);
        sampleImagesList.add(R.drawable.abc37);
        sampleImagesList.add(R.drawable.abc38);
        sampleImagesList.add(R.drawable.abc39);
        sampleImagesList.add(R.drawable.abc40);
        sampleImagesList.add(R.drawable.abc41);
        sampleImagesList.add(R.drawable.abc42);
        sampleImagesList.add(R.drawable.abc43);

        return sampleImagesList;
    }

    //Show popup for editing quote...
    private void changeQuoteText() {
        dialogChangeQuote = new Dialog(context, R.style.MY_DIALOG_Edit_Quote);
        dialogChangeQuote.setCancelable(true);
        dialogChangeQuote.setContentView(R.layout.dialog_layout_edit_quote_text);

        final EditText etQuote = dialogChangeQuote.findViewById(R.id.et_quote_text);
        final EditText etQuoteBy = dialogChangeQuote.findViewById(R.id.et_quote_by);
        ImageView ivCancelChange = dialogChangeQuote.findViewById(R.id.iv_cancel_quote_change);
        final ImageView ivClearChange = dialogChangeQuote.findViewById(R.id.iv_clear_quote_change);
        ImageView ivDoneChange = dialogChangeQuote.findViewById(R.id.iv_done_quote_change);
        ImageView ivSampleQuoteRandom = dialogChangeQuote.findViewById(R.id.iv_sample_quotes_random);
        ImageView ivSampleQuoteGet = dialogChangeQuote.findViewById(R.id.iv_sample_quotes_get);
        ImageView ivSampleQuoteRecent = dialogChangeQuote.findViewById(R.id.iv_sample_quotes_recently);

        dialogChangeQuote.show();

        if (!flagNoText)
            strQuoteText = tvQuote.getText().toString();
        if (tvQuoteBy.getText().toString().contains("-"))
            strQuoteBy = tvQuoteBy.getText().toString().replace("-", "").trim();
        else
            strQuoteBy = "";

        if (!strQuoteText.equalsIgnoreCase(""))
            etQuote.setText(strQuoteText);
        if (!strQuoteBy.equalsIgnoreCase(""))
            etQuoteBy.setText(strQuoteBy);

        etQuote.setFocusable(true);
        etQuoteBy.setFocusable(true);

        if (!etQuote.getText().toString().equalsIgnoreCase(""))
            ivClearChange.setImageResource(R.drawable.ic_clear_text_2);
        else
            ivClearChange.setImageResource(R.drawable.ic_clear_text);

        etQuote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etQuote.getText().toString().equalsIgnoreCase("") && etQuoteBy.getText().toString().equalsIgnoreCase(""))
                    ivClearChange.setImageResource(R.drawable.ic_clear_text);
                else
                    ivClearChange.setImageResource(R.drawable.ic_clear_text_2);
            }
        });

        etQuoteBy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etQuote.getText().toString().equalsIgnoreCase("") && etQuoteBy.getText().toString().equalsIgnoreCase(""))
                    ivClearChange.setImageResource(R.drawable.ic_clear_text);
                else
                    ivClearChange.setImageResource(R.drawable.ic_clear_text_2);
            }
        });

        ivCancelChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogChangeQuote.dismiss();
            }
        });

        ivClearChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                etQuote.setText("");
                etQuoteBy.setText("");
                strQuoteText = "";
                strQuoteBy = "";
            }
        });

        ivDoneChange.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                strQuoteText = etQuote.getText().toString();
                strQuoteBy = etQuoteBy.getText().toString();

                tvQuote.setBackgroundColor(Color.TRANSPARENT);

                if (!etQuote.getText().toString().equalsIgnoreCase("")) {
                    tvQuote.setText(strQuoteText);
                    flagNoText = false;
                    deploayChanges(true);
                }

                if (!etQuoteBy.getText().toString().equalsIgnoreCase(""))
                    tvQuoteBy.setText("- " + strQuoteBy);
                else
                    tvQuoteBy.setText("");

                if (etQuote.getText().toString().equalsIgnoreCase("") && etQuoteBy.getText().toString().equalsIgnoreCase("")) {
                    strQuoteBy = "";
                    strQuoteText = "";
                    tvQuote.setText("Double tap to edit");
                    tvQuoteBy.setText("");
                    tvQuote.setBackgroundColor(Color.parseColor("#66000000"));
                    flagNoText = true;
                }

                DbHelper dbHelper = new DbHelper(context);
                if (!etQuote.getText().toString().equalsIgnoreCase(""))
                    dbHelper.addRecentQuotes(etQuote.getText().toString(),
                            etQuoteBy.getText().toString());

                dialogChangeQuote.dismiss();
            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onReceive(Context context, Intent intent) {
                etQuote.setText("" + intent.getStringExtra("SampleQuote").trim());
                etQuoteBy.setText("" + intent.getStringExtra("SampleQuoteBy").trim());
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_FILTER));
        flagBroadcast = true;

        ivSampleQuoteGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(context, SampleQuotesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("QuoteCategory", "All");
                startActivity(intent);
            }
        });

        ivSampleQuoteRandom.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                DbHelper dbHelper = new DbHelper(context);
                etQuote.setText("" + dbHelper.getSingleQuote().trim());
            }
        });

        ivSampleQuoteRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(context, RecentQuotesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    //Adjust font align, spacing, size, color, shadow etc from dialog using tabs!...
    public void setFontStyles() {
        fragmentWindow = new DialogFragmentWindow();
        fragmentWindow.show(getSupportFragmentManager(), "");
    }

    //Adjust font align, spacing, size, color, shadow etc from dialog using tabs!...
    public void setWMFontStyles() {
        dialogFragmentWMFonts = new DialogFragmentWMFonts();
        dialogFragmentWMFonts.show(getSupportFragmentManager(), "");
    }

    //Manually close font style tabs from fragments...
    public void closeFontDialog() {
        if (fragmentWindow != null)
            fragmentWindow.dismiss();
    }

    //Manually close font style tabs from fragments...
    public void closeWMFontDialog() {
        if (dialogFragmentWMFonts != null)
            dialogFragmentWMFonts.dismiss();
    }

    //Give tint over image back of the quote text...
    private void setQuoteTint() {
<<<<<<< HEAD
        final DialogLayoutQuoteTintBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_quote_tint,
                null,
                false);

=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        dialogQuoteTint = new Dialog(context, R.style.QuoteDialog);
        dialogQuoteTint.setCancelable(true);
        dialogQuoteTint.setContentView(binding.getRoot());
        dialogQuoteTint.show();

        sbTint = dialogQuoteTint.findViewById(R.id.sb_quote_tint);
        sbTint.setProgress(Globals.tintLast);

        sbTint.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
<<<<<<< HEAD
                mBinding.quoteTintView.setBackgroundColor(Color.argb(progress, 0, 0, 0));
                deploayChanges(true);
                binding.llConfirmQuoteTint.setVisibility(View.VISIBLE);
                tintLast = progress;
=======
                mBinding.quoteTintView.setBackgroundColor(Color.argb(progress * Globals.tintMultiplier, 0, 0, 0));
                deploayChanges(true);
                layoutConfirm.setVisibility(View.VISIBLE);
                progress1[0] = progress;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                dialogQuoteTint.setCancelable(false);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogQuoteTint.dismiss();
<<<<<<< HEAD
                Globals.tintLast = tintLast;
=======
                Globals.tintLast = progress1[0];
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogQuoteTint.dismiss();
<<<<<<< HEAD
                binding.sbQuoteTint.setProgress(Globals.tintLast);
                mBinding.quoteTintView.setBackgroundColor(Color.argb(Globals.tintLast, 0, 0, 0));
=======
                mBinding.quoteTintView.setBackgroundColor(Color.argb(Globals.tintLast * Globals.tintMultiplier, 0, 0, 0));
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                deploayChanges(true);
            }
        });
    }

    //Set colors for text and shadow...
    private void setFontColors() {
<<<<<<< HEAD
        DialogLayoutTextColorPickerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_text_color_picker,
                null,
                false);
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
        dialogTextColors = new Dialog(context, R.style.QuoteDialog);
        dialogTextColors.setCancelable(true);
        dialogTextColors.setContentView(binding.getRoot());
        dialogTextColors.show();

<<<<<<< HEAD
        btnTextColor = binding.btnTextColorPicker;
        btnTextShadowColor = binding.btnTextShadowColorPicker;
=======
        btnTextColor = dialogTextColors.findViewById(R.id.btn_text_color_picker);
        btnTextShadowColor = dialogTextColors.findViewById(R.id.btn_text_shadow_color_picker);
        LinearLayout llApply = dialogTextColors.findViewById(R.id.ll_apply);
        LinearLayout llCancel = dialogTextColors.findViewById(R.id.ll_cancel);
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        binding.btnTextColorPicker.setBackgroundColor(Globals.lastTextColor);
        binding.btnTextShadowColorPicker.setBackgroundColor(Globals.lastTextShadowColor);

        binding.btnTextColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isFontColor = true;
                isBackColor = false;
                ColorPickerDialog.newBuilder().setColor(lastColor).show(context);
            }
        });

        binding.btnTextShadowColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isFontShadowColor = true;
                isBackColor = false;
                ColorPickerDialog.newBuilder().setColor(lastShadowColor).show(context);
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogTextColors.dismiss();
                Globals.lastTextColor = lastColor;
                Globals.lastTextShadowColor = lastShadowColor;
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogTextColors.dismiss();
                lastColor = Globals.lastTextColor;
                lastShadowColor = Globals.lastTextShadowColor;
                tvQuote.setTextColor(Globals.lastTextColor);
                tvQuoteBy.setTextColor(Globals.lastTextColor);
                tvQuote.setShadowLayer(lastProgressRadius, lastX, lastY, Globals.lastTextShadowColor);
                tvQuoteBy.setShadowLayer(lastProgressRadius, lastX, lastY, Globals.lastTextShadowColor);
            }
        });
    }

<<<<<<< HEAD
    @SuppressLint("SetTextI18n")
    private void saveQuoteDialog() {
        final DialogSaveQuoteBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
=======
    private void saveQuoteDialog() {
        DialogSaveQuoteBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                R.layout.dialog_save_quote,
                null,
                false);

        dialogWatermark = new Dialog(context, R.style.QuoteDialog);
<<<<<<< HEAD
        dialogWatermark.setCancelable(true);
        dialogWatermark.setContentView(binding.getRoot());
        dialogWatermark.show();

        if (AppPreferences.getSaveType(context) == R.id.btn_save_default) {
            binding.ivSaveDefault.setImageResource(R.drawable.ic_save_default);
            binding.tvSaveDefault.setTextColor(Color.BLACK);
            binding.tvSaveNormal.setTextColor(Color.BLACK);
        } else if (AppPreferences.getSaveType(context) == R.id.btn_save_1o5_x) {
            binding.ivSave1o5X.setImageResource(R.drawable.ic_save_1o5x);
            binding.tvSave1o5X.setTextColor(Color.BLACK);
            binding.tvSaveLarge.setTextColor(Color.BLACK);
        } else if (AppPreferences.getSaveType(context) == R.id.btn_save_2_x) {
            binding.ivSave2X.setImageResource(R.drawable.ic_save_2x);
            binding.tvSave2X.setTextColor(Color.BLACK);
            binding.tvSaveExtraLarge.setTextColor(Color.BLACK);
        }

        binding.tvSaveDefault.setText(framePicture.getLayoutParams().width +
                "x" +
                framePicture.getLayoutParams().height);
        binding.tvSave1o5X.setText((int) (framePicture.getLayoutParams().width * 1.5) +
                "x" +
                (int) (framePicture.getLayoutParams().height * 1.5));
        binding.tvSave2X.setText((framePicture.getLayoutParams().width * 2) +
                "x" +
                (framePicture.getLayoutParams().height * 2));

        binding.btnShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.water);
                dialogWatermark.dismiss();

                strClick = "ivShare";

                if (!hasPermissions(context, PERMISSIONS))
                    ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                else
                    shareQuote(framePicture);
            }
        });

        binding.btnSaveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.water);
                dialogWatermark.dismiss();

                strClick = "ivSave";

                if (!hasPermissions(context, PERMISSIONS))
                    ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                else
                    saveQuote(framePicture);
            }
        });

        binding.btnSaveDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                deploayChanges(true);

                binding.ivSaveDefault.setImageResource(R.drawable.ic_save_default);
                binding.tvSaveDefault.setTextColor(Color.BLACK);
                binding.tvSaveNormal.setTextColor(Color.BLACK);
                AppPreferences.setSaveType(context, R.id.btn_save_default);

                binding.ivSave1o5X.setImageResource(R.drawable.ic_save_1o5x_gray);
                binding.tvSave1o5X.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveLarge.setTextColor(Color.parseColor("#CCCCCC"));

                binding.ivSave2X.setImageResource(R.drawable.ic_save_2x_gray);
                binding.tvSave2X.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveExtraLarge.setTextColor(Color.parseColor("#CCCCCC"));
            }
        });

        binding.btnSave1o5X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                deploayChanges(true);

                binding.ivSave1o5X.setImageResource(R.drawable.ic_save_1o5x);
                binding.tvSave1o5X.setTextColor(Color.BLACK);
                binding.tvSaveLarge.setTextColor(Color.BLACK);
                AppPreferences.setSaveType(context, R.id.btn_save_1o5_x);

                binding.ivSave2X.setImageResource(R.drawable.ic_save_2x_gray);
                binding.tvSave2X.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveExtraLarge.setTextColor(Color.parseColor("#CCCCCC"));

                binding.ivSaveDefault.setImageResource(R.drawable.ic_save_default_gray);
                binding.tvSaveDefault.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveNormal.setTextColor(Color.parseColor("#CCCCCC"));
            }
        });

        binding.btnSave2X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                deploayChanges(true);

                binding.ivSave2X.setImageResource(R.drawable.ic_save_2x);
                binding.tvSave2X.setTextColor(Color.BLACK);
                binding.tvSaveExtraLarge.setTextColor(Color.BLACK);
                AppPreferences.setSaveType(context, R.id.btn_save_2_x);

                binding.ivSaveDefault.setImageResource(R.drawable.ic_save_default_gray);
                binding.tvSaveDefault.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveNormal.setTextColor(Color.parseColor("#CCCCCC"));

                binding.ivSave1o5X.setImageResource(R.drawable.ic_save_1o5x_gray);
                binding.tvSave1o5X.setTextColor(Color.parseColor("#CCCCCC"));
                binding.tvSaveLarge.setTextColor(Color.parseColor("#CCCCCC"));
            }
        });
    }

    private void addWatermark() {
        enableQuteFrame(false);
        enableWatermark(true);
        mBinding.tvAddWaterMark.setText(AppPreferences.getWMText(context));
        mBinding.toolbarParentLayout.ivAddWmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                DialogLayoutBackWatermarkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                        R.layout.dialog_layout_back_watermark,
                        null,
                        false);
                final Dialog dialog = new Dialog(context, R.style.MyAlertDialog);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.getWindow().setDimAmount(.35f);
                dialog.setContentView(binding.getRoot());
                dialog.show();

                binding.tvWmBackText.setTypeface(typeface3);
                binding.btnBackNo.setTypeface(typeface4);
                binding.btnBackYes.setTypeface(typeface4);

                binding.btnBackNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playSound(R.raw.button_tap);
                        dialog.dismiss();
                    }
                });

                binding.btnBackYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playSound(R.raw.button_tap);
                        dialog.dismiss();
                        mBinding.tvAddWaterMark.setText("");
                        enableQuteFrame(true);
                        enableWatermark(false);
                    }
                });
            }
        });

        mBinding.toolbarParentLayout.ivAddWmDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                enableWatermark(false);
                enableQuteFrame(true);
            }
        });

        mBinding.llWmSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                setWMFontStyles();
            }
        });

        mBinding.llEditWm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                editWatermark();
            }
        });

        mBinding.llWmOpacity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkOpacity();
            }
        });

        mBinding.llWmColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkColor();
            }
        });
    }

    private void editWatermark() {
        final DialogEditWatermarkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_edit_watermark,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.MY_DIALOG_Edit_Quote);
        dialog.setCancelable(true);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        binding.etWmText.setText(AppPreferences.getWMText(context));

        if (!binding.etWmText.getText().toString().equalsIgnoreCase(""))
            binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text_2);
        else
            binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text);

        binding.etWmText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etWmText.getText().toString().equalsIgnoreCase(""))
                    binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text);
                else
                    binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text_2);
            }
        });

        binding.ivCancelWmChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialog.dismiss();
            }
        });

        binding.ivClearWmChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                binding.etWmText.setText("");
                AppPreferences.setWMText(context, "");
            }
        });

        binding.ivDoneWmChange.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);

                AppPreferences.setWMText(context, binding.etWmText.getText().toString());
                mBinding.tvAddWaterMark.setText(binding.etWmText.getText().toString());
                deploayChanges(true);

                dialog.dismiss();
            }
        });
    }

    private void watermarkOpacity() {
        final DialogLayoutWmOpacityBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_opacity,
                null,
                false);
        dialogWMOpacity = new Dialog(context, R.style.QuoteDialog);
        dialogWMOpacity.setCancelable(true);
        dialogWMOpacity.setContentView(binding.getRoot());
        dialogWMOpacity.show();

        binding.sbWmOpacity.setProgress(AppPreferences.getWMOpacity(context));
        mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));

        binding.sbWmOpacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.tvAddWaterMark.onSetAlpha(255 - progress);
                dialogWMOpacity.setCancelable(false);
                wmOpacity = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogWMOpacity.dismiss();
                AppPreferences.setWMOpacity(context, wmOpacity);
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogWMOpacity.dismiss();
                binding.sbWmOpacity.setProgress(AppPreferences.getWMOpacity(context));
                mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
            }
        });
    }

    private void watermarkColor() {
        DialogLayoutWmColorBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_color,
                null,
                false);
        dialogWMColor = new Dialog(context, R.style.QuoteDialog);
        dialogWMColor.setCancelable(true);
        dialogWMColor.setContentView(binding.getRoot());
        dialogWMColor.show();

        btnWMTextColor = binding.btnTextColorPicker;
        btnWMTextShadowColor = binding.btnTextShadowColorPicker;

        lastWMColor = AppPreferences.getWMColor(context);
        lastWMSahdowColor = AppPreferences.getWMShadowColor(context);

        binding.btnTextColorPicker.setBackgroundColor(AppPreferences.getWMColor(context));
        binding.btnTextShadowColorPicker.setBackgroundColor(AppPreferences.getWMShadowColor(context));

        binding.btnTextColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isWMColor = true;
                isBackColor = false;
                ColorPickerDialog.newBuilder().setColor(lastWMColor).show(context);
            }
        });

        binding.btnTextShadowColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isWMSahdowColor = true;
                isBackColor = false;
                ColorPickerDialog.newBuilder().setColor(lastWMSahdowColor).show(context);
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                AppPreferences.setWMColor(context, lastWMColor);
                AppPreferences.setWMShadowColor(context, lastWMSahdowColor);
                dialogWMColor.dismiss();
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.tvAddWaterMark.setTextColor(AppPreferences.getWMColor(context));
                mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
                mBinding.tvAddWaterMark.setShadowLayer(AppPreferences.getWMLastProgressRadius(context),
                        AppPreferences.getWMLastX(context),
                        AppPreferences.getWMLastY(context),
                        AppPreferences.getWMShadowColor(context));
                dialogWMColor.dismiss();
            }
        });
    }

    private void changeStatusBar(boolean isAddWM) {
        if (isAddWM) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                mBinding.statusBarCreateQuote.setVisibility(View.VISIBLE);
                Window window = context.getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                int statusBarHeight = getStatusBarHeight(context);

                View view = new View(context);
                view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                view.getLayoutParams().height = statusBarHeight;
                ((ViewGroup) window.getDecorView()).addView(view);
                view.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark2));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                mBinding.statusBarCreateQuote.setVisibility(View.VISIBLE);
                Window window = context.getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                int statusBarHeight = getStatusBarHeight(context);

                View view = new View(context);
                view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                view.getLayoutParams().height = statusBarHeight;
                ((ViewGroup) window.getDecorView()).addView(view);
                view.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            }
        }
    }

    private void enableQuteFrame(boolean enable) {
        if (enable) {
            framePicture.setOnTouchListener(this);
            scaleGestureDetector = new ScaleGestureDetector(context, new MySimpleOnScaleGestureListener());
        } else {
            framePicture.setOnTouchListener(null);
            scaleGestureDetector = null;
        }
    }

    private void enableWatermark(boolean enable) {
        if (enable) {
            scaleGestureDetector2 = new ScaleGestureDetector(context, new MyWatermarkOnScaleGestureListener());

            slideDownShowView(mBinding.toolbarParentLayout.rlWmToolbar);
            slideUpShowView(mBinding.rlAddWmOptions);

            changeStatusBar(true);

            mBinding.llAddWaterMark.setVisibility(View.VISIBLE);
            mBinding.llAddWaterMark.setFocusableInTouchMode(true);
            mBinding.llAddWaterMark.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    scaleGestureDetector2.onTouchEvent(event);
                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            dX1 = mBinding.llAddWaterMark.getX() - event.getRawX();
                            dY1 = mBinding.llAddWaterMark.getY() - event.getRawY();

                            lastWMAction = MotionEvent.ACTION_DOWN;

                            deploayChanges(true);

                            if (doubleTap)
                                editWatermark();
                            doubleTap = true;
                            handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doubleTap = false;
                                }
                            }, 250);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (lastWMAction != MotionEvent.ACTION_POINTER_DOWN) {
                                mBinding.llAddWaterMark.setY(event.getRawY() + dY1);
                                mBinding.llAddWaterMark.setX(event.getRawX() + dX1);
                                deploayChanges(true);
                            }
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            lastWMAction = MotionEvent.ACTION_POINTER_DOWN;
                            deploayChanges(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            lastWMAction = MotionEvent.ACTION_UP;
                            deploayChanges(true);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });
        } else {
            slideUpHideView(mBinding.toolbarParentLayout.rlWmToolbar);
            slideDownHideView(mBinding.rlAddWmOptions);
            changeStatusBar(false);
            mBinding.llAddWaterMark.setOnTouchListener(null);
            mBinding.llAddWaterMark.setFocusableInTouchMode(false);
            scaleGestureDetector2 = null;
        }
    }

    private void slideUpShowView(final View view) {
        final Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.pull_up_popup);
        view.startAnimation(slideUp);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideUpHideView(final View view) {
        final Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.toolbar_up);
        view.startAnimation(slideUp);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideDownShowView(final View view) {
        final Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.toolbar_down);
        view.startAnimation(slideDown);
        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideDownHideView(final View view) {
        final Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.pull_down_popup);
        view.startAnimation(slideDown);
        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void deploayChanges(boolean isDetect) {
        if (isDetect) {
            ivSave.setImageResource(R.drawable.ic_file_download);
            Globals.isChangesOccurs = true;
        } else {
            ivSave.setImageResource(R.drawable.ic_downloaded_file);
            Globals.isChangesOccurs = false;
        }
    }

    //Get exact quote & image width...
    private void setSectionMax() {
        ViewTreeObserver observer = mBinding.ivCreateQuote.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mBinding.ivCreateQuote.getViewTreeObserver().removeOnPreDrawListener(this);
                Globals.sectionWidth = frameQuote.getMeasuredWidth();
                Globals.deviceWidth = mBinding.ivCreateQuote.getMeasuredWidth();
                Globals.sectionHeight = frameQuote.getMeasuredHeight();
                return true;
            }
        });
    }

    //Get all available fonts from assets...
    private ArrayList<Model_Font_Style> getFontStyles() {
        try {
            String[] f = getAssets().list("fonts");
            for (String f1 : f) {
                String fileName = f1.replace("_", " ").substring(0, f1.lastIndexOf("."));
                Model_Font_Style modelFontStyle = new Model_Font_Style(fileName, f1);
                fontStyleArrayList.add(modelFontStyle);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fontStyleArrayList;
    }

    //Pick image from Gallery...
    private void pickFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Choose from Gallery"), REQUEST_SELECT_PICTURE);
    }

    //Refresh Gallery after download image...
    private void refreshGallery(Context mContext, File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        mContext.sendBroadcast(mediaScanIntent);
    }

    //Get image from gallery and process it to crop and re process image sizes...
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AD) {
            if (mInterstitialAd2 != null && mInterstitialAd2.isLoaded())
                mInterstitialAd2.show();
            else if (interstitialAd2 != null && interstitialAd2.isAdLoaded())
                interstitialAd2.show();
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                //Get image path of Gallery...
=======
        dialogWatermark.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogWatermark.setCancelable(false);
        dialogWatermark.setCanceledOnTouchOutside(false);
        dialogWatermark.getWindow().setGravity(Gravity.BOTTOM);
        dialogWatermark.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogWatermark.setContentView(binding.getRoot());
        dialogWatermark.show();

        binding.btnAddWmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogWatermark.dismiss();
            }
        });

        binding.btnShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.water);
                dialogWatermark.dismiss();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

                strClick = "ivShare";

                if (!hasPermissions(context, PERMISSIONS))
                    ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                else
                    shareQuote(framePicture);
            }
        });

<<<<<<< HEAD
                mBinding.ivCreateQuote.setColorFilter(null);
=======
        binding.btnSaveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.water);
                dialogWatermark.dismiss();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

                strClick = "ivSave";

                if (!hasPermissions(context, PERMISSIONS))
                    ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                else
                    saveQuote(framePicture);
            }
        });
    }

<<<<<<< HEAD
=======
    private void addWatermark() {
        enableQuteFrame(false);
        enableWatermark(true);
        mBinding.tvAddWaterMark.setText(AppPreferences.getWMText(context));
        mBinding.toolbarParentLayout.ivAddWmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.tvAddWaterMark.setText("");
                enableWatermark(false);
                enableQuteFrame(true);
            }
        });

        mBinding.toolbarParentLayout.ivAddWmDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                enableQuteFrame(true);
                enableWatermark(false);
            }
        });

        mBinding.llEditWm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                editWatermark();
            }
        });

        mBinding.llWmOpacity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkOpacity();
            }
        });

        mBinding.llWmFontFamily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkFontFamily();
            }
        });

        mBinding.llWmColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                isWMColor = true;
                ColorPickerDialog.newBuilder().setColor(AppPreferences.getWMColor(context)).show(context);
            }
        });

        mBinding.llWmSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkFontSize();
            }
        });
    }

    private void editWatermark() {
        final DialogEditWatermarkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_edit_watermark,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.MY_DIALOG_Edit_Quote);
        dialog.setCancelable(true);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        binding.etWmText.setText(AppPreferences.getWMText(context));
        binding.etWmText.setGravity(AppPreferences.getWMAlignment(context));

        if (AppPreferences.getWMAlignment(context) == Gravity.CENTER)
            binding.ivWmAlignCenter.setImageResource(R.drawable.ic_font_align_center_black);
        else if (AppPreferences.getWMAlignment(context) == (Gravity.START | Gravity.CENTER_VERTICAL))
            binding.ivWmAlignLeft.setImageResource(R.drawable.ic_font_align_left_black);
        else
            binding.ivWmAlignRight.setImageResource(R.drawable.ic_font_align_right_black);

        if (!binding.etWmText.getText().toString().equalsIgnoreCase(""))
            binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text_2);
        else
            binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text);

        binding.etWmText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etWmText.getText().toString().equalsIgnoreCase(""))
                    binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text);
                else
                    binding.ivClearWmChange.setImageResource(R.drawable.ic_clear_text_2);
            }
        });

        binding.ivCancelWmChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialog.dismiss();
            }
        });

        binding.ivClearWmChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                binding.etWmText.setText("");
                AppPreferences.setWMText(context, "");
            }
        });

        binding.ivDoneWmChange.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);

                AppPreferences.setWMText(context, binding.etWmText.getText().toString());
                mBinding.tvAddWaterMark.setText(binding.etWmText.getText().toString());
                deploayChanges(true);

                dialog.dismiss();
            }
        });

        binding.ivWmAlignLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                binding.etWmText.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
                mBinding.tvAddWaterMark.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
                AppPreferences.setWMAlignment(context, Gravity.START | Gravity.CENTER_VERTICAL);
                binding.ivWmAlignLeft.setImageResource(R.drawable.ic_font_align_left_black);
                binding.ivWmAlignCenter.setImageResource(R.drawable.ic_font_align_center);
                binding.ivWmAlignRight.setImageResource(R.drawable.ic_font_align_right);
                deploayChanges(true);
            }
        });

        binding.ivWmAlignCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                binding.etWmText.setGravity(Gravity.CENTER);
                mBinding.tvAddWaterMark.setGravity(Gravity.CENTER);
                AppPreferences.setWMAlignment(context, Gravity.CENTER);
                binding.ivWmAlignCenter.setImageResource(R.drawable.ic_font_align_center_black);
                binding.ivWmAlignLeft.setImageResource(R.drawable.ic_font_align_left);
                binding.ivWmAlignRight.setImageResource(R.drawable.ic_font_align_right);
                deploayChanges(true);
            }
        });

        binding.ivWmAlignRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                binding.etWmText.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
                mBinding.tvAddWaterMark.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
                AppPreferences.setWMAlignment(context, Gravity.END | Gravity.CENTER_VERTICAL);
                binding.ivWmAlignRight.setImageResource(R.drawable.ic_font_align_right_black);
                binding.ivWmAlignCenter.setImageResource(R.drawable.ic_font_align_center);
                binding.ivWmAlignLeft.setImageResource(R.drawable.ic_font_align_left);
                deploayChanges(true);
            }
        });
    }

    private void watermarkOpacity() {
        final DialogLayoutWmOpacityBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_opacity,
                null,
                false);
        dialogWMOpacity = new Dialog(context, R.style.QuoteDialog);
        dialogWMOpacity.setCancelable(false);
        dialogWMOpacity.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogWMOpacity.setCanceledOnTouchOutside(false);
        dialogWMOpacity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogWMOpacity.getWindow().setGravity(Gravity.BOTTOM);
        dialogWMOpacity.setContentView(binding.getRoot());
        dialogWMOpacity.show();

        binding.sbWmOpacity.setProgress(AppPreferences.getWMOpacity(context));
        mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));

        binding.sbWmOpacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.tvAddWaterMark.onSetAlpha(255 - progress);
                wmOpacity = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogWMOpacity.dismiss();
                AppPreferences.setWMOpacity(context, wmOpacity);
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialogWMOpacity.dismiss();
                binding.sbWmOpacity.setProgress(AppPreferences.getWMOpacity(context));
                mBinding.tvAddWaterMark.onSetAlpha(255 - AppPreferences.getWMOpacity(context));
            }
        });
    }

    private void watermarkFontFamily() {
        final DialogLayoutWmFontFamilyBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_font_family,
                null,
                false);
        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        final Adapter_WM_Font_Family adapterWmFontFamily = new Adapter_WM_Font_Family(fontStyleArrayList,
                R.layout.row_layout_wm_font_types,
                context);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        binding.rvFontType.setLayoutManager(layoutManager);
        binding.rvFontType.setAdapter(adapterWmFontFamily);

        binding.rvFontType.addOnItemTouchListener(new RecyclerItemClickListener(context,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        playSound(R.raw.button_tap);
                        Adapter_WM_Font_Family.selectedID = position;
                        adapterWmFontFamily.notifyDataSetChanged();

                        String fontPath = fontStyleArrayList.get(position).getFilePath();
                        AssetManager am = context.getAssets();
                        Typeface typeface = Typeface.createFromAsset(am, "fonts/" + fontPath);
                        mBinding.tvAddWaterMark.setTypeface(typeface);
                        positionWMFonts = position;
                    }
                }));

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);

                String fontPath = fontStyleArrayList.get(positionWMFonts).getFilePath();

                AppPreferences.setWMFont(context, "fonts/" + fontPath);
                AppPreferences.setWMFontID(context, positionWMFonts);
                deploayChanges(true);

                dialog.dismiss();
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                adapterWmFontFamily.selectedID = AppPreferences.getWMFontID(context);

                String fontPath = fontStyleArrayList.get(AppPreferences.getWMFontID(context)).getFilePath();
                AssetManager am = context.getAssets();
                Typeface typeface = Typeface.createFromAsset(am, "fonts/" + fontPath);
                mBinding.tvAddWaterMark.setTypeface(typeface);
                adapterWmFontFamily.notifyDataSetChanged();

                dialog.dismiss();
            }
        });
    }

    private void watermarkColor() {
        isWMColor = false;
        DialogLayoutWmColorBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_color,
                null,
                false);
        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, (int) dpToPixel(173));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        binding.btnTextColorPicker.setBackgroundColor(lastWMColor);

        binding.btnTextColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                dialog.dismiss();
                isWMColor = true;
                ColorPickerDialog.newBuilder().setColor(lastWMColor).show(context);
            }
        });

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                AppPreferences.setWMColor(context, lastWMColor);
                dialog.dismiss();
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.tvAddWaterMark.setTextColor(AppPreferences.getWMColor(context));
                mBinding.tvAddWaterMark.onSetAlpha(AppPreferences.getWMOpacity(context));
                dialog.dismiss();
            }
        });
    }

    private void watermarkFontSize() {
        DialogLayoutWmFontSizeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_layout_wm_font_size,
                null,
                false);

        final Dialog dialog = new Dialog(context, R.style.QuoteDialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        dialog.show();

        watermarkSize = AppPreferences.getWMFontSize(context);
        binding.ivFontIncrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                watermarkSize++;
                mBinding.tvAddWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_PX, watermarkSize);
            }
        }));

        binding.ivFontDecrease.setOnTouchListener(new RepeatListener(100, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (watermarkSize > 1) {
                    playSound(R.raw.button_tap);
                    watermarkSize--;
                    mBinding.tvAddWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_PX, watermarkSize);
                }
            }
        }));

        binding.llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                AppPreferences.setWMFontSize(context, watermarkSize);
                dialog.dismiss();
                deploayChanges(true);
            }
        });

        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                mBinding.tvAddWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.getWMFontSize(context));
                dialog.dismiss();
            }
        });
    }

    private void enableQuteFrame(boolean enable) {
        if (enable) {
            framePicture.setOnTouchListener(this);
            scaleGestureDetector = new ScaleGestureDetector(context, new MySimpleOnScaleGestureListener());
        } else {
            framePicture.setOnTouchListener(null);
            scaleGestureDetector = null;
        }
    }

    private void enableWatermark(boolean enable) {
        if (enable) {
            scaleGestureDetector2 = new ScaleGestureDetector(context, new MyWatermarkOnScaleGestureListener());

            slideDownShowView(mBinding.toolbarParentLayout.rlWmToolbar);
            slideUpShowView(mBinding.rlAddWmOptions);

            mBinding.llAddWaterMark.setVisibility(View.VISIBLE);
            mBinding.llAddWaterMark.setFocusableInTouchMode(true);
            mBinding.llAddWaterMark.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    scaleGestureDetector2.onTouchEvent(event);
                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            dX1 = mBinding.llAddWaterMark.getX() - event.getRawX();
                            dY1 = mBinding.llAddWaterMark.getY() - event.getRawY();

                            lastWMAction = MotionEvent.ACTION_DOWN;

                            deploayChanges(true);

                            if (doubleTap)
                                editWatermark();
                            doubleTap = true;
                            handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doubleTap = false;
                                }
                            }, 250);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (lastWMAction != MotionEvent.ACTION_POINTER_DOWN) {
                                mBinding.llAddWaterMark.setY(event.getRawY() + dY1);
                                mBinding.llAddWaterMark.setX(event.getRawX() + dX1);
                                deploayChanges(true);
                            }
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            lastWMAction = MotionEvent.ACTION_POINTER_DOWN;
                            deploayChanges(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            lastWMAction = MotionEvent.ACTION_UP;
                            deploayChanges(true);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });
        } else {
            slideUpHideView(mBinding.toolbarParentLayout.rlWmToolbar);
            slideDownHideView(mBinding.rlAddWmOptions);
            mBinding.llAddWaterMark.setOnTouchListener(null);
            mBinding.llAddWaterMark.setFocusableInTouchMode(false);
            scaleGestureDetector2 = null;
        }
    }

    private void slideUpShowView(final View view) {
        final Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.pull_up_popup);
        view.startAnimation(slideUp);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideUpHideView(final View view) {
        final Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.toolbar_up);
        view.startAnimation(slideUp);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideDownShowView(final View view) {
        final Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.toolbar_down);
        view.startAnimation(slideDown);
        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideDownHideView(final View view) {
        final Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.pull_down_popup);
        view.startAnimation(slideDown);
        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void deploayChanges(boolean isDetect) {
        if (isDetect) {
            ivSave.setImageResource(R.drawable.ic_file_download);
            Globals.isChangesOccurs = true;
        } else {
            ivSave.setImageResource(R.drawable.ic_downloaded_file);
            Globals.isChangesOccurs = false;
        }
    }

    //Get exact quote & image width...
    private void setSectionMax() {
        ViewTreeObserver observer = mBinding.ivCreateQuote.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mBinding.ivCreateQuote.getViewTreeObserver().removeOnPreDrawListener(this);
                Globals.sectionWidth = frameQuote.getMeasuredWidth();
                Globals.deviceWidth = mBinding.ivCreateQuote.getMeasuredWidth();
                Globals.sectionHeight = frameQuote.getMeasuredHeight();
                return true;
            }
        });
    }

    //Get all available fonts from assets...
    private ArrayList<Model_Font_Style> getFontStyles() {
        try {
            String[] f = getAssets().list("fonts");
            for (String f1 : f) {
                String fileName = f1.replace("_", " ").substring(0, f1.lastIndexOf("."));
                Model_Font_Style modelFontStyle = new Model_Font_Style(fileName, f1, 1);
                fontStyleArrayList.add(modelFontStyle);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fontStyleArrayList;
    }

    //Pick image from Gallery...
    private void pickFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Choose from Gallery"), REQUEST_SELECT_PICTURE);
    }

    //Refresh Gallery after download image...
    private void refreshGallery(Context mContext, File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        mContext.sendBroadcast(mediaScanIntent);
    }

    //Get image from gallery and process it to crop and re process image sizes...
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AD) {
            if (mInterstitialAd2 != null && mInterstitialAd2.isLoaded())
                mInterstitialAd2.show();
            else if (interstitialAd2 != null && interstitialAd2.isAdLoaded())
                interstitialAd2.show();
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                //Get image path of Gallery...

                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    Log.e("GalleryImagePath>>>", selectedUri.getPath());

                    //Send path to UCrop library...
                    croppingImage(selectedUri);
                } else {
                    Toast.makeText(this, "Can't get image!", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                //Get path of cropped image...
                final Uri resultUri = UCrop.getOutput(data);

                mBinding.ivCreateQuote.setColorFilter(null);

                isBackColor = false;

                //Send path for image setup and resizing...
                setFullSizeSection(resultUri);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR)
            Log.e("UCorpEroor>>>", UCrop.getError(data).getMessage());
    }

>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    //Be careful bellow two override methods doing much work after cropping image!!!

    //This methods calls when user pick image from Gallery and resize all layouts to full size...
    private void setFullSizeSection(final Uri resultUri) {
        //LayoutParams for image, tint and section...
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        mBinding.ivCreateQuote.setLayoutParams(layoutParams);
        mBinding.quoteTintView.setLayoutParams(layoutParams);
        frameQuote.setLayoutParams(layoutParams);

        //LayoutParams for image container must be match_parent...
        FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams1.gravity = Gravity.CENTER;
        framePicture.setLayoutParams(layoutParams1);
        mBinding.llLoading.setVisibility(View.VISIBLE);
        framePicture.post(new Runnable() {
            @Override
            public void run() {
                setCropedImage(resultUri);
            }
        });
    }

    //This methods calls when user click on sample image and resize all layouts to full size...
    private void setFullSizeSection(final int resultUri) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        mBinding.ivCreateQuote.setLayoutParams(layoutParams);
        mBinding.quoteTintView.setLayoutParams(layoutParams);
        frameQuote.setLayoutParams(layoutParams);

        FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams1.gravity = Gravity.CENTER;
        framePicture.setLayoutParams(layoutParams1);
        mBinding.llLoading.setVisibility(View.VISIBLE);
        framePicture.post(new Runnable() {
            @Override
            public void run() {
                setCropedImage(resultUri);
            }
        });
    }

    //Resizing image, section, container exact image size...
    private void setCropedImage(Uri resultUri) {
        deploayChanges(true);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .load(resultUri)
                .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(requestOptions.skipMemoryCache(true))
                .apply(requestOptions.transform(new BitmapTransformation() {
                    @Override
                    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
                        /*
                        * Transformation is necessary for zooming image as per ratio..
                        * There is two conditions defined for resizing height & width
                        * 1) H > W
                        * 2) W > H / W = H
                        */
                        if (outHeight > outWidth) {
                            double aspectRatio = (double) outWidth / (double) outHeight;
                            int targetHeight = framePicture.getMeasuredHeight();
                            int targetWidth = (int) (targetHeight * aspectRatio);

                            Bitmap result = Bitmap.createScaledBitmap(toTransform, targetWidth, targetHeight, false);

                            if (result != result) {
                                // Same bitmap is returned if sizes are the same
                                result.recycle();
                            }

                            //Return resized bitmap to Glide transformation...
                            return result;
                        } else {
                            int targetWidth = framePicture.getMeasuredWidth();

                            double aspectRatio = (double) outHeight / (double) outWidth;
                            int targetHeight = (int) (targetWidth * aspectRatio);

                            Bitmap result = Bitmap.createScaledBitmap(toTransform, targetWidth, targetHeight, false);

                            if (result != result) {
                                // Same bitmap is returned if sizes are the same
                                result.recycle();
                            }
                            return result;
                        }

                    }

                    @Override
                    public void updateDiskCacheKey(MessageDigest messageDigest) {

                    }
                }))
                .into(new SimpleTarget<Drawable>() {

                    /*
                    * After transformation Glide load the image into imageview
                    * After loaded image is ready as per size transformed
                    * Then we need to resize the all layout related to ImageView
                    */
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        //Color fillter of image must be null because image is selected...
                        mBinding.ivCreateQuote.setColorFilter(null);
                        isBackColor = false;
                        mBinding.touchableFrameCreateQuote.setBackgroundColor(Color.WHITE);

                        //For getting better height & width convert drawable to bitmapdrawable...
                        BitmapDrawable bitmap = (BitmapDrawable) resource;

                        if (bitmap.getBitmap().getHeight() > bitmap.getBitmap().getWidth()) {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);

                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);

                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());

                            //It must be called after whenever you loaded new image...
                            setSectionMax();

                            mBinding.llLoading.setVisibility(View.GONE);
                        } else if (bitmap.getBitmap().getWidth() > bitmap.getBitmap().getHeight()) {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);

                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);

                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());
                            setSectionMax();
                            mBinding.llLoading.setVisibility(View.GONE);
                        } else {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);

                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);
                            setSectionMax();
                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());
                            mBinding.llLoading.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mBinding.llLoading.setVisibility(View.GONE);
                    }
                });
    }

    //Resizing image, section, container exact image size...
    private void setCropedImage(int resultUri) {
        deploayChanges(true);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .load(resultUri)
                .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(requestOptions.skipMemoryCache(true))
                .apply(requestOptions.transform(new BitmapTransformation() {
                    @Override
                    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {

                        if (outHeight > outWidth) {
                            double aspectRatio = (double) outWidth / (double) outHeight;
                            int targetHeight = framePicture.getMeasuredHeight();
                            int targetWidth = (int) (targetHeight * aspectRatio);

                            Log.e("ImageH>W>>>", "true");

                            Bitmap result = Bitmap.createScaledBitmap(toTransform, targetWidth, targetHeight, false);

                            if (result != result) {
                                // Same bitmap is returned if sizes are the same
                                result.recycle();
                            }

                            return result;
                        } else {
                            int targetWidth = framePicture.getMeasuredWidth();

                            Log.e("ImageW>H>>>", "true");

                            double aspectRatio = (double) outHeight / (double) outWidth;
                            int targetHeight = (int) (targetWidth * aspectRatio);
                            Bitmap result = Bitmap.createScaledBitmap(toTransform, targetWidth, targetHeight, false);

                            if (result != result) {
                                // Same bitmap is returned if sizes are the same
                                result.recycle();
                            }
                            return result;
                        }

                    }

                    @Override
                    public void updateDiskCacheKey(MessageDigest messageDigest) {

                    }
                }))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        mBinding.ivCreateQuote.setColorFilter(null);
                        isBackColor = false;
                        mBinding.touchableFrameCreateQuote.setBackgroundColor(Color.WHITE);

                        BitmapDrawable bitmap = (BitmapDrawable) resource;

                        if (bitmap.getBitmap().getHeight() > bitmap.getBitmap().getWidth()) {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);

                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);

                            setSectionMax();

                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());
                            mBinding.ivCreateQuote.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mBinding.llLoading.setVisibility(View.GONE);
                                }
                            }, 500);
                        } else if (bitmap.getBitmap().getWidth() > bitmap.getBitmap().getHeight()) {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);
                            setSectionMax();
                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());
                            mBinding.ivCreateQuote.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mBinding.llLoading.setVisibility(View.GONE);
                                }
                            }, 500);
                        } else {
                            float width = bitmap.getBitmap().getWidth();
                            float height = bitmap.getBitmap().getHeight();

                            Log.e("ImageWidth>>>", width + "");
                            Log.e("ImageHeight>>>", height + "");

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                            mBinding.ivCreateQuote.setLayoutParams(layoutParams);
                            mBinding.quoteTintView.setLayoutParams(layoutParams);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            layoutParams2.setMargins(60, 0, 60, 0);
                            frameQuote.setLayoutParams(layoutParams2);

                            FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams((int) width,
                                    (int) height);
                            layoutParams1.gravity = Gravity.CENTER;
                            framePicture.setLayoutParams(layoutParams1);
                            setSectionMax();
                            mBinding.ivCreateQuote.setImageBitmap(bitmap.getBitmap());
                            mBinding.ivCreateQuote.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mBinding.llLoading.setVisibility(View.GONE);
                                }
                            }, 500);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mBinding.llLoading.setVisibility(View.GONE);
                    }
                });
    }

    //Cropping image using Ucrop library...
    private void croppingImage(Uri selectedUri) {
        String SAMPLE_CROPPED_IMAGE_NAME = "Sample" + new Date(System.currentTimeMillis());
        UCrop uCrop = UCrop.of(selectedUri, Uri.fromFile(new File(getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME)));

        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(100);
        options.setFreeStyleCropEnabled(true);
        options.setToolbarColor(Color.WHITE);
        options.setActiveWidgetColor(Color.parseColor("#000000"));
        options.setToolbarWidgetColor(Color.parseColor("#AAAAAA"));
        options.setCropGridColumnCount(1);
        options.setCropGridRowCount(1);
        uCrop.withOptions(options);
        uCrop.start(context);
    }

    //InterstitialAd settings...
    private void showAdsSettings() {
        mFirebaseRemoteConfig.fetch(3600).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                } else {
                    Log.d("TAG", "task unsuccess");
                }
            }
        });
    }

    private void requestNewInterstitial() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    String isShow = mFirebaseRemoteConfig.getString("status");
                    if (isShow.equals("show")) {
<<<<<<< HEAD
                        if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(context) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            AppPreferences.setLastSavedDateForAds(context, System.currentTimeMillis());
=======
                        if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(context) > Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                            if (mInterstitialAd.isLoaded())
                                mInterstitialAd.show();

                            QuotesUtils.setLastSavedDateForAds(context, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        }
                    }
                }
            });
            mInterstitialAd.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd = new com.facebook.ads.InterstitialAd(context,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd.loadAd();
                    interstitialAd.setAdListener(new AbstractAdListener() {
                        @Override
                        public void onAdLoaded(Ad ad) {
                            super.onAdLoaded(ad);
                            String isShow = mFirebaseRemoteConfig.getString("status");
                            if (isShow.equals("show")) {
<<<<<<< HEAD
                                if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(context) >
=======
                                if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(context) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                        Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                    if (interstitialAd.isAdLoaded())
                                        interstitialAd.show();

<<<<<<< HEAD
                                    AppPreferences.setLastSavedDateForAds(context, System.currentTimeMillis());
=======
                                    QuotesUtils.setLastSavedDateForAds(context, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;

                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        String isShow = mFirebaseRemoteConfig.getString("status");
                        if (isShow.equals("show")) {
<<<<<<< HEAD
                            if (System.currentTimeMillis() - AppPreferences.getLastSavedDateForAds(context) >
=======
                            if (System.currentTimeMillis() - QuotesUtils.getLastSavedDateForAds(context) >
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                    Long.parseLong(mFirebaseRemoteConfig.getString("duration"))) {
                                if (mInterstitialAd.isLoaded())
                                    mInterstitialAd.show();

<<<<<<< HEAD
                                AppPreferences.setLastSavedDateForAds(context, System.currentTimeMillis());
=======
                                QuotesUtils.setLastSavedDateForAds(context, System.currentTimeMillis());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                            }
                        }
                    }
                });
                mInterstitialAd.loadAd(adRequest2);
            }
        }
    }

    private void requestNewInterstitial2() {
        if (mFirebaseRemoteConfig.getString("ad_network_pqac").equalsIgnoreCase("google")) {
            if (!isAdInitialized)
                MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
            isAdInitialized = true;

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
            AdRequest adRequest2 = new AdRequest.Builder()
                    .build();
            mInterstitialAd2.loadAd(adRequest2);
        } else {
            if (getInstalled("com.facebook.katana")) {
                try {
                    interstitialAd2 = new com.facebook.ads.InterstitialAd(context,
                            getResources().getString(R.string.fb_interstitial_id));
                    interstitialAd2.loadAd();
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log("FBAds>>>" + e.getMessage());
                }
            } else {
                if (!isAdInitialized)
                    MobileAds.initialize(getApplicationContext(), getString(R.string.ad_app_id));
                isAdInitialized = true;
<<<<<<< HEAD

                Log.e("AdsType>>>", "GoogleAds");

                mInterstitialAd2 = new InterstitialAd(this);
                mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd2.loadAd(adRequest2);
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private int getStatusBarHeight(Activity context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    //Convert DP to pixels...
=======

                Log.e("AdsType>>>", "GoogleAds");

                mInterstitialAd2 = new InterstitialAd(this);
                mInterstitialAd2.setAdUnitId(getString(R.string.interstitial_ad_id));
                AdRequest adRequest2 = new AdRequest.Builder()
                        .build();
                mInterstitialAd2.loadAd(adRequest2);
            }
        }
    }

    private boolean getInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    private float dpToPixel(float pixels) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, displayMetrics);
    }

    //Sample images adapter...
    private class Adapter_Background_Quotes extends RecyclerView.Adapter<Adapter_Background_Quotes.BackgroundQuotesViewHolder> {
        ArrayList<Integer> arrayList;
        int rowLayout;
        Activity context;

        Adapter_Background_Quotes(ArrayList<Integer> arrayList, int rowLayout, Activity context) {
            this.arrayList = arrayList;
            this.rowLayout = rowLayout;
            this.context = context;
        }

        @NonNull
        @Override
        public BackgroundQuotesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
            return new BackgroundQuotesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final BackgroundQuotesViewHolder holder, int position) {
            RequestOptions requestOptions = new RequestOptions();

            Glide.with(context)
                    .load(arrayList.get(position))
                    .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                    .apply(requestOptions.skipMemoryCache(true))
                    .apply(requestOptions.transform(new RoundedCorners(7)))
                    .into(holder.ivBack);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playSound(R.raw.button_tap);
                    if (holder.getAdapterPosition() > 0) {
                        //Sample images for quote...
                        mBinding.ivCreateQuote.setColorFilter(null);
                        selectedSampleImage = arrayList.get(holder.getAdapterPosition());
                        isBackColor = false;

                        setFullSizeSection(arrayList.get(holder.getAdapterPosition()));
                    } else {
                        //This is first sample item for selected color for quote background rather than image...
                        isBackColor = true;
                        ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
                        colorPickerDialog.newBuilder().setColor(Globals.lastBackColor).show(context);
                        colorPickerDialog.setColorPickerDialogListener(CreateQuoteActivity.this);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        class BackgroundQuotesViewHolder extends RecyclerView.ViewHolder {
            ImageView ivBack;
            CardView cvBack;

            BackgroundQuotesViewHolder(View itemView) {
                super(itemView);

                cvBack = itemView.findViewById(R.id.cv_sample_background);
                ivBack = itemView.findViewById(R.id.iv_sample_background);
            }
        }
    }
}
