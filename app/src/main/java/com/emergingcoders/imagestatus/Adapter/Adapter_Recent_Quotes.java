package com.emergingcoders.imagestatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emergingcoders.imagestatus.Design.RecentQuotesActivity;
import com.emergingcoders.imagestatus.Model.Model_Recent_Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.DbHelper;

import java.util.ArrayList;

public class Adapter_Recent_Quotes extends RecyclerView.Adapter<Adapter_Recent_Quotes.RecentQuotesViewHolder> {

    private ArrayList<Model_Recent_Quotes> arrayList;
    private int rowLayout;
    private Activity context;

    private String BROADCAST_FILTER = "com.emergingcoders.imagestatus.Design.SampleQuotesActivity";

    public Adapter_Recent_Quotes(ArrayList<Model_Recent_Quotes> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @NonNull
    @Override
    public RecentQuotesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
        return new RecentQuotesViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecentQuotesViewHolder holder, final int position) {
        holder.tvQuote.setText(arrayList.get(position).getQuote().trim());
        if (!arrayList.get(position).getQuoteBy().equalsIgnoreCase(""))
            holder.tvQuoteBy.setText("- " + arrayList.get(position).getQuoteBy().trim());
        else
            holder.tvQuoteBy.setText("" + arrayList.get(position).getQuoteBy().trim());

        holder.cvQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(BROADCAST_FILTER);
                intent.putExtra("SampleQuote", arrayList.get(holder.getAdapterPosition()).getQuote());
                intent.putExtra("SampleQuoteBy", arrayList.get(holder.getAdapterPosition()).getQuoteBy());
                intent.putExtra("isFrom", "Recent");
                context.sendBroadcast(intent);
                context.onBackPressed();
            }
        });

        holder.cvQuote.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog dialog = new Dialog(context, R.style.MyAlertDialog);
                dialog.setContentView(R.layout.layout_delete_recent_dialog);
                dialog.setCancelable(false);
                dialog.show();

                Button btnNo = dialog.findViewById(R.id.btn_delete_recent_no);
                Button btnYes = dialog.findViewById(R.id.btn_delete_recent_yes);

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        DbHelper dbHelper = new DbHelper(context);
                        dbHelper.deleteRecentQuote(arrayList.get(holder.getAdapterPosition()).getId());

                        ((RecentQuotesActivity) context).refreshRecentQuotes(holder.getAdapterPosition());
                    }
                });

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class RecentQuotesViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuote, tvQuoteBy;
        CardView cvQuote;

        RecentQuotesViewHolder(View itemView) {
            super(itemView);

            tvQuote = itemView.findViewById(R.id.tv_recent_quote_text);
            tvQuoteBy = itemView.findViewById(R.id.tv_recent_quote_by);
            cvQuote = itemView.findViewById(R.id.cv_recent_quote);
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
