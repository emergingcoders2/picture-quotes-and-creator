package com.emergingcoders.imagestatus.Adapter;

import android.app.Activity;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.emergingcoders.imagestatus.Model.Model_Font_Style;
import com.emergingcoders.imagestatus.databinding.RowLayoutWmFontTypesBinding;

import java.util.ArrayList;

public class Adapter_WM_Font_Family extends RecyclerView.Adapter<Adapter_WM_Font_Family.FontFamilyViewHolder> {
<<<<<<< HEAD
    private ArrayList<Model_Font_Style> arrayList;
    private int rowLayout;
    private Activity context;

=======
    ArrayList<Model_Font_Style> arrayList;
    int rowLayout;
    Activity context;

    Typeface typeface;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
    public static int selectedID = 13;

    public Adapter_WM_Font_Family(ArrayList<Model_Font_Style> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @NonNull
    @Override
    public FontFamilyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowLayoutWmFontTypesBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                rowLayout,
                parent,
                false);
        return new FontFamilyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull FontFamilyViewHolder holder, int position) {
        AssetManager am = context.getApplicationContext().getAssets();
<<<<<<< HEAD
        Typeface typeface = Typeface.createFromAsset(am, "fonts/" + arrayList.get(position).getFilePath());
=======
        typeface = Typeface.createFromAsset(am, "fonts/" + arrayList.get(position).getFilePath());
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

        holder.mBinding.tvFontTypeName.setText(arrayList.get(position).getFileName());
        holder.mBinding.tvFontTypeName.setTypeface(typeface);

        //If selectedIds ArrayList contains id then change background color of selected item...
        holder.mBinding.llFontType.setBackgroundColor(selectedID == position ? Color.parseColor("#AAAAAA") : Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class FontFamilyViewHolder extends RecyclerView.ViewHolder {
        RowLayoutWmFontTypesBinding mBinding;

<<<<<<< HEAD
        FontFamilyViewHolder(RowLayoutWmFontTypesBinding mBinding) {
=======
        public FontFamilyViewHolder(RowLayoutWmFontTypesBinding mBinding) {
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }
}
