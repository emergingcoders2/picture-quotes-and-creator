package com.emergingcoders.imagestatus.Adapter;

/*
 * Created by @nickbapu on 14/4/18.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.emergingcoders.imagestatus.Fragment.Fragment_WM_Font_Adjustment;
import com.emergingcoders.imagestatus.Fragment.Fragment_WM_Font_Family;
import com.emergingcoders.imagestatus.Fragment.Fragment_WM_Font_Shadows;
import com.emergingcoders.imagestatus.Fragment.Fragment_WM_Rotation;

public class AdapterWMFontTabs extends FragmentStatePagerAdapter {
    private int tabCount;

    public AdapterWMFontTabs(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Fragment_WM_Font_Adjustment();
            case 1:
                return new Fragment_WM_Font_Family();
            case 2:
                return new Fragment_WM_Font_Shadows();
            case 3:
                return new Fragment_WM_Rotation();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
