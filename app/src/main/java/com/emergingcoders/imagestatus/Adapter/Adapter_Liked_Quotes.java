package com.emergingcoders.imagestatus.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.emergingcoders.imagestatus.Design.ImageShowActivity;
import com.emergingcoders.imagestatus.Model.ModelLikedQuotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Adapter_Liked_Quotes extends RecyclerView.Adapter<Adapter_Liked_Quotes.LikedQuotesViewHolder> {

    private ArrayList<ModelLikedQuotes> arrayList;
    private int rowLayout;
    private Activity context;

    private int DEVICE_WIDTH = 0;
    private static final List<Integer> COLORS = Arrays.asList(0xff2E7D32, 0xff1565C0, 0xff4E342E, 0xff9E9D24, 0xff558B2F, 0xff37474F, 0xff0277BD, 0xffBF360C, 0xff4527A0, 0xff00838F, 0xff6A1B9A, 0xffAD1457, 0xff283593, 0xffC62828, 0xff00695C, 0xff000000, 0xffffffff);

    public Adapter_Liked_Quotes(ArrayList<ModelLikedQuotes> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        DEVICE_WIDTH = (displayMetrics.widthPixels / 2) - 45;
    }

    @NonNull
    @Override
    public LikedQuotesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
        return new LikedQuotesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LikedQuotesViewHolder holder, int position) {
        final int IMAGE_HEIGHT = (DEVICE_WIDTH *
                Integer.parseInt(arrayList.get(position).getHeight())) /
                Integer.parseInt(arrayList.get(position).getWidth());

        CardView.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                IMAGE_HEIGHT);
        layoutParams.setMargins(15, 0, 15, 30);
        holder.cvQuotesList.setLayoutParams(layoutParams);
        holder.cvQuotesList.setContentPadding(2, 2, 2, 2);

        switch (position % 16) {
            case 1:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(15));
                break;
            case 2:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(14));
                break;
            case 3:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(13));
                break;
            case 4:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(12));
                break;
            case 5:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(11));
                break;
            case 6:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(10));
                break;
            case 7:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(9));
                break;
            case 8:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(8));
                break;
            case 9:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(7));
                break;
            case 10:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(6));
                break;
            case 11:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(5));
                break;
            case 12:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(4));
                break;
            case 13:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(3));
                break;
            case 14:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(2));
                break;
            case 15:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(1));
                break;
            case 0:
                holder.llQuotesList.setCardBackgroundColor(COLORS.get(0));
                break;
        }

        RequestOptions requestOptions = new RequestOptions();

        Glide.with(context)
                .load(arrayList.get(position).getThumbnail_url())
                .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(requestOptions.skipMemoryCache(true))
                .apply(requestOptions.transform(new RoundedCorners(10)))
                .into(holder.ivQuotesList);

        holder.cvQuotesList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(context, ImageShowActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("LikeFlag", true);
                intent.putExtra("QuotesList", new Gson().toJson(arrayList.get(holder.getAdapterPosition())));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class LikedQuotesViewHolder extends RecyclerView.ViewHolder {
        CardView cvQuotesList, llQuotesList;
        ImageView ivQuotesList;

        LikedQuotesViewHolder(View itemView) {
            super(itemView);

            cvQuotesList = itemView.findViewById(R.id.cv_liked_quotes_list);
            ivQuotesList = itemView.findViewById(R.id.iv_liked_quotes_list);
            llQuotesList = itemView.findViewById(R.id.rl_liked_quotes_list);
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
