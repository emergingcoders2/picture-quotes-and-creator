package com.emergingcoders.imagestatus.Adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.crashlytics.android.Crashlytics;
import com.emergingcoders.imagestatus.Design.ImageShowActivity;
<<<<<<< HEAD
import com.emergingcoders.imagestatus.Globals.Globals;
=======
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import com.emergingcoders.imagestatus.Model.ModelQuotesList;
import com.emergingcoders.imagestatus.Model.ModelSingleLike;
import com.emergingcoders.imagestatus.Model.Model_Liked_Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Retrofit.RetrofitClient;
import com.emergingcoders.imagestatus.Retrofit.RetrofitInterfaces;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.GlideApp;
import com.emergingcoders.imagestatus.Utils.InternetConnection;
import com.emergingcoders.imagestatus.databinding.HeaderLayoutImageShowBinding;
import com.google.gson.Gson;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Adapter_Related_Quotes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ModelQuotesList> arrayList;
    private int rowLayout;
    private Activity context;
    private ModelQuotesList modelQuotesList;

    private int DEVICE_WIDTH = 0;
    private static final List<Integer> COLORS = Arrays.asList(0xff2E7D32, 0xff1565C0, 0xff4E342E, 0xff9E9D24, 0xff558B2F, 0xff37474F, 0xff0277BD, 0xffBF360C, 0xff4527A0, 0xff00838F, 0xff6A1B9A, 0xffAD1457, 0xff283593, 0xffC62828, 0xff00695C, 0xff000000, 0xffffffff);

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private ImageView ivSave, ivShare;
    public boolean flagUnLike = false;
    private boolean flagLike = false;
    private boolean flagDownloaded = false;
    private String DEVICE_ID;

    public String btnClick = "";
    private int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    private final static int REQUEST_AD = 100;

    private Call<Model_Liked_Quotes> likedQuotesCall;

    @SuppressLint("HardwareIds")
    public Adapter_Related_Quotes(ArrayList<ModelQuotesList> arrayList, int rowLayout, Activity context, ModelQuotesList modelQuotesList) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
        this.modelQuotesList = modelQuotesList;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        DEVICE_WIDTH = (displayMetrics.widthPixels / 2) - 45;

        DEVICE_ID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (context.getIntent().getBooleanExtra("LikeFlag", false))
            flagLike = true;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            HeaderLayoutImageShowBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                    R.layout.header_layout_image_show,
                    parent,
                    false);
            return new VHHeader(mBinding);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            final VHHeader vhHeader = (VHHeader) holder;
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            //Getting Display Height & Width for Image..
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int MW = displayMetrics.widthPixels - 80;
            int SW = Integer.parseInt(modelQuotesList.getWidth());
            int SH = Integer.parseInt(modelQuotesList.getHeight());
            int MH = (MW * SH) / SW;

            //Set Exact background card height for image...
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(MW,
                    MH);
            layoutParams2.gravity = Gravity.CENTER;
            vhHeader.mBinding.cvImage.setLayoutParams(layoutParams2);
            vhHeader.mBinding.cvImage.setContentPadding(2, 2, 2, 2);
            vhHeader.mBinding.cvImage.post(new Runnable() {
                @Override
                public void run() {
                    vhHeader.mBinding.progressImageShow.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFFFFF"),
                            android.graphics.PorterDuff.Mode.MULTIPLY);
                    vhHeader.mBinding.progressImageShow.setVisibility(View.VISIBLE);
                }
            });

            //Set image radius, margin and proper...
            GlideApp.with(context)
                    .load(modelQuotesList.getImage_url())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .transform(new RoundedCorners(10))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            vhHeader.mBinding.progressImageShow.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(vhHeader.mBinding.ivImageShow);

            vhHeader.mBinding.ivImageShow.setScaleType(ImageView.ScaleType.FIT_XY);

            //Check image is already downloaded...
            String strExist = getDownloadedPath() + modelQuotesList.getImage_name();
            File myFile = new File(strExist);
            if (myFile.exists()) {
                ivSave.setImageResource(R.drawable.ic_downloaded_file);
                flagDownloaded = true;
            }

            if (flagLike) {
                if (Build.VERSION.SDK_INT >= 21)
                    vhHeader.mBinding.btnDpLike.setLiked(true);
                else
                    vhHeader.mBinding.ivDpLike.setImageResource(R.drawable.ic_favorite_red);
            }

            if (Build.VERSION.SDK_INT >= 21) {
                vhHeader.mBinding.btnDpLike.setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        playSound(R.raw.water);
                        setQuoteLike(modelQuotesList.getId(),
                                DEVICE_ID,
                                1);
                        flagLike = true;
                        flagUnLike = false;
                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        setQuoteLike(modelQuotesList.getId(),
                                DEVICE_ID,
                                0);
                        flagLike = false;
                        flagUnLike = true;
                    }
                });
            } else {
                vhHeader.mBinding.ivDpLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (flagLike) {
                            setQuoteLike(modelQuotesList.getId(),
                                    DEVICE_ID,
                                    0);
                            vhHeader.mBinding.ivDpLike.setImageResource(R.drawable.ic_favorite_border);
                            flagLike = false;
                            flagUnLike = true;
                        } else {
                            playSound(R.raw.water);
                            setQuoteLike(modelQuotesList.getId(),
                                    DEVICE_ID,
                                    1);
                            vhHeader.mBinding.ivDpLike.setImageResource(R.drawable.ic_favorite_red);
                            flagLike = true;
                            flagUnLike = false;
                        }
                    }
                });
            }

            ivShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
<<<<<<< HEAD
                    if (!btnClick.equalsIgnoreCase("ivShare") && !btnClick.equalsIgnoreCase("ivSave")) {
=======
                    playSound(R.raw.water);

                    btnClick = "ivShare";

                    if (flagDownloaded) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                        intent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.share_text) + "\n" +
                                "\nClick here to install:\n" +
                                context.getResources().getString(R.string.app_playstore_path) +
                                "\n");
                        intent.setType("image/*");

                        Uri uri = Uri.parse(getDownloadedPath() +
                                modelQuotesList.getImage_name());

                        intent.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(intent);
                    } else {
                        if (!hasPermissions(context, PERMISSIONS))
                            ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                        else {
                            vhHeader.mBinding.ivDpShare.setVisibility(View.GONE);
                            vhHeader.mBinding.progressShareDp.setVisibility(View.VISIBLE);
                            final DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                            try {
                                Uri uri = Uri.parse(modelQuotesList.getImage_url());

                                DownloadManager.Request request = new DownloadManager.Request(uri);

                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                request.setVisibleInDownloadsUi(true);

                                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                                request.setAllowedOverRoaming(false);
                                request.setTitle("Downloading in " +
                                        "Downloaded - " +
                                        context.getResources().getString(R.string.app_name));
                                request.setDescription(modelQuotesList.getImage_name());
                                request.setDestinationInExternalPublicDir(getPathForDownloadFile(),
                                        modelQuotesList.getImage_name());
                                downloadManager.enqueue(request);

                                BroadcastReceiver receiver = new BroadcastReceiver() {
                                    @Override
                                    public void onReceive(Context context2, Intent intent) {
                                        try {
                                            String action = intent.getAction();
                                            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                                if (btnClick.equalsIgnoreCase("ivShare")) {
                                                    vhHeader.mBinding.ivDpShare.setVisibility(View.VISIBLE);
                                                    vhHeader.mBinding.progressShareDp.setVisibility(View.GONE);
                                                    vhHeader.mBinding.ivDpSave.setImageResource(R.drawable.ic_downloaded_file);

                                                    flagDownloaded = true;
                                                    btnClick = "";

                                                    Intent intent2 = new Intent(Intent.ACTION_SEND);
                                                    intent2.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                                                    intent2.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.share_text) + "\n" +
                                                            "\nClick here to install:\n" +
                                                            context.getResources().getString(R.string.app_playstore_path) +
                                                            "\n");
                                                    intent2.setType("image/*");

                                                    Uri fileuri = Uri.parse(getDownloadedPath() +
                                                            modelQuotesList.getImage_name());

                                                    intent2.putExtra(Intent.EXTRA_STREAM, fileuri);
                                                    ((ImageShowActivity) context).requestNewInterstitial2();
                                                    context.startActivityForResult(Intent.createChooser(intent2,
                                                            context.getResources().getString(R.string.app_name)),
                                                            REQUEST_AD);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                context.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                            } catch (Exception e) {
                                Log.e("DownloadError>>>", e.getMessage());
                                Crashlytics.log("SaveQuote" + e.getMessage());
                            }
                        }
                    }
                }
            });

            ivSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!flagDownloaded) {
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                        playSound(R.raw.water);

                        btnClick = "ivShare";

                        if (flagDownloaded) {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                            intent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.share_text) + "\n" +
                                    "\nClick here to install:\n" +
                                    context.getResources().getString(R.string.app_playstore_path) +
                                    "\n");
                            intent.setType("image/*");

<<<<<<< HEAD
                            Uri uri = Uri.parse(getDownloadedPath() +
                                    modelQuotesList.getImage_name());

                            intent.putExtra(Intent.EXTRA_STREAM, uri);
                            context.startActivityForResult(Intent.createChooser(intent,
                                    context.getResources().getString(R.string.app_name)),
                                    REQUEST_AD);
                        } else {
                            if (!hasPermissions(context, PERMISSIONS))
                                ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                            else {
                                vhHeader.mBinding.ivDpShare.setVisibility(View.GONE);
                                vhHeader.mBinding.progressShareDp.setVisibility(View.VISIBLE);
                                final DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                try {
=======
                        if (!hasPermissions(context, PERMISSIONS))
                            ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                        else {
                            vhHeader.mBinding.ivDpSave.setVisibility(View.GONE);
                            vhHeader.mBinding.progressDownloadDp.setVisibility(View.VISIBLE);
                            final DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                            File dir = new File(getDownloadedPath());
                            try {
                                String strExist = dir.getPath() + File.separator + modelQuotesList.getImage_name();
                                File myFile = new File(strExist);

                                if (!myFile.exists()) {
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                                    Uri uri = Uri.parse(modelQuotesList.getImage_url());

                                    DownloadManager.Request request = new DownloadManager.Request(uri);

                                    request.allowScanningByMediaScanner();
                                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                    request.setVisibleInDownloadsUi(true);

                                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                                    request.setAllowedOverRoaming(false);
                                    request.setTitle("Downloading in " +
                                            "Downloaded - " +
                                            context.getResources().getString(R.string.app_name));
                                    request.setDescription(modelQuotesList.getImage_name());
                                    request.setDestinationInExternalPublicDir(getPathForDownloadFile(),
                                            modelQuotesList.getImage_name());
                                    downloadManager.enqueue(request);

                                    BroadcastReceiver receiver = new BroadcastReceiver() {
                                        @Override
                                        public void onReceive(Context context2, Intent intent) {
                                            try {
                                                String action = intent.getAction();
                                                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                                    if (btnClick.equalsIgnoreCase("ivShare")) {
                                                        vhHeader.mBinding.ivDpShare.setVisibility(View.VISIBLE);
                                                        vhHeader.mBinding.progressShareDp.setVisibility(View.GONE);
                                                        vhHeader.mBinding.ivDpSave.setImageResource(R.drawable.ic_downloaded_file);

                                                        flagDownloaded = true;
                                                        btnClick = "";

                                                        Intent intent2 = new Intent(Intent.ACTION_SEND);
                                                        intent2.putExtra(Intent.EXTRA_SUBJECT, "Sharing by " + context.getResources().getString(R.string.app_name));
                                                        intent2.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.share_text) + "\n" +
                                                                "\nClick here to install:\n" +
                                                                context.getResources().getString(R.string.app_playstore_path) +
                                                                "\n");
                                                        intent2.setType("image/*");

                                                        Uri fileuri = Uri.parse(getDownloadedPath() +
                                                                modelQuotesList.getImage_name());

                                                        intent2.putExtra(Intent.EXTRA_STREAM, fileuri);
                                                        if (!Globals.flagRemoveAds)
                                                            ((ImageShowActivity) context).requestNewInterstitial2();
                                                        context.startActivityForResult(Intent.createChooser(intent2,
                                                                context.getResources().getString(R.string.app_name)),
                                                                REQUEST_AD);
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                btnClick = "";
                                            }
                                        }
                                    };
                                    context.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                                } catch (Exception e) {
                                    btnClick = "";
                                    Log.e("DownloadError>>>", e.getMessage());
                                    Crashlytics.log("SaveQuote" + e.getMessage());
                                }
<<<<<<< HEAD
                            }
                        }
                    }
                }
            });

            ivSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!btnClick.equalsIgnoreCase("ivShare") && !btnClick.equalsIgnoreCase("ivSave")) {
                        if (!flagDownloaded) {
                            playSound(R.raw.water);

                            btnClick = "ivSave";

                            if (!hasPermissions(context, PERMISSIONS))
                                ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
                            else {
                                vhHeader.mBinding.ivDpSave.setVisibility(View.GONE);
                                vhHeader.mBinding.progressDownloadDp.setVisibility(View.VISIBLE);
                                final DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                File dir = new File(getDownloadedPath());
                                try {
                                    String strExist = dir.getPath() + File.separator + modelQuotesList.getImage_name();
                                    File myFile = new File(strExist);

                                    if (!myFile.exists()) {
                                        Uri uri = Uri.parse(modelQuotesList.getImage_url());

                                        DownloadManager.Request request = new DownloadManager.Request(uri);

                                        request.allowScanningByMediaScanner();
                                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                        request.setVisibleInDownloadsUi(true);

                                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                                        request.setAllowedOverRoaming(false);
                                        request.setTitle("Downloading in " +
                                                "Downloaded - " +
                                                context.getResources().getString(R.string.app_name));
                                        request.setDescription(modelQuotesList.getImage_name());
                                        request.setDestinationInExternalPublicDir(getPathForDownloadFile(),
                                                modelQuotesList.getImage_name());
                                        downloadManager.enqueue(request);

                                        BroadcastReceiver receiver = new BroadcastReceiver() {
                                            @Override
                                            public void onReceive(Context context2, Intent intent) {
                                                try {
                                                    String action = intent.getAction();
                                                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                                        if (btnClick.equalsIgnoreCase("ivSave")) {
                                                            vhHeader.mBinding.ivDpSave.setVisibility(View.VISIBLE);
                                                            vhHeader.mBinding.progressDownloadDp.setVisibility(View.GONE);
                                                            vhHeader.mBinding.ivDpSave.setImageResource(R.drawable.ic_downloaded_file);
                                                            flagDownloaded = true;
                                                            btnClick = "";
                                                            Toast.makeText(context, "File downloaded successfully", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    btnClick = "";
                                                    e.printStackTrace();
                                                }
                                            }
                                        };
                                        context.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                                    }
                                } catch (Exception e) {
                                    btnClick = "";
                                    Log.e("DownloadError>>>", e.getMessage());
                                    Crashlytics.log("SaveQuote>>>" + e.getMessage());
                                }
                            }
                        } else {
                            Toast.makeText(context, "File already downloaded!", Toast.LENGTH_SHORT).show();
                            btnClick = "";
                        }
                    }
=======
                            } catch (Exception e) {
                                Log.e("DownloadError>>>", e.getMessage());
                                Crashlytics.log("SaveQuote>>>" + e.getMessage());
                            }
                        }
                    } else
                        Toast.makeText(context, "File already downloaded!", Toast.LENGTH_SHORT).show();
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
                }
            });

        } else if (holder instanceof VHItem) {
            final VHItem VHitem = (VHItem) holder;

            final int IMAGE_HEIGHT = (DEVICE_WIDTH *
                    Integer.parseInt(arrayList.get(position).getHeight())) /
                    Integer.parseInt(arrayList.get(position).getWidth());

            final CardView.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    IMAGE_HEIGHT);
            layoutParams.setMargins(15, 0, 15, 30);
            VHitem.cvQuotesList.setLayoutParams(layoutParams);
            VHitem.cvQuotesList.setContentPadding(2, 2, 2, 2);

            switch (position % 16) {
                case 1:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(15));
                    break;
                case 2:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(14));
                    break;
                case 3:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(13));
                    break;
                case 4:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(12));
                    break;
                case 5:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(11));
                    break;
                case 6:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(10));
                    break;
                case 7:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(9));
                    break;
                case 8:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(8));
                    break;
                case 9:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(7));
                    break;
                case 10:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(6));
                    break;
                case 11:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(5));
                    break;
                case 12:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(4));
                    break;
                case 13:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(3));
                    break;
                case 14:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(2));
                    break;
                case 15:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(1));
                    break;
                case 0:
                    VHitem.llQuotesList.setCardBackgroundColor(COLORS.get(0));
                    break;
            }

            GlideApp.with(context)
                    .load(arrayList.get(position).getThumbnail_url())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .transform(new RoundedCorners(10))
                    .into(VHitem.ivQuotesList);

            VHitem.cvQuotesList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playSound(R.raw.button_tap);
                    flagDownloaded = false;
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("QuotesList", new Gson().toJson(arrayList.get(holder.getAdapterPosition())));
                    context.startActivity(intent);
                    flagLike = false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    public class VHHeader extends RecyclerView.ViewHolder {
        HeaderLayoutImageShowBinding mBinding;

        VHHeader(HeaderLayoutImageShowBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

            ivSave = mBinding.ivDpSave;
            ivShare = mBinding.ivDpShare;
            if (!context.getIntent().getBooleanExtra("LikeFlag", false))
                getSingleLike(modelQuotesList.getId(), DEVICE_ID);
            if (flagLike) {
                if (Build.VERSION.SDK_INT >= 21) {
                    mBinding.progressDpLike.setVisibility(View.GONE);
                    mBinding.btnDpLike.setVisibility(View.VISIBLE);
                } else {
                    mBinding.progressDpLike.setVisibility(View.GONE);
                    mBinding.ivDpLike.setVisibility(View.VISIBLE);
                }
            }
        }

        private void getSingleLike(int id, String deviceID) {
            if (InternetConnection.checkConnection(context)) {
                RetrofitInterfaces retrofitInterfaces = RetrofitClient.getRetrofit().create(RetrofitInterfaces.class);

                Call<ModelSingleLike> call = retrofitInterfaces.getSingleLike(id, deviceID);
                call.enqueue(new Callback<ModelSingleLike>() {
                    @Override
                    public void onResponse(Call<ModelSingleLike> call, Response<ModelSingleLike> response) {
                        if (response.body() != null) {
                            if (response.body().isLiked()) {
                                if (Build.VERSION.SDK_INT < 21) {
                                    mBinding.progressDpLike.setVisibility(View.GONE);
                                    mBinding.ivDpLike.setVisibility(View.VISIBLE);
                                    mBinding.ivDpLike.setImageResource(R.drawable.ic_favorite_red);
                                } else {
                                    mBinding.progressDpLike.setVisibility(View.GONE);
                                    mBinding.btnDpLike.setVisibility(View.VISIBLE);
                                    mBinding.btnDpLike.setLiked(true);
                                }
                                flagLike = true;
                            } else {
                                if (Build.VERSION.SDK_INT < 21) {
                                    mBinding.progressDpLike.setVisibility(View.GONE);
                                    mBinding.ivDpLike.setVisibility(View.VISIBLE);
                                    mBinding.ivDpLike.setImageResource(R.drawable.ic_favorite_border);
                                } else {
                                    mBinding.progressDpLike.setVisibility(View.GONE);
                                    mBinding.btnDpLike.setVisibility(View.VISIBLE);
                                    mBinding.btnDpLike.setLiked(false);
                                }
                                flagLike = false;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelSingleLike> call, Throwable t) {
                        Crashlytics.log("SingleLike>>>" + t.getMessage());
                    }
                });
            }
        }
    }

    public class VHItem extends RecyclerView.ViewHolder {
        CardView cvQuotesList, llQuotesList;
        ImageView ivQuotesList;

        VHItem(View itemView) {
            super(itemView);

            cvQuotesList = itemView.findViewById(R.id.cv_quotes_list);
            ivQuotesList = itemView.findViewById(R.id.iv_quotes_list);
            llQuotesList = itemView.findViewById(R.id.rl_quotes_list);
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void saveImageAfterPermission() {
        if (btnClick.equalsIgnoreCase("ivSave"))
            ivSave.performClick();
        else if (btnClick.equalsIgnoreCase("ivShare"))
            ivShare.performClick();
    }

    //Check Multiple Permissions...
    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setQuoteLike(int id, String deviceID, final int flag) {
        if (InternetConnection.checkConnection(context)) {
            RetrofitInterfaces retrofitInterfaces = RetrofitClient.getClient().create(RetrofitInterfaces.class);

            if (likedQuotesCall != null)
                likedQuotesCall.cancel();
            likedQuotesCall = retrofitInterfaces.setLikedQuotes(id, flag, deviceID);
            likedQuotesCall.enqueue(new Callback<Model_Liked_Quotes>() {
                @Override
                public void onResponse(Call<Model_Liked_Quotes> call, Response<Model_Liked_Quotes> response) {
                    Log.e("LikeUnlike>>>", response.toString());
                }

                @Override
                public void onFailure(Call<Model_Liked_Quotes> call, Throwable t) {
                    //Log.e("LikeUnlike", Log.getStackTraceString(t));
                    t.printStackTrace();
                    Crashlytics.log("QuoteLike>>>" + t.getMessage());
                }
            });
        }
    }

    private String getDownloadedPath() {
        File dir = new File(Environment.getExternalStorageDirectory() +
                File.separator +
                context.getResources().getString(R.string.app_name) +
                File.separator +
                "Downloaded - " +
                context.getResources().getString(R.string.app_name) +
                File.separator);
        if (!dir.exists()) {
            dir.mkdir();
        }
        return dir.getPath() + File.separator;
    }

    private String getPathForDownloadFile() {
        return context.getResources().getString(R.string.app_name) +
                File.separator +
                "Downloaded - " +
                context.getResources().getString(R.string.app_name);
    }
}
