package com.emergingcoders.imagestatus.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emergingcoders.imagestatus.Model.Quotes;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;

import java.util.ArrayList;

public class Adapter_Sample_Quotes extends RecyclerView.Adapter<Adapter_Sample_Quotes.SampleQuotesViewHolder> {

    private ArrayList<Quotes> arrayList;
    private int rowLayout;
    private Activity context;

    private String BROADCAST_FILTER = "com.emergingcoders.imagestatus.Design.SampleQuotesActivity";

    public Adapter_Sample_Quotes(ArrayList<Quotes> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public SampleQuotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
        return new SampleQuotesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SampleQuotesViewHolder holder, int position) {
        holder.tvSampleQuotes.setText(arrayList.get(position).getQuote().trim());

        holder.cvSampleQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                Intent intent = new Intent(BROADCAST_FILTER);
                intent.putExtra("SampleQuote", arrayList.get(holder.getAdapterPosition()).getQuote());
                intent.putExtra("SampleQuoteBy", "");
                intent.putExtra("isFrom", "Sample");
                context.sendBroadcast(intent);
                context.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class SampleQuotesViewHolder extends RecyclerView.ViewHolder {
        CardView cvSampleQuotes;
        TextView tvSampleQuotes;

        SampleQuotesViewHolder(View itemView) {
            super(itemView);

            cvSampleQuotes = itemView.findViewById(R.id.cv_sample_quotes);
            tvSampleQuotes = itemView.findViewById(R.id.tv_sample_quotes);
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
