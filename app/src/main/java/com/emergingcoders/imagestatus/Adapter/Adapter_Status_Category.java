package com.emergingcoders.imagestatus.Adapter;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.emergingcoders.imagestatus.Design.DashboardActivity;
import com.emergingcoders.imagestatus.Globals.Globals;
import com.emergingcoders.imagestatus.Model.ModelStatusCategory;
import com.emergingcoders.imagestatus.R;
import com.emergingcoders.imagestatus.Utils.AppPreferences;
import com.emergingcoders.imagestatus.Utils.GlideApp;

import java.util.ArrayList;

public class Adapter_Status_Category extends RecyclerView.Adapter<Adapter_Status_Category.StatusCategoryViewHolder> {

    private ArrayList<ModelStatusCategory> arrayList;
    private int rowLayout;
    private Activity context;

    private Typeface typeface;

    public Adapter_Status_Category(ArrayList<ModelStatusCategory> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
        setHasStableIds(true);

        AssetManager am = context.getApplicationContext().getAssets();
        typeface = Typeface.createFromAsset(am, Globals.APP_FONTS);
    }

    @NonNull
    @Override
    public StatusCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
        return new StatusCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StatusCategoryViewHolder holder, int position) {
        holder.tvTitle.setText(arrayList.get(position).getName());

        GlideApp.with(context)
                .load(arrayList.get(position).getImage_Url())
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivCategory);

        holder.cvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(R.raw.button_tap);
                if (context instanceof DashboardActivity) {
                    ((DashboardActivity) context).strCatID = arrayList.get(holder.getAdapterPosition()).getId() + "";
                    ((DashboardActivity) context).strCatName = arrayList.get(holder.getAdapterPosition()).getName();

                    if (!Globals.flagRemoveAds)
                        ((DashboardActivity) context).requestNewInterstitial();
                    ((DashboardActivity) context).getQuotesByCategory(arrayList.get(holder.getAdapterPosition()).getId() + "",
                            arrayList.get(holder.getAdapterPosition()).getName());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class StatusCategoryViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cvCategory;
        ImageView ivCategory;
        TextView tvTitle;

        StatusCategoryViewHolder(View itemView) {
            super(itemView);

            cvCategory = itemView.findViewById(R.id.cv_category);
            ivCategory = itemView.findViewById(R.id.iv_category_image);
            tvTitle = itemView.findViewById(R.id.tv_category_title);

            tvTitle.setTypeface(typeface);
        }
    }

    private void playSound(int raw) {
        if (AppPreferences.isSoundOn(context)) {
            MediaPlayer mp = MediaPlayer.create(context, raw);
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(context, raw);
                    mp.start();
                } else {
                    mp.start();
                }
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
