package com.emergingcoders.imagestatus.Adapter;


import android.app.Activity;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.emergingcoders.imagestatus.Model.Model_Font_Style;
import com.emergingcoders.imagestatus.databinding.RowLayoutFontTypesBinding;

import java.util.ArrayList;

public class Adapter_Font_Family extends RecyclerView.Adapter<Adapter_Font_Family.FontFamilyViewHolder> {
    ArrayList<Model_Font_Style> arrayList;
    int rowLayout;
    Activity context;

    Typeface typeface;
    public static int selectedID = 13;

    public Adapter_Font_Family(ArrayList<Model_Font_Style> arrayList, int rowLayout, Activity context) {
        this.arrayList = arrayList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @NonNull
    @Override
    public FontFamilyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowLayoutFontTypesBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                rowLayout,
                parent,
                false);
        return new FontFamilyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull FontFamilyViewHolder holder, int position) {
        AssetManager am = context.getApplicationContext().getAssets();
        typeface = Typeface.createFromAsset(am, "fonts/" + arrayList.get(position).getFilePath());

        holder.mBinding.tvFontTypeName.setText(arrayList.get(position).getFileName());
        holder.mBinding.tvFontTypeName.setTypeface(typeface);

        //If selectedIds ArrayList contains id then change background color of selected item...
        holder.mBinding.llFontType.setBackgroundColor(selectedID == position ? Color.parseColor("#AAAAAA") : Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class FontFamilyViewHolder extends RecyclerView.ViewHolder {
        RowLayoutFontTypesBinding mBinding;

        public FontFamilyViewHolder(RowLayoutFontTypesBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }
}
