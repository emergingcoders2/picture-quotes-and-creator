package com.emergingcoders.imagestatus.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.emergingcoders.imagestatus.Fragment.Fragment_Font_Adjustment;
import com.emergingcoders.imagestatus.Fragment.Fragment_Font_Family;
import com.emergingcoders.imagestatus.Fragment.Fragment_Font_Shadows;
import com.emergingcoders.imagestatus.Fragment.Fragment_Quote_Rotate;

public class AdapterFragmentPager extends FragmentStatePagerAdapter {

    private int tabCount;

    public AdapterFragmentPager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Fragment_Font_Adjustment();
            case 1:
                return new Fragment_Font_Family();
            case 2:
                return new Fragment_Font_Shadows();
            case 3:
                return new Fragment_Quote_Rotate();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
