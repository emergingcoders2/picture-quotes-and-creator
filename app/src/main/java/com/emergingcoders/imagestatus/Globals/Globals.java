package com.emergingcoders.imagestatus.Globals;

import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup;

public class Globals {
    public static final String APP_FONTS = "fonts/choco-cookie.ttf";

    public static boolean flagRemoveWm = false;
    public static boolean flagRemoveAds = false;

    public static Boolean isFontColor = false,
            isChangesOccurs = false;

    public static int lastAction,
            sectionWidth,
            sectionHeight = ViewGroup.LayoutParams.MATCH_PARENT,
            sectionChildGravity = Gravity.CENTER,
            sectionChildFontGravity = Gravity.CENTER,
            fontFamilyPosition = 13,
            wmFontFamilyPosition = 13,
            quoteTextSize = 32,
            quoteBySize = 16,
            lineSpacing = 2,
            deviceWidth,
            tintLast = 0,
            lastTextColor = Color.WHITE,
            lastTextShadowColor = Color.BLACK,
            lastBackColor = Color.BLACK,
            lastRotateProgress = 24,
            lastRotate = 0,
            lastProgressX = 11,
            lastProgressY = 11,
            lastProgressRadius = 2,
            lastX = 0,
            lastY = 0;
    public static float rotateMultiplier = 7.5f;
}
