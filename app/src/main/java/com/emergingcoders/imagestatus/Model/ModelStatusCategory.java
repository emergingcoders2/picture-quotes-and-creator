package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mr on 15/11/17.
 */

public class ModelStatusCategory {

    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("url")
    String Image_Url;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage_Url() {
        return Image_Url;
    }
}
