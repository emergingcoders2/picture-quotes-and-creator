package com.emergingcoders.imagestatus.Model;


public class Model_Recent_Quotes {

    private int id;
    private String quote, quoteBy;

    public Model_Recent_Quotes(int id, String quote, String quoteBy) {
        this.id = id;
        this.quote = quote;
        this.quoteBy = quoteBy;
    }

    public int getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public String getQuoteBy() {
        return quoteBy;
    }
}
