package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

public class ModelSingleLike {
    @SerializedName("result")
    private boolean isLiked;

    public boolean isLiked() {
        return isLiked;
    }
}
