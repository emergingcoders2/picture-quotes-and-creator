package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Status_Category {

    @SerializedName("result")
    private String success;

    @SerializedName("data")
    private ArrayList<ModelStatusCategory> categoryList;

    public String getSuccess() {
        return success;
    }

    public ArrayList<ModelStatusCategory> getCategoryList() {
        return categoryList;
    }
}
