package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mr on 15/11/17.
 */

public class ModelQuotesList {

    @SerializedName("id")
    int id;

    @SerializedName("category_id")
    int CatID;

    @SerializedName("flag")
    int flag;

    @SerializedName("category_name")
    String CatName;

    @SerializedName("quote")
    String quote;

    @SerializedName("url")
    String image_url;

    @SerializedName("thumbnail_url")
    String thumbnail_url;

    @SerializedName("height")
    String height;

    @SerializedName("width")
    String width;

    @SerializedName("image_name")
    String image_name;

    public int getId() {
        return id;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getQuote() {
        return quote;
    }

    public String getImage_name() {
        return image_name;
    }

    public int getCatID() {
        return CatID;
    }

    public String getCatName() {
        return CatName;
    }

    public int getFlag() {
        return flag;
    }
}
