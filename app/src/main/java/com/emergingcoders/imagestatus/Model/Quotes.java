package com.emergingcoders.imagestatus.Model;

public class Quotes {
    private int id;
    private String categoryId;
    private String quote;
    private int isLiked;
    private String utp;

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUtp() {
        return utp;
    }

    public void setUtp(String utp) {
        this.utp = utp;
    }
}