package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Quotes_List {

    @SerializedName("result")
    private String success;

    @SerializedName("data")
    private ArrayList<ModelQuotesList> qoutesList;

    public String getSuccess() {
        return success;
    }

    public ArrayList<ModelQuotesList> getQoutesList() {
        return qoutesList;
    }
}
