package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

public class ModelLikedQuotes {

    @SerializedName("id")
    private int id;

    @SerializedName("category_id")
    private int CatID;

    @SerializedName("category_name")
    private String CatName;

    @SerializedName("flag")
    private int flag;

    @SerializedName("quote")
    private String quote;

    @SerializedName("url")
    private String image_url;

    @SerializedName("thumbnail_url")
    private String thumbnail_url;

    @SerializedName("height")
    private String height;

    @SerializedName("width")
    private String width;

    @SerializedName("image_name")
    private String image_name;

    public int getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getImage_name() {
        return image_name;
    }

    public int getCatID() {
        return CatID;
    }

    public String getCatName() {
        return CatName;
    }

    public int getFlag() {
        return flag;
    }
}
