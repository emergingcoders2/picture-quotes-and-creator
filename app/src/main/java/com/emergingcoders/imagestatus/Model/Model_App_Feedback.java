package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

public class Model_App_Feedback {

    @SerializedName("result")
    private String result;

    public String getResult() {
        return result;
    }
}
