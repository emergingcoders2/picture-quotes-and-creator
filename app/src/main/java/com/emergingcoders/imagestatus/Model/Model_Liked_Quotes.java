package com.emergingcoders.imagestatus.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Liked_Quotes {

    @SerializedName("result")
    private String success;

    @SerializedName("data")
    private ArrayList<ModelLikedQuotes> qoutesList;

    public String getSuccess() {
        return success;
    }

    public ArrayList<ModelLikedQuotes> getQoutesList() {
        return qoutesList;
    }
}
