package com.emergingcoders.imagestatus.Model;

public class Model_Font_Style {

    private String fileName, filePath;

    public Model_Font_Style(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }
}
