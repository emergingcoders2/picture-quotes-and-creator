package com.emergingcoders.imagestatus.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.emergingcoders.imagestatus.Design.SampleQuotesActivity;
import com.emergingcoders.imagestatus.Model.Categories;
import com.emergingcoders.imagestatus.Model.Model_Recent_Quotes;
import com.emergingcoders.imagestatus.Model.Quotes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class DbHelper extends SQLiteOpenHelper {

    private static final String TAG = "DbHelper";

    private static final String DATABASE_NAME = "quotes.sqlite";

    public static final int DATABASE_VERSION = 2;

    public static final String SP_KEY_DB_VER = "db_ver";
    public static final String TABLE_RECENT = "recent_quotes";
    private Context mContext = null;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        initialize();
    }

    public void removeDatabase(Context mContext) {
        mContext.deleteDatabase(DATABASE_NAME);
    }

    /**
     * Initializes database. Creates database if doesn't exist.
     */
    private void initialize() {
        if (databaseExists()) {
            SharedPreferences prefs = mContext.getSharedPreferences("PREF", Context.MODE_PRIVATE);
            int dbVersion = prefs.getInt(SP_KEY_DB_VER, 1);
            if (DATABASE_VERSION != dbVersion) {
                createDatabase();
            }
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_RECENT +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "quote TEXT," +
                    "quote_by TEXT," +
                    "added INTEGER(20))");
        }
        if (!databaseExists()) {
            createDatabase();
        }
    }

    /**
     * Returns true if database file exists, false otherwise.
     */
    private boolean databaseExists() {
        File dbFile = mContext.getDatabasePath(DATABASE_NAME);
        return dbFile.exists();
    }

    /**
     * Creates database by copying it from assets directory.
     */
    private void createDatabase() {
        String parentPath = mContext.getDatabasePath(DATABASE_NAME).getParent();
        String path = mContext.getDatabasePath(DATABASE_NAME).getPath();

        File file = new File(parentPath);
        if (!file.exists()) {
            if (!file.mkdir()) {
                Log.w(TAG, "Unable to create database directory");
                return;
            }
        }

        InputStream is = null;
        OutputStream os = null;
        try {
            is = mContext.getAssets().open(DATABASE_NAME);
            os = new FileOutputStream(path);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
            SharedPreferences prefs = mContext.getSharedPreferences("PREF", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(SP_KEY_DB_VER, DATABASE_VERSION);
            editor.apply();
        } catch (IOException e) {
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public ArrayList<Categories> getCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM category ORDER BY name";

        ArrayList<Categories> arrayList = new ArrayList<>();
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                Categories categories = new Categories();
                categories.setId(cur.getString(cur.getColumnIndex("_id")));
                categories.setCategoryName(cur.getString(cur.getColumnIndex("name")));

                arrayList.add(categories);
            } while (cur.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public ArrayList<Quotes> getQuotesByCategory(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM quotes WHERE category_id=" + id + " LIMIT 30";

        ArrayList<Quotes> arrayList = new ArrayList<>();
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                Quotes quotes = new Quotes();
                quotes.setId(cur.getInt(cur.getColumnIndex("_id")));
                quotes.setCategoryId(cur.getString(cur.getColumnIndex("category_id")));
                quotes.setQuote(cur.getString(cur.getColumnIndex("quote")));

                arrayList.add(quotes);
                ((SampleQuotesActivity) mContext).loadedIds += cur.getString(cur.getColumnIndex("_id")) + ",";
            } while (cur.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public ArrayList<Quotes> getQuotes(String loadedIDs) {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM quotes WHERE _id NOT IN(" + removeLastComa(loadedIDs) + ") ORDER BY RANDOM() LIMIT 30";

        ArrayList<Quotes> arrayList = new ArrayList<>();
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                Quotes quotes = new Quotes();
                quotes.setId(cur.getInt(cur.getColumnIndex("_id")));
                quotes.setCategoryId(cur.getString(cur.getColumnIndex("category_id")));
                quotes.setQuote(cur.getString(cur.getColumnIndex("quote")));

                arrayList.add(quotes);
                ((SampleQuotesActivity) mContext).loadedIds += cur.getString(cur.getColumnIndex("_id")) + ",";
            } while (cur.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public ArrayList<Quotes> getMoreQuotesByCategory(String loadedIDs, String CID)  {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM quotes WHERE _id NOT IN(" + removeLastComa(loadedIDs) + ") AND category_id=" + CID + " LIMIT 30";

        ArrayList<Quotes> arrayList = new ArrayList<>();
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                Quotes quotes = new Quotes();
                quotes.setId(cur.getInt(cur.getColumnIndex("_id")));
                quotes.setCategoryId(cur.getString(cur.getColumnIndex("category_id")));
                quotes.setQuote(cur.getString(cur.getColumnIndex("quote")));

                arrayList.add(quotes);
                ((SampleQuotesActivity) mContext).loadedIds += cur.getString(cur.getColumnIndex("_id")) + ",";
            } while (cur.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public ArrayList<Quotes> getQuotes() {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM quotes ORDER BY RANDOM() LIMIT 30";

        ArrayList<Quotes> arrayList = new ArrayList<>();
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                Quotes quotes = new Quotes();
                quotes.setId(cur.getInt(cur.getColumnIndex("_id")));
                quotes.setCategoryId(cur.getString(cur.getColumnIndex("category_id")));
                quotes.setQuote(cur.getString(cur.getColumnIndex("quote")));

                arrayList.add(quotes);

                ((SampleQuotesActivity) mContext).loadedIds += cur.getString(cur.getColumnIndex("_id")) + ",";
            } while (cur.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public String getSingleQuote() {
        SQLiteDatabase db = this.getReadableDatabase();
        String strQuery = "SELECT * FROM quotes ORDER BY RANDOM() LIMIT 1";
        String quote = "";

        Cursor cursor = db.rawQuery(strQuery, null);
        if (cursor.moveToFirst()) {
            quote = cursor.getString(cursor.getColumnIndex("quote"));
        }
        db.close();

        return quote;
    }

    public void addRecentQuotes(String quote, String quoteBy) {
        SQLiteDatabase database = this.getReadableDatabase();
        String strQuery = "SELECT * FROM " + TABLE_RECENT + " WHERE quote = ?";
        Cursor cursor = database.rawQuery(strQuery, new String[]{quote});

        if (cursor.moveToFirst()) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("quote_by", quoteBy);
            contentValues.put("added", Integer.parseInt(String.valueOf(System.currentTimeMillis() / 1000)));

            db.update(TABLE_RECENT, contentValues, "quote = ?", new String[]{quote});
            db.close();
        } else {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put("quote", quote);
            contentValues.put("quote_by", quoteBy);
            contentValues.put("added", Integer.parseInt(String.valueOf(System.currentTimeMillis() / 1000)));

            db.insert(TABLE_RECENT, null, contentValues);
            db.close();
        }
        database.close();

        Log.e("QuoteRecent>>>", quote);
    }

    public ArrayList<Model_Recent_Quotes> getRecentQuotes(int pageID) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Model_Recent_Quotes> arrayList = new ArrayList<>();

        int startFrom = (pageID - 1) * 30;
        String strQuery = "SELECT * FROM " + TABLE_RECENT + " ORDER BY added DESC LIMIT " + startFrom + ",30";

        Cursor cursor = db.rawQuery(strQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Model_Recent_Quotes modelRecentQuotes = new Model_Recent_Quotes(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("quote")),
                        cursor.getString(cursor.getColumnIndex("quote_by")));
                arrayList.add(modelRecentQuotes);
            } while (cursor.moveToNext());
        }
        db.close();

        return arrayList;
    }

    public void deleteRecentQuote(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String strQuery = "DELETE FROM " + TABLE_RECENT + " WHERE id=" + id;
        db.execSQL(strQuery);
    }

    public String removeLastComa(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
