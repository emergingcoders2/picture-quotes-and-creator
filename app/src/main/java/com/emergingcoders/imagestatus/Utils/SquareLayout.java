package com.emergingcoders.imagestatus.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/*
 * Created by Nick Bapu on 21-08-2017.
 */

public class SquareLayout extends FrameLayout {

    public SquareLayout(Context context) {
        super(context);
    }

    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int width, int height) {
        // note we are applying the width value as the height
        super.onMeasure(width, width);
    }

}
