package com.emergingcoders.imagestatus.Utils;

/*
 * Created by nickbapu on 26/2/18.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
<<<<<<< HEAD
=======
import android.os.Environment;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505
import android.view.Gravity;

import com.emergingcoders.imagestatus.R;

import static android.content.Context.MODE_PRIVATE;

public class AppPreferences {
    private static final String MY_PREF = "MP";

    private static SharedPreferences getPreference(Context mContext) {
        return mContext.getSharedPreferences(MY_PREF, MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getPreferenceForEdit(Context context) {
        return context.getSharedPreferences(MY_PREF, Context.MODE_PRIVATE).edit();
    }

    public static Long getLastSavedDateForAds(Context mContext) {
        return getPreference(mContext).getLong("last_date_for_ads", 0);
    }

    public static void setLastSavedDateForAds(Context mContext, Long date) {
        getPreferenceForEdit(mContext).putLong("last_date_for_ads", date).apply();
    }

    public static boolean isSoundOn(Context mContext) {
        return getPreference(mContext).getBoolean("is_sound_on", true);
    }

    public static void setSoundOn(Context mContext, boolean isOn) {
        getPreferenceForEdit(mContext).putBoolean("is_sound_on", isOn).apply();
    }

    public static void setWMText(Context mContext, String text) {
        getPreferenceForEdit(mContext).putString("WMText", text).apply();
    }

    public static String getWMText(Context mContext) {
        return getPreference(mContext).getString("WMText", "Watermark Text");
    }

    public static void setWMAlignment(Context mContext, int align) {
        getPreferenceForEdit(mContext).putInt("WMAlign", align).apply();
    }

    public static int getWMAlignment(Context mContext) {
        return getPreference(mContext).getInt("WMAlign", Gravity.CENTER);
    }

    public static void setWMOpacity(Context mContext, int opacity) {
        getPreferenceForEdit(mContext).putInt("WMOpacity", opacity).apply();
    }

    public static int getWMOpacity(Context mContext) {
        return getPreference(mContext).getInt("WMOpacity", 0);
    }

    public static void setWMFont(Context mContext, String fonts) {
        getPreferenceForEdit(mContext).putString("WMFonts", fonts).apply();
    }

    public static String getWMFont(Context mContext) {
        return getPreference(mContext).getString("WMFonts", "fonts/Bebas Neue Bold.ttf");
    }

    public static void setWMFontID(Context mContext, int id) {
        getPreferenceForEdit(mContext).putInt("WMFontID", id).apply();
    }

    public static int getWMFontID(Context mContext) {
        return getPreference(mContext).getInt("WMFontID", 13);
    }

    public static void setWMColor(Context mContext, int color) {
        getPreferenceForEdit(mContext).putInt("WMColor", color).apply();
    }

    public static int getWMColor(Context mContext) {
        return getPreference(mContext).getInt("WMColor", Color.WHITE);
    }

    public static void setWMFontSize(Context mContext, int size) {
        getPreferenceForEdit(mContext).putInt("WMFontSize", size).apply();
    }

    public static int getWMFontSize(Context mContext) {
        return getPreference(mContext).getInt("WMFontSize", 32);
    }

    public static void setSaveType(Context mContext, int buttonId) {
        getPreferenceForEdit(mContext).putInt("SaveButton", buttonId).apply();
    }

    public static int getSaveType(Context mContext) {
        return getPreference(mContext).getInt("SaveButton", R.id.btn_save_default);
    }

    public static void setWMLastProgressX(Context mContext, int lastProgressX) {
        getPreferenceForEdit(mContext).putInt("WMLastProgressX", lastProgressX).apply();
    }

    public static int getWMLastProgressX(Context mContext) {
        return getPreference(mContext).getInt("WMLastProgressX", 11);
    }

    public static void setWMLastProgressY(Context mContext, int lastProgressY) {
        getPreferenceForEdit(mContext).putInt("WMLastProgressY", lastProgressY).apply();
    }

    public static int getWMLastProgressY(Context mContext) {
        return getPreference(mContext).getInt("WMLastProgressY", 11);
    }

    public static void setWMLastProgressRadius(Context mContext, int lastProgressRadius) {
        getPreferenceForEdit(mContext).putInt("WMLastProgressRadius", lastProgressRadius).apply();
    }

    public static int getWMLastProgressRadius(Context mContext) {
        return getPreference(mContext).getInt("WMLastProgressRadius", 2);
    }

    public static void setWMLastX(Context mContext, int lastX) {
        getPreferenceForEdit(mContext).putInt("WMLastX", lastX).apply();
    }

    public static int getWMLastX(Context mContext) {
        return getPreference(mContext).getInt("WMLastX", 0);
    }

    public static void setWMLastY(Context mContext, int lastY) {
        getPreferenceForEdit(mContext).putInt("WMLastY", lastY).apply();
    }

    public static int getWMLastY(Context mContext) {
        return getPreference(mContext).getInt("WMLastY", 0);
    }

    public static void setWMShadowColor(Context mContext, int color) {
        getPreferenceForEdit(mContext).putInt("WMShadowColor", color).apply();
    }

    public static int getWMShadowColor(Context mContext) {
        return getPreference(mContext).getInt("WMShadowColor", Color.BLACK);
    }

    public static void setWMLastRotateProgress(Context mContext, int lastRotateProgress) {
        getPreferenceForEdit(mContext).putInt("WMLastRotateProgress", lastRotateProgress).apply();
    }

    public static int getWMLastRotateProgress(Context mContext) {
        return getPreference(mContext).getInt("WMLastRotateProgress", 24);
    }

    public static void setWMLastRotate(Context mContext, int lastRotate) {
        getPreferenceForEdit(mContext).putInt("WMLastRotate", lastRotate).apply();
    }

    public static int getWMLastRotate(Context mContext) {
        return getPreference(mContext).getInt("WMLastRotate", 0);
    }

    public static void setWMText(Context mContext, String text) {
        getPreferenceForEdit(mContext).putString("WMText", text).apply();
    }

    public static String getWMText(Context mContext) {
        return getPreference(mContext).getString("WMText", "Watermark Text");
    }

    public static void setWMAlignment(Context mContext, int align) {
        getPreferenceForEdit(mContext).putInt("WMAlign", align).apply();
    }

    public static int getWMAlignment(Context mContext) {
        return getPreference(mContext).getInt("WMAlign", Gravity.CENTER);
    }

    public static void setWMOpacity(Context mContext, int opacity) {
        getPreferenceForEdit(mContext).putInt("WMOpacity", opacity).apply();
    }

    public static int getWMOpacity(Context mContext) {
        return getPreference(mContext).getInt("WMOpacity", 0);
    }

    public static void setWMFont(Context mContext, String fonts) {
        getPreferenceForEdit(mContext).putString("WMFonts", fonts).apply();
    }

    public static String getWMFont(Context mContext) {
        return getPreference(mContext).getString("WMFonts", "fonts/Bebas Neue Bold.ttf");
    }

    public static void setWMFontID(Context mContext, int id) {
        getPreferenceForEdit(mContext).putInt("WMFontID", id).apply();
    }

    public static int getWMFontID(Context mContext) {
        return getPreference(mContext).getInt("WMFontID", 13);
    }

    public static void setWMColor(Context mContext, int color) {
        getPreferenceForEdit(mContext).putInt("WMColor", color).apply();
    }

    public static int getWMColor(Context mContext) {
        return getPreference(mContext).getInt("WMColor", Color.WHITE);
    }

    public static void setWMFontSize(Context mContext, int size) {
        getPreferenceForEdit(mContext).putInt("WMFontSize", size).apply();
    }

    public static int getWMFontSize(Context mContext) {
        return getPreference(mContext).getInt("WMFontSize", 32);
    }
}
