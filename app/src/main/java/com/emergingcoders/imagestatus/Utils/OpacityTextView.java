package com.emergingcoders.imagestatus.Utils;

import android.content.Context;
import android.util.AttributeSet;
<<<<<<< HEAD
=======
import android.widget.TextView;
>>>>>>> 49be3fe5e16678a58543033b4c8f22d3ee41f505

public class OpacityTextView extends android.support.v7.widget.AppCompatTextView {

    public OpacityTextView(Context context) {
        super(context);
    }

    public OpacityTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OpacityTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onSetAlpha(int alpha) {
        setTextColor(getTextColors().withAlpha(alpha));
        setHintTextColor(getHintTextColors().withAlpha(alpha));
        setLinkTextColor(getLinkTextColors().withAlpha(alpha));
        //getBackground().setAlpha(alpha);
        return true;
    }
}
